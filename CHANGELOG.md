<!-- markdownlint-disable MD024 -->
# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

### Added

- Add copyrights year automatically

### Updated

- Update versioning

### Fixed

- Fix crash on first time swipe arrived

### Removed

-


## [0.1.0] - 2021-08-02

[Unreleased]: https://gitlab.com/robomart/ios-apps/robomart-driver-app/-/compare/0.1.0...develop
[0.1.0]: https://gitlab.com/robomart/ios-apps/robomart-driver-app/-/tags/0.1.0
