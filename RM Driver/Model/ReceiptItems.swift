//
//  ReceiptItems.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 11/4/20.
//

import Foundation

// MARK: - CustomToken
struct ReceiptItems: Codable {
  let success: Bool?
  let data: [ReceiptItemData]?
  let error: String?
}

struct ReceiptItemData: Codable {
  let id, orderID, productID, productName: String?
  let price: Double?
  let qty: Int?
  let img: String?

  enum CodingKeys: String, CodingKey {
    case id
    case orderID = "order_id"
    case productID = "product_id"
    case productName = "product_name"
    case price, qty, img
  }
}
