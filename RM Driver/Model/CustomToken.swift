//
//  CustomToken.swift
//  RobomartDriver
//
//  Created by Fahad Naqvi on 10/26/20.
//

import Foundation

// MARK: - CustomToken
struct CustomToken: Codable {
  let success: Bool?
  let data, error: String?
}
