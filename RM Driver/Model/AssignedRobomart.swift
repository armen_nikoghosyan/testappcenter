//
//  AssignedRobomart.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 12/13/20.
//

import Foundation

// MARK: - AssignedRobomart
struct AssignedRobomart: Codable {
  let success: Bool?
  let data, error: String?
}
