//
//  Menu.swift
//  Robomart
//
//  Created by Macbook Pro on 26/04/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import Foundation

struct SideMenu {
  var name: String?
  var icon: String?
  var identifier: String?
}

struct Menu {
  var catID: String?
  var name: String?
  var icon: String?
  var isCategory: Bool?
}

extension Menu {
  enum sortByName {
    static let sortNames: (Menu, Menu) -> Bool = {
      ($0.name!) < ($1.name!)
    }
  }
}
