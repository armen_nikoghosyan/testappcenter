//
//  UserData.swift
//  RM Driver
//
//  Created by sose yeritsyan on 8/6/21.
//

import Foundation

// MARK: - UserData
struct UserData: Codable {
  var inviteCode, stripeToken: String?
  var email: String?
  var promotionalNotification: Bool?
  var cardType: String?
  var deviceType: Int?
  var deliveryNotification: Bool?
  var osVersion: String?
  var isEmailVerified: Bool?
  var id, code: String?
  var deviceToken: String?
  var emailCode, stripeCustomerID, photoURL: String?
  var deviceID, cardLast4: String?
  var fullname, phone, newPhone: String?
  var isActive, deliverySMS: Bool?
  var createdOn, lastUpdatedOn, newEmail: String?
  var isFirsttimeEmailVerified: Bool?

  enum CodingKeys: String, CodingKey {
    case inviteCode = "invite_code"
    case stripeToken = "stripe_token"
    case email
    case promotionalNotification = "promotional_notification"
    case cardType = "card_type"
    case deviceType = "device_type"
    case deliveryNotification = "delivery_notification"
    case osVersion = "os_version"
    case isEmailVerified = "is_email_verified"
    case id, code
    case deviceToken = "device_token"
    case emailCode = "email_code"
    case stripeCustomerID = "stripe_customer_id"
    case photoURL = "photo_url"
    case deviceID = "device_id"
    case cardLast4 = "card_last4"
    case fullname, phone
    case newPhone = "new_phone"
    case isActive = "is_active"
    case deliverySMS = "delivery_sms"
    case createdOn = "created_on"
    case lastUpdatedOn = "last_updated_on"
    case newEmail = "new_email"
    case isFirsttimeEmailVerified = "is_firsttime_email_verified"
  }
}
