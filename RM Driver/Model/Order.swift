//
//  Order.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 12/13/20.
//

import Foundation

// MARK: - Order
struct Order: Codable {
  let data: OrderData?
  let success: Bool?
  let error: String?
}

// MARK: - DataClass
struct OrderData: Codable {
  let orderID: String?
  let itemsCount: Int?
  let initialEta: String?
  let paymentStatus: Int?
  let subtotal: Double?
  let statusID: Int?
  let consumerID, requestDoorClose: String?
  let initialAlternateDistance, serviceFee: Int?
  let orderLng: Double?
  let hailingFee: Int?
  let status, shoppingEnd, fullname, vehicleOnrouteTime: String?
  let shoppingStart, typeID: String?
  let orderLat: Double?
  let orderAssignedTime: String?
  let isCanceledByConsumer: Bool?
  let mapImageURL: String?
  let stripePayment: Int?
  let total, tax: Double?
  let requestDoorOpen, storeID, phone, initialAlternateEta: String?
  let address: String?
  let items: [OrderItem]?
  let vehicleDoorOpen: String?
  let vehicleLng: Int?
  let transactionID, vehicleDoorClose, email, transactionError: String?
  let typeImageURL: String?
  let requestTime: String?
  let vehicleLat: Int?
  let typeName, vehicleArrivedTime, receiptGenerated: String?
  let initialDistance, stripeFee: Int?
  let canceledTime, vehicleName, vehicleID, canceledBy: String?
  let vehicleDepart, receiptNo, cardLast4: String?

  enum CodingKeys: String, CodingKey {
    case orderID = "order_id"
    case itemsCount = "items_count"
    case initialEta = "initial_eta"
    case paymentStatus = "payment_status"
    case subtotal
    case statusID = "status_id"
    case consumerID = "consumer_id"
    case requestDoorClose = "request_door_close"
    case initialAlternateDistance = "initial_alternate_distance"
    case serviceFee = "service_fee"
    case orderLng = "order_lng"
    case hailingFee = "hailing_fee"
    case status
    case shoppingEnd = "shopping_end"
    case fullname
    case vehicleOnrouteTime = "vehicle_onroute_time"
    case shoppingStart = "shopping_start"
    case typeID = "type_id"
    case orderLat = "order_lat"
    case orderAssignedTime = "order_assigned_time"
    case isCanceledByConsumer = "is_canceled_by_consumer"
    case mapImageURL = "map_image_url"
    case stripePayment = "stripe_payment"
    case total, tax
    case requestDoorOpen = "request_door_open"
    case storeID = "store_id"
    case phone
    case initialAlternateEta = "initial_alternate_eta"
    case address, items
    case vehicleDoorOpen = "vehicle_door_open"
    case vehicleLng = "vehicle_lng"
    case transactionID = "transaction_id"
    case vehicleDoorClose = "vehicle_door_close"
    case email
    case transactionError = "transaction_error"
    case typeImageURL = "type_image_url"
    case requestTime = "request_time"
    case vehicleLat = "vehicle_lat"
    case typeName = "type_name"
    case vehicleArrivedTime = "vehicle_arrived_time"
    case receiptGenerated = "receipt_generated"
    case initialDistance = "initial_distance"
    case stripeFee = "stripe_fee"
    case canceledTime = "canceled_time"
    case vehicleName = "vehicle_name"
    case vehicleID = "vehicle_id"
    case canceledBy = "canceled_by"
    case vehicleDepart = "vehicle_depart"
    case receiptNo = "receipt_no"
    case cardLast4 = "card_last4"
  }

  //    let items: [OrderItem]?
  //    let typeImageURL: String?
  //    let receiptNo, vehicleDoorOpen, transactionID: String?
  //    let orderLng: Double?
  //    let vehicleID, cardLast4, shoppingStart, status: String?
  //    let subtotal: Int?
  //    let orderAssignedTime, canceledTime: String?
  //    let vehicleLat, itemsCount: Int?
  //    let vehicleDepart, email, fullname, requestTime: String?
  //    let total: Int?
  //    let typeName: String?
  //    let stripeFee: Int?
  //    let transactionError: String?
  //    let mapImageURL: String?
  //    let vehicleArrivedTime: String?
  //    let stripePayment, tax: Int?
  //    let initialDistance, orderLat: Double?
  //    let initialEta, address: String?
  //    let statusID: Int?
  //    let consumerID, vehicleOnrouteTime, typeID, shoppingEnd: String?
  //    let phone: String?
  //    let isCanceledByConsumer: Bool?
  //    let hailingFee: Int?
  //    let vehicleDoorClose, canceledBy: String?
  //    let paymentStatus: Int?
  //    let orderID: String?
  //    let vehicleLng: Int?
  //    let vehicleName: String?
  //    let serviceFee: Int?
  //    let storeID: String?
  //
  //    enum CodingKeys: String, CodingKey {
  //        case items
  //        case typeImageURL = "type_image_url"
  //        case receiptNo = "receipt_no"
  //        case vehicleDoorOpen = "vehicle_door_open"
  //        case transactionID = "transaction_id"
  //        case orderLng = "order_lng"
  //        case vehicleID = "vehicle_id"
  //        case cardLast4 = "card_last4"
  //        case shoppingStart = "shopping_start"
  //        case status, subtotal
  //        case orderAssignedTime = "order_assigned_time"
  //        case canceledTime = "canceled_time"
  //        case vehicleLat = "vehicle_lat"
  //        case itemsCount = "items_count"
  //        case vehicleDepart = "vehicle_depart"
  //        case email, fullname
  //        case requestTime = "request_time"
  //        case total
  //        case typeName = "type_name"
  //        case stripeFee = "stripe_fee"
  //        case transactionError = "transaction_error"
  //        case mapImageURL = "map_image_url"
  //        case vehicleArrivedTime = "vehicle_arrived_time"
  //        case stripePayment = "stripe_payment"
  //        case tax
  //        case initialDistance = "initial_distance"
  //        case orderLat = "order_lat"
  //        case initialEta = "initial_eta"
  //        case address
  //        case statusID = "status_id"
  //        case consumerID = "consumer_id"
  //        case vehicleOnrouteTime = "vehicle_onroute_time"
  //        case typeID = "type_id"
  //        case shoppingEnd = "shopping_end"
  //        case phone
  //        case isCanceledByConsumer = "is_canceled_by_consumer"
  //        case hailingFee = "hailing_fee"
  //        case vehicleDoorClose = "vehicle_door_close"
  //        case canceledBy = "canceled_by"
  //        case paymentStatus = "payment_status"
  //        case orderID = "order_id"
  //        case vehicleLng = "vehicle_lng"
  //        case vehicleName = "vehicle_name"
  //        case serviceFee = "service_fee"
  //        case storeID = "store_id"
  //    }

}

// MARK: - OrderItem
struct OrderItem: Codable {
  let qty: Int?
  let orderID: String?
  let tax: Double?
  let productName: String?
  let price: Double?
  let productID, id: String?
  let img: String?

  enum CodingKeys: String, CodingKey {
    case qty
    case orderID = "order_id"
    case tax
    case productName = "product_name"
    case price
    case productID = "product_id"
    case id, img
  }
  //
  //    let id, orderID, productID, productName: String?
  //    let price: Double?
  //    let qty: Int?
  //    let img: String?
  //
  //    enum CodingKeys: String, CodingKey {
  //        case id
  //        case orderID = "order_id"
  //        case productID = "product_id"
  //        case productName = "product_name"
  //        case price, qty, img
  //    }
}
