//
//  Vehicle.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 5/20/21.
//

import Foundation

// MARK: - VehicleWithName
struct VehicleWithName: Codable {
  let error: String?
  let success: Bool?
  let data: VehicleWithNameData?
}

// MARK: - DataClass
struct VehicleWithNameData: Codable {
  let name, id: String?
}
