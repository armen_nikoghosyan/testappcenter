//
//  AllZones.swift
//  Robomart
//
//  Created by Fahad Naqvi on 8/23/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import Foundation

// MARK: - AllZone

struct AllZone: Codable {
  let data: [AllZoneData]?
  let success: Bool?
  let error: String?
}

// MARK: - AllZoneData

struct AllZoneData: Codable {
  let city, country, updatedBy, coords: String?
  let name, code, updatedOn, createdBy: String?
  let id: String?
  let isActive: Bool?
  let state, createdAt: String?

  enum CodingKeys: String, CodingKey {
    case city, country
    case updatedBy = "updated_by"
    case coords, name, code
    case updatedOn = "updated_on"
    case createdBy = "created_by"
    case id
    case isActive = "is_active"
    case state
    case createdAt = "created_at"
  }
}
