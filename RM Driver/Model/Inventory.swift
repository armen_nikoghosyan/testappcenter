//
//  Inventory.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 12/3/20.
//

import Foundation

// MARK: - Inventory
struct Inventory: Codable {
  let error: String?
  let success: Bool?
  let data: InventoryData?
}

// MARK: - InventoryData
struct InventoryData: Codable {
  let updatedOn: UpdatedOn?
  let products: [InventoryProduct]?

  enum CodingKeys: String, CodingKey {
    case updatedOn = "updated_on"
    case products
  }
}

// MARK: - Product
struct InventoryProduct: Codable {
  let currentQty: Int?
  let price: Double?
  let img: String?
  let baselineQty, unionQty: Int?
  let name, productID: String?

  enum CodingKeys: String, CodingKey {
    case currentQty = "current_qty"
    case price, img
    case baselineQty = "baseline_qty"
    case unionQty = "union_qty"
    case name
    case productID = "product_id"
  }
}

//struct InventoryProduct: Codable {
//    let qty: Int?
//    let img: String?
//    let price: Double?
//    let name, code, productID, desc: String?
//    let vid: String?
//
//    enum CodingKeys: String, CodingKey {
//        case qty, img, price, name, code
//        case productID = "product_id"
//        case desc, vid
//    }
//}

// MARK: - UpdatedOn
struct UpdatedOn: Codable {
  let updatedOn: Int64?

  enum CodingKeys: String, CodingKey {
    case updatedOn = "updated_on"
  }
}
