//
//  OrderHistory.swift
//  Robomart
//
//  Created by Macbook Pro on 01/08/2019.
//  Copyright © 2019 Fahad Naqvi. All rights reserved.
//

import Foundation

// MARK: - OrderHistory

struct OrderHistory: Codable {
  let data: OrderHistoryData?
  let success: Bool?
  let error: String?
}

// MARK: - DataClass

struct OrderHistoryData: Codable {
  let recordsCount: Int?
  let records: [OrderHistoryRecord]?

  enum CodingKeys: String, CodingKey {
    case recordsCount = "RecordsCount"
    case records = "Records"
  }
}

// MARK: - Record
struct OrderHistoryRecord: Codable {
  let phone: String?
  let initialAlternateDistance: Int?
  let shoppingStart: String?
  let mapImageURL: String?
  let vehicleDoorOpen: String?
  let vehicleLng: Int?
  let typeName: String?
  let receiptNo: String?
  let storeID: String?
  let tax: Double?
  let consumerID, vehicleArrivedTime: String?
  let paymentStatus: Int?
  let isCanceledByConsumer: Bool?
  let vehicleID, transactionID, requestTime: String?
  let stripePayment: Int?
  let orderLat: Double?
  let canceledTime, vehicleDepart, requestDoorOpen: String?
  let orderLng: Double?
  let transactionError, vehicleOnrouteTime: String?
  let subtotal: Double?
  let initialEta: String?
  let itemsCount: Int?
  let items: [JSONAny]?
  let fullname, typeID: String?
  let vehicleLat: Int?
  let requestDoorClose, vehicleDoorClose, receiptGenerated: String?
  let vehicleName: String?
  let hailingFee: Int?
  let typeImageURL: String?
  let shoppingEnd, orderAssignedTime: String?
  let serviceFee: Int?
  let orderID: String?
  let statusID, initialDistance: Int?
  let status: String?
  let canceledBy: String?
  let stripeFee: Int?
  let total: Double?
  let address, cardLast4, initialAlternateEta, email: String?

  enum CodingKeys: String, CodingKey {
    case phone
    case initialAlternateDistance = "initial_alternate_distance"
    case shoppingStart = "shopping_start"
    case mapImageURL = "map_image_url"
    case vehicleDoorOpen = "vehicle_door_open"
    case vehicleLng = "vehicle_lng"
    case typeName = "type_name"
    case receiptNo = "receipt_no"
    case storeID = "store_id"
    case tax
    case consumerID = "consumer_id"
    case vehicleArrivedTime = "vehicle_arrived_time"
    case paymentStatus = "payment_status"
    case isCanceledByConsumer = "is_canceled_by_consumer"
    case vehicleID = "vehicle_id"
    case transactionID = "transaction_id"
    case requestTime = "request_time"
    case stripePayment = "stripe_payment"
    case orderLat = "order_lat"
    case canceledTime = "canceled_time"
    case vehicleDepart = "vehicle_depart"
    case requestDoorOpen = "request_door_open"
    case orderLng = "order_lng"
    case transactionError = "transaction_error"
    case vehicleOnrouteTime = "vehicle_onroute_time"
    case subtotal
    case initialEta = "initial_eta"
    case itemsCount = "items_count"
    case items, fullname
    case typeID = "type_id"
    case vehicleLat = "vehicle_lat"
    case requestDoorClose = "request_door_close"
    case vehicleDoorClose = "vehicle_door_close"
    case receiptGenerated = "receipt_generated"
    case vehicleName = "vehicle_name"
    case hailingFee = "hailing_fee"
    case typeImageURL = "type_image_url"
    case shoppingEnd = "shopping_end"
    case orderAssignedTime = "order_assigned_time"
    case serviceFee = "service_fee"
    case orderID = "order_id"
    case statusID = "status_id"
    case initialDistance = "initial_distance"
    case status
    case canceledBy = "canceled_by"
    case stripeFee = "stripe_fee"
    case total, address
    case cardLast4 = "card_last4"
    case initialAlternateEta = "initial_alternate_eta"
    case email
  }
  //    let status: String?
  //    let statusID: Int?
  //    let initialDistance: Double?
  //    let vehicleDoorOpen, transactionError, storeID, shoppingEnd: String?
  //    let requestTime, shoppingStart: String?
  //    let serviceFee: Int?
  //    let typeImageURL: String?
  //    let isCanceledByConsumer: Bool?
  //    let mapImageURL: String?
  //    let fullname: String?
  //    let typeName: String?
  //    let phone: String?
  //    let vehicleName: String?
  //    let transactionID, cardLast4: String?
  //    let stripePayment: Int?
  //    let total: Double?
  //    let stripeFee: Int?
  //    let vehicleDoorClose, orderAssignedTime, orderID: String?
  //    let orderLng, orderLat: Double?
  //    let hailingFee: Int?
  //    let typeID: String?
  //    let tax, itemsCount: Int?
  //    let vehicleID: String?
  //    let subtotal: Double?
  //    let vehicleOnrouteTime, vehicleArrivedTime, address: String?
  //    let vehicleLng: Int?
  //    let canceledTime: String?
  //    let items: [JSONAny]?
  //    let receiptNo: String?
  //    let consumerID: String?
  //    let paymentStatus, vehicleLat: Int?
  //    let canceledBy, email, vehicleDepart, initialEta: String?
  //
  //    enum CodingKeys: String, CodingKey {
  //        case status
  //        case statusID = "status_id"
  //        case initialDistance = "initial_distance"
  //        case vehicleDoorOpen = "vehicle_door_open"
  //        case transactionError = "transaction_error"
  //        case storeID = "store_id"
  //        case shoppingEnd = "shopping_end"
  //        case requestTime = "request_time"
  //        case shoppingStart = "shopping_start"
  //        case serviceFee = "service_fee"
  //        case typeImageURL = "type_image_url"
  //        case isCanceledByConsumer = "is_canceled_by_consumer"
  //        case mapImageURL = "map_image_url"
  //        case fullname
  //        case typeName = "type_name"
  //        case phone
  //        case vehicleName = "vehicle_name"
  //        case transactionID = "transaction_id"
  //        case cardLast4 = "card_last4"
  //        case stripePayment = "stripe_payment"
  //        case total
  //        case stripeFee = "stripe_fee"
  //        case vehicleDoorClose = "vehicle_door_close"
  //        case orderAssignedTime = "order_assigned_time"
  //        case orderID = "order_id"
  //        case orderLng = "order_lng"
  //        case orderLat = "order_lat"
  //        case hailingFee = "hailing_fee"
  //        case typeID = "type_id"
  //        case tax
  //        case itemsCount = "items_count"
  //        case vehicleID = "vehicle_id"
  //        case subtotal
  //        case vehicleOnrouteTime = "vehicle_onroute_time"
  //        case vehicleArrivedTime = "vehicle_arrived_time"
  //        case address
  //        case vehicleLng = "vehicle_lng"
  //        case canceledTime = "canceled_time"
  //        case items
  //        case receiptNo = "receipt_no"
  //        case consumerID = "consumer_id"
  //        case paymentStatus = "payment_status"
  //        case vehicleLat = "vehicle_lat"
  //        case canceledBy = "canceled_by"
  //        case email
  //        case vehicleDepart = "vehicle_depart"
  //        case initialEta = "initial_eta"
  //    }

}
