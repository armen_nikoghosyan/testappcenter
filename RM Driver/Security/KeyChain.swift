//
//  KeyChain.swift
//  Robomart
//
//  Created by Fahad Naqvi on 9/16/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import Foundation
import Security

// Arguments for the keychain queries
let kSecClassValue = NSString(format: kSecClass)
let kSecAttrAccountValue = NSString(format: kSecAttrAccount)
let kSecValueDataValue = NSString(format: kSecValueData)
let kSecClassGenericPasswordValue = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceValue = NSString(format: kSecAttrService)
let kSecMatchLimitValue = NSString(format: kSecMatchLimit)
let kSecReturnDataValue = NSString(format: kSecReturnData)
let kSecMatchLimitOneValue = NSString(format: kSecMatchLimitOne)

class KeyChain {
  class func save(key: userDefaultKey, data: Data) -> OSStatus {
    let query =
      [
        kSecClass as String: kSecClassGenericPassword as String,
        kSecAttrAccount as String: key.rawValue,
        kSecValueData as String: data,
      ] as [String: Any]

    SecItemDelete(query as CFDictionary)

    return SecItemAdd(query as CFDictionary, nil)
  }

  class func load(key: userDefaultKey) -> Data? {
    print("KeyChain: Key: \(key.rawValue)")

    let query =
      [
        kSecClass as String: kSecClassGenericPassword,
        kSecAttrAccount as String: key.rawValue,
        kSecReturnData as String: kCFBooleanTrue!,
        kSecMatchLimit as String: kSecMatchLimitOne,
      ] as [String: Any]

    var dataTypeRef: AnyObject?

    let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)

    print("KeyChain: status: \(status)")

    if status == noErr {
      return dataTypeRef as! Data?
    } else {
      return nil
    }
  }

  // convert string to data
  class func stringToData(string: String) -> Data {
    let _Data = Data(string.utf8)
    // ^ (string as NSString).data(using: String.Encoding.utf8.rawValue)
    return _Data
  }

  // convert data to string
  class func dataToString(data: Data) -> String {
    //        let returned_string : String = String(data: data, encoding: .utf8)! //NSString(data: data, encoding: NSUTF8StringEncoding)! as String
    let str = String(decoding: data, as: UTF8.self)
    return str
  }

  class func updatePassword(service: String, account: String, data: String) {
    if let dataFromString: Data = data.data(
      using: String.Encoding.utf8, allowLossyConversion: false)
    {
      // Instantiate a new default keychain query
      let keychainQuery = NSMutableDictionary(
        objects: [kSecClassGenericPasswordValue, service, account],
        forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue])

      let status = SecItemUpdate(
        keychainQuery as CFDictionary, [kSecValueDataValue: dataFromString] as CFDictionary)

      if status != errSecSuccess {
        if #available(iOS 11.3, *) {
          if let err = SecCopyErrorMessageString(status, nil) {
            print("Read failed: \(err)")
          }
        } else {
          // TODO: Add code or remove else section
          // Fallback on earlier versions
        }
      }
    }
  }

  class func removeItem(key: userDefaultKey) {
    //        let secItemClasses = [kSecAttrAccount, kSecClassGenericPassword, kSecClassInternetPassword, kSecClassCertificate, kSecClassKey, kSecClassIdentity]
    //        for itemClass in secItemClasses {
    //            let spec: NSDictionary = [kSecAttrAccount: itemClass]
    //            print("CHecking deletion in keychain: \(spec)")
    //            SecItemDelete(spec)
    //        }

    let query =
      [
        kSecClass as String: kSecClassGenericPassword as String,
        kSecAttrAccount as String: key.rawValue,
      ] as [String: Any]

    SecItemDelete(query as CFDictionary)

    //        // Instantiate a new default keychain query
    //        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, account, kCFBooleanTrue!], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue])
    //
    //        // Delete any existing items
    //        let status = SecItemDelete(keychainQuery as CFDictionary)
    //        if (status != errSecSuccess) {
    //            if #available(iOS 11.3, *) {
    //                if let err = SecCopyErrorMessageString(status, nil) {
    //                    print("Remove failed: \(err)")
    //                }
    //            } else {
    //                // Fallback on earlier versions
    //            }
    //        }
  }

  public class func logout() {
    let secItemClasses = [
      kSecClassGenericPassword,
      kSecClassInternetPassword,
      kSecClassCertificate,
      kSecClassKey,
      kSecClassIdentity,
      kSecAttrAccount,
    ]
    for itemClass in secItemClasses {
      let spec: NSDictionary = [kSecClass: itemClass]
      SecItemDelete(spec)
    }
  }

  class func createUniqueID() -> String {
    let uuid: CFUUID = CFUUIDCreate(nil)
    let cfStr: CFString = CFUUIDCreateString(nil, uuid)

    let swiftString: String = cfStr as String
    return swiftString
  }
}

extension Data {
  init<T>(from value: T) {
    var value = value
    //        self.init(buffer: UnsafeBufferPointer(start: &value, count: 1))
    var myData = Data()
    withUnsafePointer(to: &value) { (ptr: UnsafePointer<T>) -> Void in
      myData = Data(buffer: UnsafeBufferPointer(start: ptr, count: 1))
    }
    self.init(myData)
  }

  func to<T>(type _: T.Type) -> T {
    return withUnsafeBytes { $0.load(as: T.self) }
  }
}
