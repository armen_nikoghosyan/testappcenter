//
//  AppDelegate.swift
//  RobomartDriver
//
//  Created by Fahad Naqvi on 10/24/20.
//

import Analytics
import Firebase
import FirebaseAuth
import FirebaseCrashlytics
import FirebaseMessaging
import GoogleCloudLogging
import GoogleMaps
import GooglePlaces
import Intercom
import Logging
import UIKit
import UserNotificationsUI

#if DEBUG
  let googleAPIKey = "AIzaSyDlSO6kc-2GG1vT4cf1QgZjpgzY_tgisa8"
#else
  let googleAPIKey = "AIzaSyANREV_3AxhUMSym_HI4Gec4rRNGHo-AIQ"
#endif

var loggerEvents = Logger(label: "\(Bundle.main.bundleIdentifier!).event")
var loggerDebug = Logger(label: "\(Bundle.main.bundleIdentifier!)")

let versionNumber =
  Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String
var iosVersion = UIDevice.current.systemVersion
var build = Bundle.main.infoDictionary?["CFBundleVersion"] as! String

func getLogLevelInCaps(logLevel: String) -> String {
  return logLevel.uppercased()
}

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  let gcmMessageIDKey = "gcm.message_id"

  #if DEBUG
    // Development mode
    let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info-Dev", ofType: "plist")
  #else
    // Production mode
    let firebaseConfig = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")
  #endif

  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

    if #available(iOS 13.0, *) {
      window?.overrideUserInterfaceStyle = .light
    }

    do {
      #if DEBUG
        // Register both console and Google Logging backends
        LoggingSystem.bootstrap {
          MultiplexLogHandler([
            GoogleCloudLogHandler(label: $0),
            StreamLogHandler.standardOutput(label: $0),
          ])
        }

        try GoogleCloudLogHandler.setup(
          serviceAccountCredentials: Bundle.main.url(
            forResource: "dev-robomart-0a1c2f37ef00", withExtension: "json")!,
          clientId: UIDevice.current.identifierForVendor)
      #else
        // Register only Google Logging backend
        LoggingSystem.bootstrap(GoogleCloudLogHandler.init)

        try GoogleCloudLogHandler.setup(
          serviceAccountCredentials: Bundle.main.url(
            forResource: "robomart-1459c-ad5c58c5eee0", withExtension: "json")!,
          clientId: UIDevice.current.identifierForVendor)
      #endif

    } catch {
      // Log GoogleCloudLogHandler setup error
      loggerDebug.error("Message: \(error.localizedDescription)")
    }

    // Set the default log levels
    loggerDebug.logLevel = .trace
    loggerEvents.logLevel = .debug

    // Set global metadata
    loggerDebug[metadataKey: "deviceId"] = "\(UIDevice.current.identifierForVendor!.uuidString)"
    loggerDebug[metadataKey: "deviceType"] = "\(UIDevice.current.type)"
    loggerDebug[metadataKey: "osName"] = "iOS"
    loggerDebug[metadataKey: "osVersion"] = "\(iosVersion)"
    loggerDebug[metadataKey: "appName"] = "\(appName!)"
    loggerDebug[metadataKey: "appVersion"] = "\(versionNumber)"
    loggerDebug[metadataKey: "appBuildNumber"] = "\(build)"

    loggerEvents[metadataKey: "deviceId"] = "\(UIDevice.current.identifierForVendor!.uuidString)"
    loggerEvents[metadataKey: "deviceType"] = "\(UIDevice.current.type)"
    loggerEvents[metadataKey: "osName"] = "iOS"
    loggerEvents[metadataKey: "osVersion"] = "\(iosVersion)"
    loggerEvents[metadataKey: "appName"] = "\(appName!)"
    loggerEvents[metadataKey: "appVersion"] = "\(versionNumber)"
    loggerEvents[metadataKey: "appBuildNumber"] = "\(build)"

    // TODO: Move to later stage if AppUser class is used

    // Configure firebase
    guard let options = FirebaseOptions(contentsOfFile: firebaseConfig!) else {
      loggerDebug.error("Invalid Firebase configuration file.")
      fatalError("Invalid Firebase configuration file.")
    }

    FirebaseApp.configure(options: options)

    if UserDefaults.standard.bool(forKey: "loggedIn") {
      if let user = Auth.auth().currentUser {
        loggerDebug[metadataKey: "userName"] = "\(user.displayName!)"
        // loggerDebug[metadataKey: "userPhoneNumber"] = "\(user.phoneNumber!)"
        loggerDebug[metadataKey: "userEmail"] = "\(user.email!)"

        loggerEvents[metadataKey: "userName"] = "\(user.displayName!)"
        // loggerEvents[metadataKey: "userPhoneNumber"] = "\(user.phoneNumber!)"
        loggerEvents[metadataKey: "userEmail"] = "\(user.email!)"
      }

    } else {
      loggerDebug.error("User is not signed in.")
    }

    // Initiate notification permission prompt
    notificationPermissionAlert(application: application)

    // Google maps keys
    GMSServices.provideAPIKey(googleAPIKey)
    GMSPlacesClient.provideAPIKey(googleAPIKey)

    // Check if user logged in already
    guard let isUserLoggedIn: Bool = AppUser.sharedInstance.getData(forKey: .loggedIn) else {
      loggerDebug.debug("User isn't logged in")
      return false
    }

    // signOutUser(fromMobile: true)
    setupUserInfoIntoCrashlytics()

    if isUserLoggedIn {
      showMainScreen()
    } else {
      showLoginScreen()
    }

    return true
  }

  func application(_: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)

    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      loggerDebug.info("Message ID: \(messageID)")
    }

    if Auth.auth().canHandleNotification(userInfo) {
      loggerDebug.info("\(userInfo)")
      return
    }

    // Log full message
    loggerDebug.info("AppDelegate: didReceiveRemoteNotification: \(userInfo)")
  }

  func application(
    _: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
    fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void
  ) {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)

    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      loggerDebug.info("Message ID: \(messageID)")
    }

    // Print full message.
    loggerDebug.info("AppDelegate: fetchCompletionHandler: \(userInfo)")

    completionHandler(UIBackgroundFetchResult.newData)
  }

  private func application(
    application _: UIApplication,
    didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
  ) {
    // At development time we use .sandbox
    // Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.unknown)

    loggerDebug.info("DeviceToken: \(deviceToken)")
    Messaging.messaging().apnsToken = deviceToken

    // Set device token for push notifications.
    // Intercom.setDeviceToken(deviceToken)

    getFCMToken { (token: String?) in
      loggerDebug.info("Checking token in AppDelegate: \(token ?? "nil")")
    }
  }

  func application(
    _: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
  ) {
    loggerDebug.info(
      "AppDelegate: didRegisterForRemoteNotificationsWithDeviceToken: \(deviceToken)"
    )

    // At development time we use .sandbox
    // Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)

    let bundleID = Bundle.main.bundleIdentifier

    loggerDebug.info("bundle id of app: \(String(describing: bundleID))")

    Messaging.messaging().apnsToken = deviceToken

    // Set device token for push notifications.
    // Intercom.setDeviceToken(deviceToken)

    getFCMToken { (token: String?) in
      loggerDebug.info("Checking token in AppDelegate: \(token ?? "nil")")
    }
  }

  func application(_: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
  {
    loggerDebug.info(
      "AppDelegate: didFailToRegisterForRemoteNotificationsWithError: \(error.localizedDescription)"
    )
  }

  func applicationDidEnterBackground(_: UIApplication) {
    loggerEvents.info("app_close")
    GoogleCloudLogHandler.upload()
  }

  func applicationWillEnterForeground(_: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    // Upload logs to Google Cloud
    GoogleCloudLogHandler.upload()

    loggerEvents.info("open_app")
    loggerDebug.debug("Application is moved to foreground.")

    // Upload logs to Google Cloud
    GoogleCloudLogHandler.upload()
  }

  func applicationWillTerminate(_: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Upload logs to Google Cloud
    GoogleCloudLogHandler.upload()

    loggerEvents.info("force_close_app")

    // Upload logs to Google Cloud
    GoogleCloudLogHandler.upload()

  }
}

extension AppDelegate {
  // Notification permission prompt
  private func notificationPermissionAlert(application: UIApplication) {
    if #available(iOS 10.0, *) {
      // For iOS 10 display notification (sent via APNS)
      UNUserNotificationCenter.current().delegate = self

      let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
      // UNUserNotificationCenter.current().requestAuthorization(
      //     options: authOptions,
      //     completionHandler: {_, _ in })

      UNUserNotificationCenter.current().requestAuthorization(options: authOptions) {
        granted, _ in
        // Add your own
        // UNUserNotificationCenter.current().delegate = self

        if !granted {
          let alertController = UIAlertController(
            title: "Notification Alert", message: "Please enable notifications",
            preferredStyle: .alert)
          let settingsAction = UIAlertAction(title: "Settings", style: .default) { _ -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
              loggerDebug.warning("Settings URL is nil")
              return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
              UIApplication.shared.open(
                settingsUrl,
                completionHandler: { _ in
                })
            }
          }
          let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
          alertController.addAction(cancelAction)
          alertController.addAction(settingsAction)
          DispatchQueue.main.async {
            self.window?.rootViewController?.present(
              alertController, animated: true, completion: nil)
          }
        }
      }

    } else {
      let settings =
        UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
      application.registerUserNotificationSettings(settings)
    }

    Messaging.messaging().delegate = self
    application.registerForRemoteNotifications()
  }

  // Redirect to login screen if user is not logged in
  func showLoginScreen() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let loginViewController: LoginVC =
      storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC

    var navigationController = UINavigationController()
    navigationController = UINavigationController(rootViewController: loginViewController)

    // It removes all view controllers from the navigation controller then sets the new root view controller and it pops.
    window?.rootViewController = navigationController

    // Navigation bar is hidden
    navigationController.isNavigationBarHidden = true
  }

  // Redirect to main page if user exist
  func showMainScreen() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)

    // let dashboardViewController: MainVC = storyboard.instantiateViewController(withIdentifier: "MainVC") as! MainVC

    // var navigationController = UINavigationController()
    // navigationController = UINavigationController(rootViewController: dashboardViewController)

    if let vc = storyboard.instantiateViewController(withIdentifier: "MainNav")
      as? UINavigationController
    {
      window?.rootViewController = vc

      // Navigation bar is hidden
      // vc.isNavigationBarHidden = true
    }
  }
}

extension AppDelegate: MessagingDelegate {
  func messaging(_: Messaging, didReceiveRegistrationToken fcmToken: String?) {
    loggerDebug.info("Firebase registration token: \(String(describing: fcmToken))")

    let dataDict: [String: String] = ["token": fcmToken!]
    NotificationCenter.default.post(
      name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.

    Messaging.messaging().token { token, error in
      if let error = error {
        loggerDebug.error("Error fetching FCM registration token: \(error)")
      } else if let token = token {
        loggerDebug.trace("FCM registration token: \(token)")
        // Saving token to keychain
        let token = KeyChain.stringToData(string: token)
        _ = KeyChain.save(key: .fcmtoken, data: token)
      }
    }
    //
    //        InstanceID.instanceID().instanceID { result, error in
    //            if let error = error {
    //                logger.error("Error fetching remote instance ID: \(error)")
    //            } else if let result = result {
    //                logger.info("Remote instance ID token: \(result.token)")
    //
    //
    //
    //
    //            }
    //        }
  }
}

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {
  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(
    _: UNUserNotificationCenter,
    willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
  ) {
    let userInfo = notification.request.content.userInfo

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)

    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      loggerDebug.info("Message ID: \(messageID)")
    }

    // Print full message.
    loggerDebug.info("\(userInfo)")

    // Change this to your preferred presentation option
    completionHandler([.alert, .badge, .sound])
  }

  func userNotificationCenter(
    _: UNUserNotificationCenter,
    didReceive response: UNNotificationResponse,
    withCompletionHandler completionHandler: @escaping () -> Void
  ) {
    let userInfo = response.notification.request.content.userInfo
    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      loggerDebug.info("Message ID: \(messageID)")
    }

    // Print full message.
    loggerDebug.info("\(userInfo)")

    completionHandler()
  }

  func setupUserInfoIntoCrashlytics() {
    if let user = Auth.auth().currentUser {
      let keysAndValues =
        [
          "clientId": UIDevice.current.identifierForVendor as Any,
          "userName": user.displayName as Any,
          "userId": user.uid as Any,
          "userEmail": user.email as Any,
          "userPhone": user.phoneNumber as Any,
        ] as [String: Any]

      Crashlytics.crashlytics().setCustomKeysAndValues(keysAndValues)
    }
  }
}
extension UIApplication {

  class func getTopViewController(
    base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
  ) -> UIViewController? {

    if let nav = base as? UINavigationController {
      return getTopViewController(base: nav.visibleViewController)

    } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
      return getTopViewController(base: selected)

    } else if let presented = base?.presentedViewController {
      return getTopViewController(base: presented)
    }
    return base
  }

}
