//
//  APIManager.swift
//  sebs-list
//
//  Created by Fahad Naqvi on 19/06/2019.
//  Copyright © 2019 Fahad Naqvi. All rights reserved.
//

import Alamofire
import FirebaseAuth
import Foundation
import SwiftyJSON
import UIKit

typealias JsonParam = [String: Any]
typealias ServiceResponse = (Data?, NSError?) -> Void
// typealias ServiceResponse = (result<poll>) -> Void

class APIManager: NSObject {
  static let sharedInstance = APIManager()

  // Get token for robomart api
  let token = UserDefaults.standard.value(forKey: "token") as? String ?? "0"

  // MARK: Post Method

  func post(endpoint: EndPoints, parameters: JsonParam?, completion: @escaping ServiceResponse) {
    let fullUrl = baseURL + endpoint.path

    loggerDebug.trace("FullURL: \(fullUrl)")

//    var token2: String? = "0"
    // ^ = AppUser.sharedInstance.getData(forKey: .token) ?? "0" //UserDefaults.standard.value(forKey: "token") as? String ?? "0"
//    if let tokenNew = KeyChain.load(key: .token) {
//      let tok = KeyChain.dataToString(data: tokenNew)
//      token2 = tok
//    }

    let vehicleID: String? = AppUser.sharedInstance.getData(forKey: .vehicleID)

    let para = parameters ?? [:]

    var header: HTTPHeaders?

    AppUser.sharedInstance.getFirebaseAuthIDToken { success, fbToken in

      if success! {
        loggerDebug.debug("FBToken test: \(fbToken!)")
        if let vid = vehicleID {
          header = ["vehicle_id": "\(vid)", "fbtoken": fbToken!]
        } else {
          header = ["vehicle_id": "", "fbtoken": fbToken!]
        }

      } else {
        loggerDebug.error("Could not get FBToken.")
        header = ["vehicle_id": "\(vehicleID!)", "fbtoken": ""]
      }

      Alamofire.request(
        fullUrl, method: .post, parameters: para, encoding: JSONEncoding.default, headers: header!
      ).responseJSON { response in
        // handle JSON
        switch response.result {
        case .success:
          completion(response.data, nil)
        case .failure(let error):
          if error.localizedDescription.contains("http") {
            let invalidUrl = NSError(domain: "Invalid URL", code: 400, userInfo: [:])
            completion(nil, invalidUrl)
            loggerDebug.error("Invalid URL")
          } else {
            completion(nil, error as NSError?)
            loggerDebug.error("\(error)")
          }
        }
      }
    }
  }

  // MARK: Post with JSON Body Method

  func postWithJsonBody(
    endpoint: EndPoints, parameters: JsonParam?, completion: @escaping ServiceResponse
  ) {
    let fullUrl = baseURL + endpoint.path
    let para = parameters ?? [:]
    let header: HTTPHeaders = ["token": token]
    #if DEBUG
      loggerDebug.trace("\(fullUrl)")
    #endif

    Alamofire.request(
      fullUrl, method: .post, parameters: para, encoding: URLEncoding.httpBody, headers: header
    ).responseJSON { response in
      // handle JSON
      switch response.result {
      case .success:
        completion(response.data, nil)
      case .failure(let error):
        if error.localizedDescription.contains("http") {
          let invalidUrl = NSError(domain: "Invalid URL", code: 400, userInfo: [:])
          completion(nil, invalidUrl)
          loggerDebug.error("Invalid URL")
        } else {
          completion(nil, error as NSError?)
          loggerDebug.error("\(error)")

        }
      }
    }
  }

  // MARK: Get Method

  func get(endpoint: EndPoints, completion: @escaping ServiceResponse) {
    let fullUrl = baseURL + endpoint.path

    // Get vehicle id from user default
    let vehicleID: String? = AppUser.sharedInstance.getData(forKey: .vehicleID)

    var header: HTTPHeaders?

    // NOTE: Prints url for call
    loggerDebug.trace("service URL : \(fullUrl)")

    // Get firebase id token
    AppUser.sharedInstance.getFirebaseAuthIDToken { success, fbToken in

      if success! {
        //                logger.info("APIManager get: fbtoken: \(fbToken)")
        if let vid = vehicleID {
          header = ["vehicle_id": "\(vid)", "fbtoken": fbToken!]
        } else {
          header = ["vehicle_id": "", "fbtoken": fbToken!]
        }

      } else {
        loggerDebug.error("Could not get FBToken.")
        header = ["vehicle_id": "\(vehicleID ?? "vehicle_id is nil")", "fbtoken": ""]
      }

      // logger.error("Checking header in get: \(header)")

      Alamofire.request(fullUrl, method: .get, headers: header!)
        .responseJSON { response in
          switch response.result {
          case .success:
            loggerDebug.debug("\(response)")
            completion(response.data, nil)
          case .failure(let error):
            if error.localizedDescription.contains("http") {
              let invalidUrl = NSError(domain: "Invalid URL", code: 400, userInfo: [:])
              completion(nil, invalidUrl)
              loggerDebug.error("Invalid URL")
            } else {
              completion(nil, error as NSError?)
              loggerDebug.error("\(error)")
            }
          }
        }
    }
  }

  // MARK: Delete Method

  func delete(endpoint: EndPoints, parameters: JsonParam?, completion: @escaping ServiceResponse) {
    let fullUrl = baseURL + endpoint.path

    // NOTE: Prints url for call
    loggerDebug.trace("service URL : \(fullUrl)")

//    var token2: String? = "0"
    // AppUser.sharedInstance.getData(forKey: .token) ?? "0"

//    if let tokenNew = KeyChain.load(key: .token) {
//      let tok = KeyChain.dataToString(data: tokenNew)
//      token2 = tok
//    }

    // let header: HTTPHeaders = ["token": token2!]
    var header: HTTPHeaders?

    // Get firebase id token
    AppUser.sharedInstance.getFirebaseAuthIDToken { success, fbToken in

      if success! {
        loggerDebug.notice("APIManager delete: fbtoken: \(fbToken!)")
        header = ["fbtoken": fbToken!]

      } else {
        loggerDebug.error("Could not get FBToken.")
        header = ["fbtoken": ""]
      }

      Alamofire.request(fullUrl, method: .delete, headers: header!)
        .responseJSON { response in
          switch response.result {
          case .success:

            do {
              let json = try JSON(data: response.data!)
              loggerDebug.trace("\(json)")

            } catch let jsonError {
              loggerDebug.error("\(jsonError.localizedDescription)")
            }

            completion(response.data, nil)
          case .failure(let error):
            if error.localizedDescription.contains("http") {
              let invalidUrl = NSError(domain: "Invalid URL", code: 400, userInfo: [:])
              completion(nil, invalidUrl)
              loggerDebug.error("Invalid URL")
            } else {
              completion(nil, error as NSError?)
              loggerDebug.error("\(error)")
            }
          }
        }

      // Alamofire.request(fullUrl, method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: header!).responseJSON { response in
      //     // handle JSON
      //     switch response.result {
      //     case .success:
      //
      //         do {
      //             let json = try JSON(data: response.data!)
      //             logger.info("\(json)")
      //
      //         }catch let jsonError {
      //             logger.error("\(jsonError.localizedDescription)")
      //         }
      //
      //         completion(response.data, nil)
      //     case let .failure(error):
      //         if error.localizedDescription.contains("http") {
      //             let invalidUrl = NSError(domain: "Invalid URL", code: 400, userInfo: [:])
      //             completion(nil, invalidUrl)
      //
      //         } else {
      //             completion(nil, error as NSError?)
      //         }
      //     }
      // }
    }
  }

  func apiRequest<T: Decodable>(
    endpoint: EndPoints, method: HTTPMethod, encoding: ParameterEncoding, params: Parameters?,
    completion: @escaping (T) -> Void
  ) {
    let fullUrl = baseURL + endpoint.path

    // get token for robomart api
    // var token2: String? = AppUser.sharedInstance.getData(forKey: .token) ?? "0"

    var token2: String = "0"

    // if let tokenNew = KeyChain.load(key: .token) {
    //     let tok = KeyChain.dataToString(data: tokenNew)
    //     token2 = tok
    // }

    if let tokenNew = KeyChain.load(key: .token) {
      let tok = KeyChain.dataToString(data: tokenNew)
      token2 = tok
    }

    loggerDebug.trace("Get Token : \(token2)")

    // NOTE: Prints url for call
    loggerDebug.trace("Service URL : \(fullUrl)")

    var header: HTTPHeaders?

    // Get firebase id token
    // let fbToken: String? = AppUser.sharedInstance.getFirebaseAuthIDToken()

    AppUser.sharedInstance.getFirebaseAuthIDToken { success, fbToken in

      if success! {
        loggerDebug.trace("Get firebase Token : \(fbToken!)")

        header = ["token": token2, "fbtoken": fbToken!]

      } else {
        loggerDebug.error("Could not get FBToken.")
        header = ["token": token2, "fbtoken": ""]
      }

      Alamofire.request(
        fullUrl, method: method, parameters: params!, encoding: encoding, headers: header!
      ).response { response in

        do {
          let json = try JSON(data: response.data!)
          loggerDebug.trace("\(json)")

          if let dataObj = try? JSONDecoder().decode(T.self, from: response.data!) {
            completion(dataObj)
          } else {
            loggerDebug.error("Something went wrong!")
          }

        } catch let jsonError {
          DispatchQueue.main.async {
            loggerDebug.error("Failed to fetch data : \(jsonError)")
          }
        }
      }
    }
  }

  func uploadFiles<T: Decodable>(
    endpoint: EndPoints, files: [AnyObject?]?, fileName _: String?,
    completion: @escaping (T?, Error?) -> Void
  ) {
    // let token = UserDefaults.standard.value(forKey: "token") as? String ?? "0"

    // let url = "\(baseURL)/feed/AddPollResponse"

    // let token: String? = AppUser.sharedInstance.getData(forKey: .token) ?? "0"

    var token2: String? = "0"

    if let tokenNew = KeyChain.load(key: .token) {
      let tok = KeyChain.dataToString(data: tokenNew)
      token2 = tok
      loggerDebug.trace("User Token saved after : \(tok)")
    }

    let fullUrl = baseURL + endpoint.path
    let header: HTTPHeaders = ["token": token2!]

    let params: Parameters = ["upload_file[]": "uploaded file"]

    Alamofire.upload(
      multipartFormData: { (multipartFormData: MultipartFormData) in

        let count = files!.count

        for i in 0..<count {
          loggerDebug.trace("Images count : \(i)")

          if let img = files![i]! as? UIImage {
            if let imgData = img.jpegData(compressionQuality: 0.6) {
              let imageSize: Int = imgData.count
              loggerDebug.debug("Uploading size of image in KB: %f \(Double(imageSize) / 1000.0)")

              multipartFormData.append(
                imgData, withName: "morephoto[\(i)]", fileName: "photo\(i).jpg",
                mimeType: "image/jpeg")
            }

          } else if let fileURL = files![i]! as? URL {
            // == "application/pdf"

            var ext = ""
            var mimeType = ""
            let str = "\(fileURL)"
            var fileData: Data?

            do {
              fileData = try Data(contentsOf: fileURL)
              if str.contains("pdf") {
                loggerDebug.debug("Document Check : PDF")
                ext = "pdf"
                mimeType = "application/pdf"
              } else if str.contains("xls") || str.contains("xlsx") {
                loggerDebug.debug("Document Check : XLS")

                if str.contains("xlsx") {
                  // ext = "msword"
                  ext = "xlsx"
                } else {
                  ext = "xls"
                }

                // ext = "vnd.ms-excel"
                mimeType = "application/vnd.ms-excel"

              } else if str.contains("doc") || str.contains("docx") {
                loggerDebug.debug("Document Check : DOC")

                if str.contains("docx") {
                  // ext = "msword"
                  ext = "docx"
                } else {
                  ext = "doc"
                }

                mimeType = "application/msword"
              } else if str.contains("ppt") || str.contains("pptx") {
                loggerDebug.debug("Document Check : PPT")

                if str.contains("pptx") {
                  // ext = "msword"
                  ext = "pptx"
                } else {
                  ext = "ppt"
                }

                mimeType = "application/vnd.ms-powerpoint"

              } else if str.contains("MOV") {
                loggerDebug.debug("Document Check : MOV")

                ext = "mp4"
                mimeType = "video/mp4"
              }

              multipartFormData.append(
                fileData!, withName: "morefile[\(i)]", fileName: "file\(i).\(ext)",
                mimeType: "\(mimeType)")
            } catch let thisError {
              loggerDebug.warning("Error: could not upload file - \(thisError)")
            }
          }
        }

        for (key, value) in params {
          multipartFormData.append(
            (value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
        }
        loggerDebug.debug("multipartFormData before upload : \(multipartFormData)")

      },
      to: fullUrl,
      method: .post,
      headers: header
    ) { result in

      switch result {
      case .success(let upload, _, _):
        loggerDebug.debug("Before upload success!! \(upload)")

        upload.uploadProgress(closure: { progress in
          loggerDebug.debug("Upload Progress: \(progress.fractionCompleted)")

          if progress.fractionCompleted == 1.0 {}

        })
        upload.responseJSON { response in
          if let error = response.error {
            loggerDebug.error("Upload Error : \(error.localizedDescription)")
            completion(nil, error.localizedDescription as? Error)

          } else {
            do {
              _ = try JSON(data: response.data!)

              let data = try? JSONDecoder().decode(T.self, from: response.data!)
              completion(data, nil)

            } catch let jsonError {
              completion(nil, jsonError.localizedDescription as? Error)
              loggerDebug.error("Error : \(jsonError.localizedDescription)")
            }
          }
        }

      case .failure(let encodingError):
        loggerDebug.error("Error : \(encodingError)")
      // completion(encodingError)
      }
    }
  }

  func addZone() {

  }
}
