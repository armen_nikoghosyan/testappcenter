//
//  EndPoints.swift
//  sebs-list
//
//  Created by Fahad Naqvi on 19/06/2019.
//  Copyright © 2019 Fahad Naqvi. All rights reserved.
//

import UIKit

#if DEBUG
  // version 1.0 API
  // let baseURL = "https://dev.dashboard.robomart.co/v1/api/"

  // version 2.0 API
  //let baseURL = "https://dev.dashboard.robomart.co/v2/api/"

  // version 3.0 API
  let baseURL = "https://dev.api.robomart.co/v3/api/"

#else
  let baseURL = "https://dashboard.robomart.co/v2/api/"
#endif

enum EndPoints {
  case getCustomToken
  case startEnRoute
  case setRobomartStatus
  case openAndCloseDoors
  case getVehicleTempBasket
  case confirmReceipt
  case cancelByDriver(consumerID: String?, orderID: String?)
  case getInventory
  case getBaselineInventory
  case getInventoryCounts
  case getAssignedRobomrt
  case getOrder(id: String?)
  case updatePushNotificationToken
  case getAllForDriver(page: Int?)
  case getAssignedRobomrtWithName(id: String?)
  case getAllZonesForDriverApp

  var path: String {
    switch self {
    case .getCustomToken: return "User/GetCustomToken?requestedRight=1"
    case .startEnRoute: return "Robomart/StartRoute"
    case .setRobomartStatus: return "Robomart/SetRobomartStatus"
    case .openAndCloseDoors: return "Robomart/OpenAndCloseDoors"
    case .getVehicleTempBasket: return "Robomart/GetVehicleTempBasket"
    case .confirmReceipt: return "Robomart/confirmReceipt"
    case .cancelByDriver(let userID, let orderID):
      return "order/CancelByDriver/\(userID!)/\(orderID!)"
    case .getInventory: return "Robomart/GetInventory"
    case .getBaselineInventory: return "Robomart/GetBaselineInventory"
    case .getInventoryCounts: return "Robomart/GetInventoryCounts"
    case .getAssignedRobomrt: return "Robomart/GetAssignedRobomrt"
    case .getOrder(let id): return "Robomart/GetOrder?order_id=\(id!)"
    case .updatePushNotificationToken: return "user/UpdatePushNotificationToken"
    case .getAllForDriver(let page): return "Order/GetAllForDriver?page=\(page!)"
    case .getAssignedRobomrtWithName(let id):
      return "Robomart/GetAssignedRobomrtWithName?vehicle_id=\(id!)"
    case .getAllZonesForDriverApp: return "Order/GetAllZonesForDriverApp"
    }
  }
}
