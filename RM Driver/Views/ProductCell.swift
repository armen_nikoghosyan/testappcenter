//
//  ProductCell.swift
//  Robomart
//
//  Created by Macbook Pro on 28/06/2019.
//  Copyright © 2019 Fahad Naqvi. All rights reserved.
//

import Kingfisher
import UIKit

class ProductCell: UITableViewCell {

  let baselineLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0, text: "")
  let productName = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Bananas")
  let productImage = Components.shared.customImageView(
    cornerRadius: 15, clipToBounds: true, contentMode: .scaleAspectFit)
  let productPriceLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0,
    text: "$500.00")
  let productQuantityLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "100"
  )

  var receiptItem: ReceiptItemData! {
    didSet {

      if let productImageURL = URL(string: getCorrectPath(path: receiptItem.img!)) {
        productImage.kf.indicatorType = .activity
        productImage.kf.setImage(with: productImageURL)
      } else {
        productImage.image = UIImage(named: "produce")
      }

      productName.text = receiptItem.productName!
      productPriceLabel.text = "$\(receiptItem.price!)"
      productQuantityLabel.text = "\(receiptItem.qty!)"
    }
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundColor = .white
    selectionStyle = .none
    separatorInset = .zero
    layoutMargins = .zero

    addSubview(baselineLabel)
    baselineLabel.textColor = .init(white: 0.5, alpha: 1)
    baselineLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    baselineLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
    baselineLabel.widthAnchor.constraint(equalToConstant: 30).isActive = true

    addSubview(productImage)
    productImage.image = UIImage(named: "produce")
    productImage.leadingAnchor.constraint(equalTo: baselineLabel.trailingAnchor, constant: 5)
      .isActive = true
    productImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    productImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
    productImage.heightAnchor.constraint(equalToConstant: 40).isActive = true

    addSubview(productName)
    productName.textColor = .init(white: 0.5, alpha: 1)
    productName.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    productName.leadingAnchor.constraint(equalTo: productImage.trailingAnchor, constant: 10)
      .isActive = true
    productName.widthAnchor.constraint(equalToConstant: 120).isActive = true

    addSubview(productPriceLabel)
    productPriceLabel.textColor = .init(white: 0.5, alpha: 1)
    productPriceLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    productPriceLabel.leadingAnchor.constraint(equalTo: productName.trailingAnchor, constant: 10)
      .isActive = true

    addSubview(productQuantityLabel)
    productQuantityLabel.textColor = .init(white: 0.5, alpha: 1)
    productQuantityLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive =
      true
    productQuantityLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive =
      true

    //        ProductPriceLabel.trailingAnchor.constraint(equalTo: ProductQuantityLabel.leadingAnchor, constant: -10).isActive = true

    //        ProductName.trailingAnchor.constraint(equalTo: ProductQuantityLabel.leadingAnchor, constant: -10).isActive = true
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
