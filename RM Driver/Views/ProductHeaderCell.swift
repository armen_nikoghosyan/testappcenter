//
//  ProductHeaderCell.swift
//  Robomart
//
//  Created by Fahad Naqvi on 2/17/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import UIKit

class ProductHeaderCell: UITableViewCell {
  let titleLabel = Components.shared.customLabel(
    fontSize: 22, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0,
    text: "Basket")
  let subTitleLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0,
    text: "Candy Bar, Carrot Chips and Cheese")
  let lastPingLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextMedium, fontAlignment: .center, numberOfLines: 0,
    text: "Last ping 0 second(s) ago")

  let productImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.image = UIImage(named: "produce")
    imageView.layer.cornerRadius = 50
    imageView.layer.borderColor = UIColor(white: 0.8, alpha: 1).cgColor
    imageView.layer.borderWidth = 1
    imageView.clipsToBounds = true

    //        imagealpha = 0

    return imageView
  }()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundColor = .white
    selectionStyle = .none
    separatorInset = .zero
    layoutMargins = .zero

    addSubview(titleLabel)
    titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -8).isActive = true

    titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30).isActive = true
    titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30).isActive = true

    addSubview(lastPingLabel)
    lastPingLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 0).isActive =
      true
    lastPingLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 0).isActive =
      true

  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
