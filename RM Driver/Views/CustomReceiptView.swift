//
//  CustomOverlayPrompt.swift
//  Robomart
//
//  Created by Fahad Naqvi on 4/9/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

@objc protocol CustomReceiptViewDelegate {
  @objc optional func didConfirm()
}

class CustomReceiptView: UIView {

  private let productCellId = "productCellId"
  private let productHeaderCellId = "productHeaderCellId"
  private let shimmerReceiptItemCellId = "shimmerReceiptItemCellId"

  static let shared = CustomReceiptView()

  var delegate: CustomReceiptViewDelegate?

  let blackBgView = UIView()
  let contentView = Components.shared.customView()
  let titleLabel = Components.shared.customLabel(
    fontSize: 22, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Basket")
  let emptyTableLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "No Items")
  let lastPingLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextMedium, fontAlignment: .center, numberOfLines: 0,
    text: "Last ping 0 second(s)")

  var contentViewBottomConstraints: NSLayoutConstraint?

  let productListTV = Components.shared.customUITableView()
  var productListArray = [ReceiptItemData]()

  var timer: Timer?

  override init(frame: CGRect) {
    super.init(frame: frame)

    setupUI()
    setupTV()
    blackBgView.isHidden = true
    contentView.isHidden = true

  }

  // Setup tableview
  func setupTV() {

    productListTV.register(ProductCell.self, forCellReuseIdentifier: productCellId)
    productListTV.register(ProductHeaderCell.self, forCellReuseIdentifier: productHeaderCellId)
    productListTV.register(
      ShimmerReceiptItemCell.self, forCellReuseIdentifier: shimmerReceiptItemCellId)

    productListTV.delegate = self
    productListTV.dataSource = self

    productListTV.separatorStyle = .none
    productListTV.keyboardDismissMode = .interactive

    contentView.addSubview(productListTV)
    productListTV.backgroundColor = .white
    productListTV.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0).isActive = true
    productListTV.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10)
      .isActive = true
    productListTV.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10)
      .isActive = true
    productListTV.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0).isActive =
      true

    // ProductListTV.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)

  }

  func setupUI() {
    addSubview(blackBgView)
    blackBgView.alpha = 0
    blackBgView.backgroundColor = UIColor(white: 0, alpha: 0.5)
    blackBgView.translatesAutoresizingMaskIntoConstraints = false
    blackBgView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
    blackBgView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
    blackBgView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
    blackBgView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true

    addSubview(contentView)
    contentView.backgroundColor = .white
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.layer.cornerRadius = 10

    contentViewBottomConstraints = contentView.bottomAnchor.constraint(
      equalTo: bottomAnchor, constant: 350)
    contentViewBottomConstraints?.isActive = true

    contentView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
    contentView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
    contentView.heightAnchor.constraint(equalToConstant: 400).isActive = true

    contentView.addSubview(titleLabel)
    titleLabel.isHidden = false
    titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 17).isActive = true
    titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 0).isActive =
      true
    // titleLabel.heightAnchor.constraint(equalToConstant: 80).isActive = true

    contentView.addSubview(lastPingLabel)
    lastPingLabel.isHidden = false
    lastPingLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 0).isActive =
      true
    lastPingLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 0).isActive =
      true
    contentView.addSubview(emptyTableLabel)
    emptyTableLabel.isHidden = true
    emptyTableLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: 0)
      .isActive = true
    emptyTableLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 0)
      .isActive = true

  }

  // Get receipt items from server
  @objc private func getReceiptItems() {

    DispatchQueue.main.async {
      APIManager.sharedInstance.get(endpoint: .getVehicleTempBasket) { (res, error) in

        if let err = error {
          loggerDebug.error("Error: \(err.localizedDescription)")
        } else {

          do {

            let json = try JSON(data: res!)
            loggerDebug.trace("basket : \(json)")

            let response = try JSONDecoder().decode(ReceiptItems.self, from: res!)

            // show items in tableview if receipt items count is > 0
            if response.data!.count > 0 {
              self.productListArray = response.data!
              self.productListTV.reloadData()
              self.productListTV.isHidden = false
              self.titleLabel.isHidden = true
              self.emptyTableLabel.isHidden = true
              self.lastPingLabel.isHidden = true
            } else {

              self.productListTV.isHidden = true
              self.titleLabel.isHidden = false
              self.emptyTableLabel.isHidden = false
              self.lastPingLabel.isHidden = false

            }

          } catch let jsonError {

            loggerDebug.error("\(jsonError.localizedDescription)")

          }

        }

      }

      // self.GetInventory(endpoint: .getInventoryCounts) { (success, inventory) in
      //
      //     if !success! {
      //         logger.error("Inventory error.")
      //     }
      //
      // }
    }

  }

  // Get inventory
  private func getInventory(
    endpoint: EndPoints, completion: @escaping (Bool?, [InventoryProduct]?) -> Void
  ) {

    APIManager.sharedInstance.get(endpoint: endpoint) { (res, error) in

      if let err = error {

        loggerDebug.error("\(err.localizedDescription)")
        // AlertService.shared.alert(in: self, message: "\(err.localizedDescription)")
        completion(false, nil)

      } else {

        do {

          let json = try JSON(data: res!)
          loggerDebug.trace("GetInventory: \(json)")

          // converting json data into model
          let response = try JSONDecoder().decode(Inventory.self, from: res!)

          if response.success! {

            if endpoint.path == EndPoints.getInventoryCounts.path {

              let indexPath = IndexPath(item: 0, section: 0)
              let currentCell = self.productListTV.cellForRow(at: indexPath) as! ProductHeaderCell

              currentCell.lastPingLabel.text =
                "Last ping \(response.data!.updatedOn!.updatedOn!) second(s) ago"
              self.lastPingLabel.text =
                "Last ping \(response.data!.updatedOn!.updatedOn!) second(s) ago"

              let product = response.data!.products!

              if self.productListArray.count > 0 {
                for i in 0..<self.productListArray.count {
                  if product.firstIndex(where: {
                    $0.productID! == self.productListArray[i].productID!
                  }) != nil {

                    // let productIndexPath = IndexPath(item: i, section: 1)
                    // let productCell = self.ProductListTV.cellForRow(at: productIndexPath) as! ProductCell
                    //
                    //
                    // productCell.BaselineLabel.text = "\(product[index].baselineQty!)"

                  }
                }
              }

            }

            completion(true, response.data!.products!)

          } else {
            loggerDebug.error("\(response.error!)")
            completion(false, nil)
          }

        } catch let jsonError {

          loggerDebug.error("\(jsonError.localizedDescription)")
          completion(false, nil)

        }

      }
    }

  }

  @objc func runGetReceiptTimer() {

    timer = Timer.scheduledTimer(
      timeInterval: 1.0,
      target: self,
      selector: #selector(getReceiptItems),
      userInfo: nil,
      repeats: true)

  }

  // Show receipt with animation
  @objc func show() {
    loggerDebug.debug("Menu Button Clicked!")

    runGetReceiptTimer()

    blackBgView.isHidden = false
    contentView.isHidden = false

    UIView.animate(
      withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1,
      options: .curveEaseIn,
      animations: {
        self.blackBgView.alpha = 1

        if hasSafeArea {
          if let window = UIApplication.shared.keyWindow {
            self.contentViewBottomConstraints?.constant = -(10 + 70 + window.safeAreaInsets.bottom)
          }

        } else {
          self.contentViewBottomConstraints?.constant = -105
        }

        self.layoutIfNeeded()
      }, completion: nil)
  }

  // Hide receipt with animation
  @objc func hide() {

    timer?.invalidate()

    UIView.animate(
      withDuration: 0.5,
      animations: {
        self.blackBgView.alpha = 0
        if hasSafeArea {
          self.contentViewBottomConstraints?.constant = 300 + 70
        } else {
          self.contentViewBottomConstraints?.constant = 300 + 95
        }
        self.layoutIfNeeded()
      }
    ) { _ in
      self.blackBgView.isHidden = true
      self.contentView.isHidden = true
    }
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension CustomReceiptView: UITableViewDelegate, UITableViewDataSource {

  func numberOfSections(in _: UITableView) -> Int {
    return 2
  }

  func tableView(_: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 1 {

      loggerDebug.trace("product array count: \(productListArray.count)")
      if productListArray.count > 0 {
        return productListArray.count
      }
      return 5
    }
    return 1
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == 1 {

      if self.productListArray.count > 0 {
        let cell =
          tableView.dequeueReusableCell(withIdentifier: productCellId, for: indexPath)
          as! ProductCell

        if productListArray.count > 0 {
          cell.receiptItem = productListArray[indexPath.row]
        }

        return cell
      }

      let cell =
        tableView.dequeueReusableCell(withIdentifier: shimmerReceiptItemCellId, for: indexPath)
        as! ShimmerReceiptItemCell

      return cell

    } else {
      let cell =
        tableView.dequeueReusableCell(withIdentifier: productHeaderCellId, for: indexPath)
        as! ProductHeaderCell

      return cell
    }
  }

  func tableView(_: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    if indexPath.section == 1 {
      return 50
    }
    return 80
  }
}
