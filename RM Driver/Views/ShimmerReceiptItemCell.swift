//
//  ShimmerOrderHistoryCell.swift
//  Robomart
//
//  Created by Fahad Naqvi on 7/4/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import UIKit

class ShimmerReceiptItemCell: UITableViewCell {
  let userImage = Components.shared.customView()
  let orderTitleLabel = Components.shared.customView()
  let dateLabel = Components.shared.customView()
  let priceLabel = Components.shared.customView()
  let statusLabel = Components.shared.customView()

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    backgroundColor = .white
    separatorInset = .zero
    layoutMargins = .zero

    setupUI()
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  func setupUI() {
    addSubview(userImage)
    userImage.layer.cornerRadius = 20
    userImage.backgroundColor = .init(white: 0.9, alpha: 1)
    userImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    userImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 35).isActive = true
    userImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
    userImage.heightAnchor.constraint(equalToConstant: 40).isActive = true

    addSubview(orderTitleLabel)
    orderTitleLabel.layer.cornerRadius = 5
    orderTitleLabel.backgroundColor = .init(white: 0.9, alpha: 1)
    orderTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    orderTitleLabel.leadingAnchor.constraint(equalTo: userImage.trailingAnchor, constant: 10)
      .isActive = true
    orderTitleLabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
    orderTitleLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true

    //        addSubview(dateLabel)
    //        dateLabel.layer.cornerRadius = 5
    //        dateLabel.backgroundColor = .init(white: 0.9, alpha: 1)
    //        dateLabel.topAnchor.constraint(equalTo: orderTitleLabel.bottomAnchor, constant: 5).isActive = true
    //        dateLabel.leadingAnchor.constraint(equalTo: orderTitleLabel.leadingAnchor, constant: 0).isActive = true
    //        dateLabel.widthAnchor.constraint(equalToConstant: 120).isActive = true
    //        dateLabel.heightAnchor.constraint(equalToConstant: 14).isActive = true

    addSubview(priceLabel)
    priceLabel.layer.cornerRadius = 5
    priceLabel.backgroundColor = .init(white: 0.9, alpha: 1)
    priceLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
    priceLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
    priceLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true

    //        addSubview(statusLabel)
    //        statusLabel.layer.cornerRadius = 5
    //        statusLabel.backgroundColor = .init(white: 0.9, alpha: 1)
    //        statusLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -50).isActive = true
    //        statusLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 5).isActive = true
    //        statusLabel.widthAnchor.constraint(equalToConstant: 50).isActive = true
    //        statusLabel.heightAnchor.constraint(equalToConstant: 14).isActive = true

    userImage.alpha = 1
    orderTitleLabel.alpha = 1
    //        dateLabel.alpha = 1
    priceLabel.alpha = 1
    //        statusLabel.alpha = 1

    fadeIn(finished: true)
  }

  func fadeIn(finished _: Bool) {
    UIView.animate(
      withDuration: 1.0, delay: 0, options: [.curveEaseInOut],
      animations: {
        self.userImage.alpha = 0.5
        self.orderTitleLabel.alpha = 0.5
        //            self.dateLabel.alpha = 0.5
        self.priceLabel.alpha = 0.5
        //            self.statusLabel.alpha = 0.5

      }, completion: fadeOut)
  }

  func fadeOut(finished _: Bool) {
    UIView.animate(
      withDuration: 1.0, delay: 0, options: [.curveEaseInOut],
      animations: {
        self.userImage.alpha = 1
        self.orderTitleLabel.alpha = 1
        //            self.dateLabel.alpha = 1
        self.priceLabel.alpha = 1
        //            self.statusLabel.alpha = 1

      }, completion: fadeIn)
  }
}
