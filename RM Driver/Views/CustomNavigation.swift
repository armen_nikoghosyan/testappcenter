//
//  CustomNavigation.swift
//  Robomart
//
//  Created by Fahad Naqvi on 3/4/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import UIKit

protocol NavigationDelegate {
  func isButtonPressed(_ isBack: Bool?)
}

class CustomNavigation: UIView {
  let customNavigationbar = Components.shared.customView()
  let navigationbarBorder = Components.shared.customView()
  let backButton = Components.shared.customButton(
    title: "", fontName: .avenirNextRegular, fontSize: 12, imageName: "back-thin-icon")
  let navigationTitleLabel = Components.shared.customLabel(
    fontSize: 18, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0,
    text: "Receipt")
  let rightBarButton = Components.shared.customButton(
    title: "", fontName: .helveticaBold, fontSize: 16, imageName: "help-icon")

  var delegate: NavigationDelegate!

  var backButtonYConstraints: NSLayoutConstraint?
  var titleYConstraints: NSLayoutConstraint?

  override init(frame: CGRect) {
    super.init(frame: frame)
    translatesAutoresizingMaskIntoConstraints = false
    customNavigationBarSetup()
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension CustomNavigation {
  func customNavigationBarSetup() {
    addSubview(customNavigationbar)
    //        customNavigationbar.backgroundColor = .init(white: 0.95, alpha: 1)
    customNavigationbar.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
    customNavigationbar.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive =
      true
    customNavigationbar.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive =
      true
    customNavigationbar.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true

    customNavigationbar.addSubview(backButton)
    backButton.tag = 101
    backButton.contentHorizontalAlignment = .left
    backButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    backButtonYConstraints = backButton.centerYAnchor.constraint(
      equalTo: customNavigationbar.centerYAnchor, constant: 0)
    backButtonYConstraints?.isActive = true
    backButton.leadingAnchor.constraint(equalTo: customNavigationbar.leadingAnchor, constant: 15)
      .isActive = true
    backButton.widthAnchor.constraint(equalToConstant: 35).isActive = true
    backButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
    backButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)

    customNavigationbar.addSubview(navigationTitleLabel)
    navigationTitleLabel.font = UIFont.systemFont(ofSize: 18, weight: .medium)
    titleYConstraints = navigationTitleLabel.centerYAnchor.constraint(
      equalTo: customNavigationbar.centerYAnchor, constant: 0)
    titleYConstraints?.isActive = true
    //        navigationTitleLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
    navigationTitleLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true

    customNavigationbar.addSubview(rightBarButton)
    rightBarButton.tag = 102
    rightBarButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    rightBarButton.setTitleColor(.white, for: .normal)
    rightBarButton.contentHorizontalAlignment = .right
    rightBarButton.centerYAnchor.constraint(equalTo: customNavigationbar.centerYAnchor, constant: 0)
      .isActive = true
    rightBarButton.trailingAnchor.constraint(
      equalTo: customNavigationbar.trailingAnchor, constant: -15
    ).isActive = true
    rightBarButton.widthAnchor.constraint(equalToConstant: 35).isActive = true
    rightBarButton.heightAnchor.constraint(equalToConstant: 35).isActive = true
    rightBarButton.addTarget(self, action: #selector(backAction(_:)), for: .touchUpInside)

    customNavigationbar.addSubview(navigationbarBorder)
    navigationbarBorder.backgroundColor = .init(white: 0.85, alpha: 1)
    navigationbarBorder.bottomAnchor.constraint(
      equalTo: customNavigationbar.bottomAnchor, constant: 0
    ).isActive = true
    navigationbarBorder.leadingAnchor.constraint(
      equalTo: customNavigationbar.leadingAnchor, constant: 0
    ).isActive = true
    navigationbarBorder.trailingAnchor.constraint(
      equalTo: customNavigationbar.trailingAnchor, constant: 0
    ).isActive = true
    navigationbarBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true

    navigationTitleLabel.leadingAnchor.constraint(equalTo: backButton.trailingAnchor, constant: 5)
      .isActive = true
    navigationTitleLabel.trailingAnchor.constraint(
      equalTo: rightBarButton.leadingAnchor, constant: -5
    ).isActive = true
  }

  @objc private func backAction(_ sender: UIButton) {
    if sender.tag == 101 {
      delegate.isButtonPressed(true)
    } else {
      delegate.isButtonPressed(false)
    }
  }
}
