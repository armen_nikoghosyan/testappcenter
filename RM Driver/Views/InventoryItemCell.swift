//
//  InventoryItemCell.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 12/2/20.
//

import UIKit

class InventoryItemCell: UITableViewCell {

  let productNameLabel = Components.shared.customLabel(
    fontSize: 16, fontName: .avenirNextDemiBold, fontAlignment: .left, numberOfLines: 0,
    text: "Face Masks")
  let priceLabel = Components.shared.customLabel(
    fontSize: 16, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0,
    text: "$10.99")

  let quantityHeaderLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextMedium, fontAlignment: .left, numberOfLines: 0,
    text: "Baseline")
  let quantityLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "5")

  let currQuantityHeaderLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextMedium, fontAlignment: .left, numberOfLines: 0, text: "Last")
  let currQuantityLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "5")

  let unionQuantityHeaderLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextMedium, fontAlignment: .left, numberOfLines: 0, text: "Union"
  )
  let unionQuantityLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "5")

  let bottomBorder = Components.shared.customView()

  var inventoryProduct: InventoryProduct? {
    didSet {

      productNameLabel.text = inventoryProduct?.name!
      priceLabel.text = String(format: "$%.2f", inventoryProduct!.price!)
      quantityLabel.text = "\(inventoryProduct!.baselineQty!)"
      currQuantityLabel.text = "\(inventoryProduct!.currentQty!)"
      unionQuantityLabel.text = "\(inventoryProduct!.unionQty!)"

    }
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundColor = .white
    selectionStyle = .none

    addSubview(productNameLabel)
    // ProductNameLabel.backgroundColor = .red
    productNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
    productNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
    productNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -120).isActive =
      true

    addSubview(priceLabel)
    priceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
    priceLabel.topAnchor.constraint(equalTo: productNameLabel.topAnchor, constant: 0).isActive =
      true

    addSubview(quantityHeaderLabel)
    quantityHeaderLabel.backgroundColor = .init(white: 0.95, alpha: 1)
    quantityHeaderLabel.textColor = .init(white: 0.4, alpha: 1)
    quantityHeaderLabel.leadingAnchor.constraint(
      equalTo: productNameLabel.leadingAnchor, constant: 0
    ).isActive = true
    quantityHeaderLabel.topAnchor.constraint(equalTo: productNameLabel.bottomAnchor, constant: 5)
      .isActive = true
    quantityHeaderLabel.widthAnchor.constraint(equalToConstant: frame.width / 3).isActive = true

    addSubview(currQuantityHeaderLabel)
    currQuantityHeaderLabel.isHidden = false
    currQuantityHeaderLabel.backgroundColor = .init(white: 0.95, alpha: 1)
    currQuantityHeaderLabel.textColor = .init(white: 0.4, alpha: 1)
    currQuantityHeaderLabel.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive =
      true
    currQuantityHeaderLabel.topAnchor.constraint(
      equalTo: productNameLabel.bottomAnchor, constant: 5
    ).isActive = true
    currQuantityHeaderLabel.widthAnchor.constraint(equalToConstant: frame.width / 3).isActive = true

    addSubview(unionQuantityHeaderLabel)
    unionQuantityHeaderLabel.isHidden = false
    unionQuantityHeaderLabel.backgroundColor = .init(white: 0.95, alpha: 1)
    unionQuantityHeaderLabel.textColor = .init(white: 0.4, alpha: 1)
    unionQuantityHeaderLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20)
      .isActive = true
    unionQuantityHeaderLabel.topAnchor.constraint(
      equalTo: productNameLabel.bottomAnchor, constant: 5
    ).isActive = true
    unionQuantityHeaderLabel.widthAnchor.constraint(equalToConstant: frame.width / 3).isActive =
      true

    addSubview(quantityLabel)
    quantityLabel.textColor = .init(white: 0.4, alpha: 1)
    quantityLabel.leadingAnchor.constraint(equalTo: quantityHeaderLabel.leadingAnchor, constant: 5)
      .isActive = true
    quantityLabel.topAnchor.constraint(equalTo: quantityHeaderLabel.bottomAnchor, constant: 5)
      .isActive = true
    quantityLabel.widthAnchor.constraint(equalToConstant: (frame.width / 3) - 10).isActive = true

    addSubview(currQuantityLabel)
    currQuantityLabel.textColor = .init(white: 0.4, alpha: 1)
    currQuantityLabel.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0).isActive = true
    currQuantityLabel.topAnchor.constraint(equalTo: quantityHeaderLabel.bottomAnchor, constant: 5)
      .isActive = true
    currQuantityLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    currQuantityLabel.widthAnchor.constraint(equalToConstant: (frame.width / 3) - 10).isActive =
      true

    addSubview(unionQuantityLabel)
    unionQuantityLabel.textColor = .init(white: 0.4, alpha: 1)
    unionQuantityLabel.trailingAnchor.constraint(
      equalTo: unionQuantityHeaderLabel.trailingAnchor, constant: -5
    ).isActive = true
    unionQuantityLabel.topAnchor.constraint(equalTo: quantityHeaderLabel.bottomAnchor, constant: 5)
      .isActive = true
    unionQuantityLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10).isActive = true
    unionQuantityLabel.widthAnchor.constraint(equalToConstant: (frame.width / 3) - 10).isActive =
      true

    addSubview(bottomBorder)
    bottomBorder.backgroundColor = .init(white: 0.90, alpha: 1)
    bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    bottomBorder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
    bottomBorder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
    bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true

  }

  //    override func setSelected(_ selected: Bool, animated: Bool) {
  //        super.setSelected(selected, animated: animated)
  //
  ////        SelectImage.isHidden = selected ? false : true
  //    }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
