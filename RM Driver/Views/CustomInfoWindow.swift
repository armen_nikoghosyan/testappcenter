//
//  CustomInfoWindow.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 12/16/20.
//

import UIKit

class CustomInfoWindow: UIView {

  let bgView = Components.shared.customView()
  let titleLabel = Components.shared.customLabel(
    fontSize: 16, fontName: .avenirNextBold, fontAlignment: .left, numberOfLines: 0, text: "Address"
  )
  let addressLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "1049 N Fairfax Ave, West Hollywood, CA 90046, USA")

  let triangleView = Components.shared.customView()

  static let shared = CustomInfoWindow()

  override init(frame: CGRect) {
    super.init(frame: frame)

    // initiate UI
    setupUI()

  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

}

extension CustomInfoWindow {

  // add properties to veriables
  private func setupUI() {

    backgroundColor = .clear

    addSubview(bgView)

    bgView.layer.cornerRadius = 15
    bgView.backgroundColor = .white
    bgView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).cgColor
    bgView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    bgView.layer.shadowOpacity = 1.0
    bgView.layer.shadowRadius = 2.0
    bgView.layer.masksToBounds = false

    bgView.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
    bgView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0).isActive = true
    bgView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
    bgView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true

    //        bgView.addSubview(titleLabel)
    //        titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15).isActive = true
    //        titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
    //        titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true

    bgView.addSubview(addressLabel)
    addressLabel.topAnchor.constraint(equalTo: topAnchor, constant: 15).isActive = true
    addressLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 15).isActive = true
    addressLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15).isActive = true
    addressLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true

    addSubview(triangleView)
    triangleView.backgroundColor = .clear

    triangleView.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.40).cgColor
    triangleView.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
    triangleView.layer.shadowOpacity = 1.0
    triangleView.layer.shadowRadius = 1.0
    triangleView.layer.masksToBounds = false

    triangleView.topAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    triangleView.centerXAnchor.constraint(equalTo: bgView.centerXAnchor, constant: 0).isActive =
      true
    triangleView.widthAnchor.constraint(equalToConstant: 25).isActive = true
    triangleView.heightAnchor.constraint(equalToConstant: 20).isActive = true

    let width = 25
    let height = 20

    //        logger.info("checking triangle: width: \(width) and height: \(height)")

    let path = CGMutablePath()

    path.move(to: CGPoint(x: 0, y: 0))
    path.addLine(to: CGPoint(x: width / 2, y: height / 2))
    path.addLine(to: CGPoint(x: width, y: 0))
    path.addLine(to: CGPoint(x: 0, y: 0))

    let shape = CAShapeLayer()
    shape.path = path
    shape.fillColor = UIColor.white.cgColor

    triangleView.layer.insertSublayer(shape, at: 0)

  }

}
