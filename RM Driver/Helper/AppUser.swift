//
//  User.swift
//  Robomart
//
//  Created by Macbook Pro on 11/04/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import FirebaseAuth
import SwiftyJSON
import UIKit

class AppUser: NSObject {
  static var sharedInstance = AppUser()

  //    var userData: UserData?
  //    var cardData: CardDataModel?
  //    var categoriesData: CategoryListData?
  //    var selectedCategoryData: Record?

  fileprivate let userDefaults: UserDefaults!

  override fileprivate init() {
    userDefaults = UserDefaults.standard
  }

  // Save card list to local
  func saveData<T: Encodable>(_ data: T?, forKey: userDefaultKey?) {
    if let thisData = data as? Bool {
      loggerDebug.debug("UserDefault: save Bool value")
      UserDefaults.standard.set(thisData, forKey: forKey!.rawValue)
      UserDefaults.standard.synchronize()
      return
    } else if let thisData = data as? String {
      loggerDebug.debug("UserDefault: save String value")
      UserDefaults.standard.set(thisData, forKey: forKey!.rawValue)
      UserDefaults.standard.synchronize()
      return
    }

    do {
      let encodedData = try JSONEncoder().encode(data!)
      loggerDebug.debug("UserDefault: save Model value")
      UserDefaults.standard.set(encodedData, forKey: forKey!.rawValue)
      UserDefaults.standard.synchronize()

    } catch let jsonError {
      loggerDebug.error("Error : \(jsonError.localizedDescription)")
    }

    // if let card = data as? CardDataModel {
    //
    //     do{
    //
    //         let encodedData = try? JSONEncoder().encode(card)
    //         UserDefaults.standard.set(encodedData, forKey: "cardList")
    //
    //     }catch let jsonError{
    //         print("Error : \(jsonError)")
    //     }
    //
    // } else if let categorydata = data as? CategoryListData {
    //
    //
    //     do{
    //
    //         let encodedData = try? JSONEncoder().encode(categorydata)
    //         UserDefaults.standard.set(encodedData, forKey: "categoriesList")
    //
    //     }catch let jsonError{
    //        print("Error : \(jsonError)")
    //    }
    //
    // } else if let selectedCategory = data as? Record {
    //
    //     do{
    //
    //         let encodedData = try? JSONEncoder().encode(selectedCategory)
    //         UserDefaults.standard.set(encodedData, forKey: "selectedCategory")
    //
    //     }catch let jsonError{
    //        print("Error : \(jsonError)")
    //    }
    //
    // }
  }

  func getData<T: Codable>(forKey: userDefaultKey?) -> T? {
    loggerDebug.trace("\(forKey!.rawValue)")

    if let decodedUser = userDefaults.data(forKey: forKey!.rawValue) {
      do {
        let loadedData = try JSONDecoder().decode(T.self, from: decodedUser)
        return loadedData

      } catch let jsonError {
        loggerDebug.error("Error: \(jsonError.localizedDescription)")
        return nil
      }

      // return self.selectedCategoryData

    } else if let stringValue = userDefaults.value(forKey: forKey!.rawValue) {
      loggerDebug.trace("\(stringValue)")
      return stringValue as? T
    } else {
      return nil
    }
  }

  func removeData(forKey: userDefaultKey?) {
    userDefaults.removeObject(forKey: forKey!.rawValue)
    userDefaults.synchronize()
  }

  // MARK: User Manage Mehtods

  // func saveCurrentUser(_ user: UserData) {
  //     do {
  //         let encodedData = try? JSONEncoder().encode(user)
  //         UserDefaults.standard.set(encodedData, forKey: "currentUser")
  //
  //     } catch let jsonError {
  //         print("Error : \(jsonError)")
  //     }
  // }

  // func removeCurrentUser() {
  //     userDefaults.removeObject(forKey: "currentUser")
  //     userDefaults.synchronize()
  //     userData = nil
  // }
  //
  // func getCurrentUser() -> UserData? {
  //     if let decodedUser = userDefaults.data(forKey: "currentUser") {
  //         do {
  //             let json = try JSON(data: decodedUser)
  //             // print("Current User : \(json)")
  //
  //             let decoder = JSONDecoder()
  //
  //             if let loadedPerson = try? decoder.decode(UserData.self, from: decodedUser) {
  //                 userData = loadedPerson
  //             }
  //
  //         } catch let jsonError {
  //             print("Error: \(jsonError)")
  //         }
  //
  //         return userData
  //
  //     } else {
  //         return nil
  //     }
  // }

  // Get token for robomart server
  // func getToken() -> String?{
  //
  //     if let token = UserDefaults.standard.string(forKey: )
  //
  // }

  // Get firebase IDToken
  func getFirebaseAuthIDToken(completion: @escaping (Bool?, String?) -> Void) {
    if let user = Auth.auth().currentUser {
      // Get idtoken for user
      var fbTokenString: String = "0"

      if let fbtoken = KeyChain.load(key: .fbtoken),
        let currDate: Date = AppUser.sharedInstance.getData(forKey: .tokenDate)
      {
        let minDiff = getDateDiff(start: currDate, end: Date())

        loggerDebug.trace("Checking time: diffence: \(minDiff)")

        if minDiff >= 60 {

          // logger.info("checking time: fbToken: \(KeyChain.dataToString(data: fbtoken)), line: \(#line)")

          user.getIDTokenForcingRefresh(true) { (token, error) in

            if let err = error {
              loggerDebug.error("Error: \(err.localizedDescription)")
              completion(false, nil)
              return
            }

            loggerDebug.trace("Checking time: create fb token again")

            // Save firebase token to keychain
            let token = KeyChain.stringToData(string: token!)
            _ = KeyChain.save(key: .fbtoken, data: token)

            // Save the date to check 1 hour expiry
            AppUser.sharedInstance.saveData(Date(), forKey: .tokenDate)

            let updatedFBToken = KeyChain.load(key: .fbtoken)
            fbTokenString = KeyChain.dataToString(data: updatedFBToken!)
            // logger.info("checking time: updated fbToken: \(fbTokenString)")

            completion(true, fbTokenString)

          }

        } else {

          // logger.info("checking time: fbToken: \(KeyChain.dataToString(data: fbtoken)), line: \(#line)")

          fbTokenString = KeyChain.dataToString(data: fbtoken)
          completion(true, fbTokenString)
        }

      } else {

        loggerDebug.trace("Checking time: fbtoken is not in keychain")

        user.getIDToken(completion: { token, error in

          if let err = error {
            loggerDebug.error("Error: \(err.localizedDescription)")
            completion(false, nil)
            return
          }

          // Save firebase token to keychain
          let token = KeyChain.stringToData(string: token!)
          _ = KeyChain.save(key: .fbtoken, data: token)

          // Save the date to check 1 hour expiry
          AppUser.sharedInstance.saveData(Date(), forKey: .tokenDate)

          let fbtoken = KeyChain.load(key: .fbtoken)
          fbTokenString = KeyChain.dataToString(data: fbtoken!)

          completion(true, fbTokenString)

        })
      }

      // return fbTokenString

    } else {
      completion(false, nil)
    }
  }

  // func getFirebaseAuthIDToken() -> String? {
  //
  // }

  func getDateDiff(start: Date, end: Date) -> Int {
    let calendar = Calendar.current
    let dateComponents = calendar.dateComponents([Calendar.Component.minute], from: start, to: end)

    let min = dateComponents.minute
    return Int(min!)
  }
}

enum userDefaultKey: String {
  case loggedIn
  case userTest = "user"
  case currentUser
  case userID
  case selectedCategory
  case cardList
  case categoriesList
  case isCardSaved
  case orderID = "order_id"
  case authVerificationID
  case token
  case fbtoken
  case fcmtoken
  case isActiveOrder
  case selectedRobomart
  case lastLocation
  case vehicleID = "vehicle_id"
  case inviteCode = "invite_code"
  case tokenDate = "currentDate"
  case isOrderCompleted
  case currentOrder
  case isRestockActive
  case vehicleName
}
