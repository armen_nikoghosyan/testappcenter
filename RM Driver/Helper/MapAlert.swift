//
//  MapAlert.swift
//  RM Driver
//
//  Created by sose yeritsyan on 9/13/21.
//

import UIKit

class ChooseMap: UIViewController {

  var lat = 0.0
  var lon = 0.0
  let zoom: Float = 16

  override func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.3)
    setup()
  }

  func setup() {
    let alertView = UIView()
    alertView.backgroundColor = .white
    alertView.layer.masksToBounds = true
    alertView.layer.cornerRadius = 12

    self.view.addSubview(alertView)

    alertView.frame = CGRect(
      x: 4, y: self.view.frame.size.height - 250, width: self.view.frame.size.width - 8, height: 240
    )

    let titleLabel = UILabel(
      frame: CGRect(x: 0, y: 0, width: alertView.frame.size.width, height: 80))
    titleLabel.numberOfLines = 0
    titleLabel.text = "Choose an app to open direction"
    titleLabel.textColor = .gray
    titleLabel.font = UIFont(name: "AvenirNext-Medium", size: 20)
    titleLabel.textAlignment = .center
    alertView.addSubview(titleLabel)

    let googleMapsButton =
      UIButton(
        frame: CGRect(
          x: 0,
          y: alertView.frame.size.height - 150,
          width: alertView.frame.size.width,
          height: 50))
    googleMapsButton.setTitleColor(.blue, for: .normal)
    googleMapsButton.titleLabel?.font.withSize(23)

    googleMapsButton.setTitle("Google Maps", for: .normal)
    googleMapsButton.addTarget(self, action: #selector(googleMaps), for: .touchUpInside)

    alertView.addSubview(googleMapsButton)

    let appleMapsButton =
      UIButton(
        frame: CGRect(
          x: 0,
          y: alertView.frame.size.height - 100,
          width: alertView.frame.size.width,
          height: 50))
    appleMapsButton.setTitleColor(.blue, for: .normal)
    appleMapsButton.titleLabel?.font.withSize(23)

    appleMapsButton.setTitle("Apple Maps", for: .normal)
    appleMapsButton.addTarget(self, action: #selector(appleMaps), for: .touchUpInside)

    alertView.addSubview(appleMapsButton)

    let canccelButton =
      UIButton(
        frame: CGRect(
          x: 0,
          y: alertView.frame.size.height - 50,
          width: alertView.frame.size.width,
          height: 50))
    canccelButton.setTitleColor(.blue, for: .normal)
    canccelButton.titleLabel?.font.withSize(23)

    canccelButton.setTitle("Cancel", for: .normal)
    canccelButton.addTarget(self, action: #selector(cancel), for: .touchUpInside)

    alertView.addSubview(canccelButton)

  }
  @objc func googleMaps() {
    if UIApplication.shared.canOpenURL(URL(string: "comgooglemaps://")!) {
      loggerDebug.debug("Open google maps")
      UIApplication.shared.open(
        URL(
          string:
            "comgooglemaps://?center=\(String(describing: lat)),\(String(describing: lon))&zoom=\(self.zoom)&views=traffic&q=\(String(describing: lat)),\(String(describing: lon))"
        )!, options: [:], completionHandler: nil)

    } else {  // Open map link
      loggerDebug.debug("Can't use google maps")
      UIApplication.shared.open(
        URL(
          string:
            "http://maps.google.com/maps?q=loc:\(String(describing: lat)),\(String(describing: lon))&zoom=\(self.zoom)&views=traffic&q=\(String(describing: lat)),\(String(describing: lon))"
        )!, options: [:], completionHandler: nil)
    }
  }
  @objc func appleMaps() {
    let mapURL =
      "maps://?q=Customer+location&ll=\(String(describing: lat)),\(String(describing: lon))"
    if UIApplication.shared.canOpenURL(URL(string: mapURL)!) {
      loggerDebug.debug("Open apple maps")
      UIApplication.shared.open(URL(string: mapURL)!)
    } else {
      loggerDebug.debug("Can't open apple maps")
    }

  }

  @objc func cancel() {
    dismiss(animated: true, completion: nil)
  }
}
