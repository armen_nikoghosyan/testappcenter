import UIKit

@objc protocol AURUnlockSliderDelegate {
  func unlockSliderDidUnlock(_ slider: AURUnlockSlider)
}

open class AURUnlockSlider: UIView {
  var delegate: AURUnlockSliderDelegate?

  public final var sliderText = "Slide to Unlock"
  public final var sliderTextColor: UIColor? = .white
  public final var sliderTextFont = UIFont(name: "HelveticaNeue-Thin", size: 15.0)!
  public final var sliderCornerRadius: CGFloat = 3.0
  public final var sliderColor = UIColor.clear
  public final var sliderImageTint = UIColor.white
  public final var sliderBackgroundColor = UIColor.clear

  fileprivate final let sliderContainer = UIView(frame: CGRect.zero)
  fileprivate final let sliderView = UIView(frame: CGRect.zero)
  fileprivate final let sliderViewImage = UIImageView(frame: CGRect.zero)
  fileprivate final let sliderViewLabel = UILabel(frame: CGRect.zero)
  fileprivate final var isCurrentDraggingSlider = false
  fileprivate final var lastDelegateFireOffset = CGFloat(0)
  fileprivate final var touchesBeganPoint = CGPoint.zero
  fileprivate final var valueChangingTimer: Timer?
  fileprivate final let sliderPanGestureRecogniser = UIPanGestureRecognizer()
  fileprivate final let dynamicButtonAnimator = UIDynamicAnimator()
  fileprivate final var snappingBehavior: SliderSnappingBehavior?

  override public init(frame: CGRect) {
    super.init(frame: frame)

    setupView()
    setNeedsLayout()
  }

  public required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupView()
    setNeedsLayout()
  }

  fileprivate func setupView() {
    sliderContainer.backgroundColor = backgroundColor

    sliderContainer.addSubview(sliderView)

    sliderViewLabel.isUserInteractionEnabled = false
    sliderViewLabel.textAlignment = NSTextAlignment.center
    sliderViewLabel.textColor = sliderTextColor!
    sliderViewImage.tintColor = sliderImageTint
    sliderView.addSubview(sliderViewLabel)

    sliderView.addSubview(sliderViewImage)

    //        sliderPanGestureRecogniser.addTarget(self, action: NSSelectorFromString("handleGesture:"))
    sliderPanGestureRecogniser.addTarget(self, action: #selector(handleGesture(_:)))
    sliderView.addGestureRecognizer(sliderPanGestureRecogniser)

    sliderContainer.center = CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)
    addSubview(sliderContainer)
    clipsToBounds = true
  }

  override open func layoutSubviews() {
    super.layoutSubviews()

    sliderContainer.frame = frame
    sliderContainer.center = CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)
    sliderContainer.backgroundColor = sliderBackgroundColor

    sliderView.frame = CGRect(x: 0.0, y: 0.0, width: bounds.size.width, height: bounds.size.height)
    sliderView.center = CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)
    sliderView.backgroundColor = sliderColor

    sliderViewLabel.frame = CGRect(
      x: 0.0, y: 0.0, width: sliderView.bounds.size.width, height: sliderView.bounds.size.height)
    sliderViewLabel.center = CGPoint(
      x: sliderViewLabel.bounds.size.width * 0.5, y: sliderViewLabel.bounds.size.height * 0.5)
    sliderViewLabel.backgroundColor = sliderColor
    sliderViewLabel.font = sliderTextFont
    sliderViewLabel.text = sliderText
    sliderViewLabel.textColor = sliderTextColor!

    sliderViewImage.frame = CGRect(x: 0.0, y: 0.0, width: 30, height: 30)
    sliderViewImage.center = CGPoint(x: 30, y: sliderViewLabel.bounds.size.height * 0.5)
    sliderViewImage.image = UIImage(named: "right-arrow")
    sliderViewImage.clipsToBounds = true
    sliderViewImage.contentMode = .scaleAspectFit
    sliderViewImage.tintColor = sliderImageTint

    layer.cornerRadius = sliderCornerRadius
  }

  @objc final func handleGesture(_ sender: UIGestureRecognizer) {
    if sender as NSObject == sliderPanGestureRecogniser {
      switch sender.state {
      case .began:
        isCurrentDraggingSlider = true
        touchesBeganPoint = sliderPanGestureRecogniser.translation(in: sliderView)
        if dynamicButtonAnimator.behaviors.count != 0 {
          dynamicButtonAnimator.removeBehavior(snappingBehavior!)
        }

        lastDelegateFireOffset = ((touchesBeganPoint.x + touchesBeganPoint.x) * 0.40)

      case .changed:
        valueChangingTimer?.invalidate()
        let translationInView = sliderPanGestureRecogniser.translation(in: sliderView)
        let translatedCenterX: CGFloat =
          (bounds.size.width * 0.5) + (touchesBeganPoint.x + translationInView.x)
        sliderView.center = CGPoint(x: translatedCenterX, y: sliderView.center.y)
        lastDelegateFireOffset = translatedCenterX

      case .ended:
        loggerDebug.debug("Slider ended!!")
        //                var point: CGPoint?
        //                point = CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)
        //                snappingBehavior = SliderSnappingBehavior(item: sliderView, snapToPoint: point!)
        //                lastDelegateFireOffset = sliderView.center.x
        //                dynamicButtonAnimator.addBehavior(snappingBehavior!)
        //                isCurrentDraggingSlider = true
        //                lastDelegateFireOffset = center.x
        //                valueChangingTimer?.invalidate()
        fallthrough

      case .failed, .cancelled:
        var point: CGPoint?
        if sliderView.frame.origin.x > sliderContainer.center.x {
          delegate?.unlockSliderDidUnlock(self)
          point = CGPoint(x: bounds.size.width * 1.5, y: bounds.size.height * 0.5)
        } else {
          point = CGPoint(x: bounds.size.width * 0.5, y: bounds.size.height * 0.5)
        }

        snappingBehavior = SliderSnappingBehavior(item: sliderView, snapToPoint: point!)
        lastDelegateFireOffset = sliderView.center.x
        dynamicButtonAnimator.addBehavior(snappingBehavior!)
        isCurrentDraggingSlider = false
        lastDelegateFireOffset = center.x
        valueChangingTimer?.invalidate()

      case .possible:

        loggerDebug.debug("possible")
      @unknown default:
        loggerDebug.debug("unknown")
      }
    }
  }
}

final class SliderSnappingBehavior: UIDynamicBehavior {
  var snappingPoint: CGPoint
  init(item: UIDynamicItem, snapToPoint point: CGPoint) {
    let dynamicItemBehavior = UIDynamicItemBehavior(items: [item])
    dynamicItemBehavior.allowsRotation = false

    let snapBehavior = UISnapBehavior(item: item, snapTo: point)
    snapBehavior.damping = 1

    snappingPoint = point

    super.init()

    addChildBehavior(snapBehavior)
    addChildBehavior(dynamicItemBehavior)
  }
}
