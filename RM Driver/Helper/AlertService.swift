//
//  AlertService.swift
//  Robomart
//
//  Created by Macbook Pro on 08/04/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import UIKit

struct AlertService {
  static let shared = AlertService()

  private init() {}

  func alert(in vc: UIViewController, message: String) {
    let alert = UIAlertController(title: "Robomart", message: message, preferredStyle: .alert)

    let ok = UIAlertAction(title: "OK", style: .cancel) { _ in
      alert.dismiss(animated: true)
    }
    alert.addAction(ok)
    vc.present(alert, animated: true, completion: nil)
  }

  // alert view with text field
  public func alertWithTextField(
    vc: UIViewController, title: String? = nil, message: String? = nil, placeholder: String? = nil,
    completion: @escaping ((String) -> Void) = { _ in }
  ) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

    alert.view.tintColor = .gray

    alert.addTextField { newTextField in
      newTextField.placeholder = placeholder
      newTextField.textAlignment = .center
      newTextField.autocapitalizationType = .allCharacters
    }
    alert.addAction(UIAlertAction(title: "No", style: .cancel) { _ in completion("NO") })
    alert.addAction(
      UIAlertAction(title: "Yes", style: .default) { action in
        if let textFields = alert.textFields,
          let tf = textFields.first,
          let result = tf.text
        {
          completion(result)
        } else {
          completion("")
        }
      })
    vc.present(alert, animated: true)
  }
}
