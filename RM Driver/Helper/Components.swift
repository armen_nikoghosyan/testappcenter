//
//  Components.swift
//  Robomart
//
//  Created by Macbook Pro on 12/04/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import AVFoundation
import Firebase
import FirebaseAuth
import Foundation
import Intercom
import UIKit

struct Components {
  static let shared = Components()

  func customCollectionView(scrollDirection: UICollectionView.ScrollDirection) -> UICollectionView {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = scrollDirection
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.translatesAutoresizingMaskIntoConstraints = false
    cv.backgroundColor = .clear
    //        cv.dataSource = self
    //        cv.delegate = self
    return cv
  }

  func customCollectionView() -> UICollectionView {
    let layout = UICollectionViewFlowLayout()
    let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
    cv.translatesAutoresizingMaskIntoConstraints = false
    cv.backgroundColor = .clear
    //        cv.dataSource = self
    //        cv.delegate = self
    return cv
  }

  func optionsProgressBar() -> UIProgressView {
    let pv = UIProgressView(progressViewStyle: .bar)
    pv.translatesAutoresizingMaskIntoConstraints = false
    pv.setProgress(0.5, animated: true)
    pv.trackTintColor = .init(white: 0.9, alpha: 1)
    pv.tintColor = Colors.shared.logoBlue.withAlphaComponent(0.5)
    pv.isHidden = true
    pv.layer.cornerRadius = 5
    pv.clipsToBounds = true
    return pv
  }

  func customLabel(
    fontSize: CGFloat, fontName: fontName, fontAlignment: NSTextAlignment, numberOfLines: Int,
    text: String?
  ) -> UILabel {
    let label = UILabel()
    label.backgroundColor = .clear
    label.text = text!
    label.textAlignment = fontAlignment
    label.textColor = .init(white: 0.3, alpha: 1)
    label.font = UIFont(name: fontName.rawValue, size: fontSize)

    label.translatesAutoresizingMaskIntoConstraints = false
    label.numberOfLines = numberOfLines
    return label
  }

  func customImageView(cornerRadius: CGFloat, clipToBounds: Bool, contentMode: UIView.ContentMode)
    -> UIImageView
  {
    let imageView = UIImageView()
    imageView.translatesAutoresizingMaskIntoConstraints = false
    //        imageView.image = UIImage(named: "feed-profile-image")
    imageView.layer.cornerRadius = cornerRadius
    imageView.contentMode = contentMode
    imageView.clipsToBounds = clipToBounds
    return imageView
  }

  func customButton(title: String?, fontName: fontName, fontSize: CGFloat?, imageName: String?)
    -> UIButton
  {
    let button = UIButton()

    if title! != "" {
      button.setTitle(title!, for: .normal)

      button.titleLabel?.font = UIFont(name: fontName.rawValue, size: fontSize!)
      button.setTitleColor(.init(white: 0.5, alpha: 1), for: .normal)
    }

    button.tintColor = UIColor(white: 0.5, alpha: 1)

    if let img = imageName {
      if img != "" {
        button.setImage(UIImage(named: img)?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.contentMode = .scaleAspectFit
      }
    }

    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }

  func customView() -> UIView {
    let view = UIView()
    view.translatesAutoresizingMaskIntoConstraints = false
    //        view.backgroundColor = UIColor(white: 0.8, alpha: 0.5)
    return view
  }

  func customScrollView() -> UIScrollView {
    let scrollView = UIScrollView()
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    scrollView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    return scrollView
  }

  func customUITextView(fontName: fontName, fontSize: CGFloat?, enableScroll: Bool?) -> UITextView {
    let input = UITextView()
    //        input.placeholder = "Write Something here..."
    input.font = UIFont(name: fontName.rawValue, size: fontSize!)
    input.textColor = UIColor(white: 0.8, alpha: 1)
    input.translatesAutoresizingMaskIntoConstraints = false
    //        input.text = "Write Something here..."
    input.isScrollEnabled = enableScroll!
    return input
  }

  func customUITextField(
    placeholder: String?, fontName: fontName, fontSize: CGFloat?, textAlignment: NSTextAlignment,
    borderSyle: UITextField.BorderStyle, secureText: Bool?
  ) -> UITextField {
    let input = UITextField()
    input.translatesAutoresizingMaskIntoConstraints = false
    input.placeholder = placeholder!
    input.backgroundColor = UIColor.clear
    input.font = UIFont(name: fontName.rawValue, size: fontSize!)
    input.textColor = UIColor.darkGray
    input.textAlignment = textAlignment
    input.keyboardType = .default
    input.borderStyle = borderSyle
    input.isSecureTextEntry = secureText!

    return input
  }

  func customUITableView() -> UITableView {
    let table = UITableView()
    table.backgroundColor = .init(white: 0.95, alpha: 1)
    table.translatesAutoresizingMaskIntoConstraints = false
    return table
  }

  func previewImageFromVideo(url: URL) -> UIImage? {
    //        let url = url as URL
    let request = URLRequest(url: url)
    let cache = URLCache.shared

    if let cachedResponse = cache.cachedResponse(for: request),
      let image = UIImage(data: cachedResponse.data)
    {
      return image
    }

    let asset = AVAsset(url: url)
    let imageGenerator = AVAssetImageGenerator(asset: asset)
    imageGenerator.appliesPreferredTrackTransform = true
    imageGenerator.maximumSize = CGSize(width: 250, height: 120)

    var time = asset.duration
    time.value = min(time.value, 2)

    var image: UIImage?

    do {
      let cgImage = try imageGenerator.copyCGImage(at: time, actualTime: nil)
      image = UIImage(cgImage: cgImage)
    } catch {}

    if let image = image,
      let data = image.pngData(),
      let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)
    {
      let cachedResponse = CachedURLResponse(response: response, data: data)

      cache.storeCachedResponse(cachedResponse, for: request)
    }

    return image
  }

  func pageTransition(type: CATransitionType, subType: CATransitionSubtype) -> CATransition {
    let transition = CATransition()
    transition.duration = 0.5
    transition.type = type
    transition.subtype = subType
    transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
    return transition
  }
}

enum fontName: String {
  case helveticaBold = "Helvetica-Bold"
  case helveticaNeue = "HelveticaNeue"
  case avenirNextRegular = "AvenirNext-Regular"
  case avenirNextBold = "AvenirNext-Bold"
  case avenirNextMedium = "AvenirNext-Medium"
  case avenirNextDemiBold = "AvenirNext-DemiBold"
  case comfortaaRegular = "Comfortaa-Regular"
  case comfortaaRegularLight = "Comfortaa-Regular_Light"
  case comfortaaRegularBold = "Comfortaa-Regular_Bold"
}

func cardIcon(category: String) -> String {
  var imageName: String!
  switch category {
  case "visa":
    imageName = "visa"
  case "mastercard":
    imageName = "mastercard"
  case "american express":
    imageName = "amex"
  case "discover":
    imageName = "discover"
  case "diners club":
    imageName = "diners"
  case "jcb":
    imageName = "jcb"
  case "unionpay":
    imageName = "unionpay_en"
  default:
    break
  }

  return imageName
}

enum newOrderStatus: String {
  case newRequest = "1"
  case assigned = "2"
  case onRoute = "3"
  case parked = "4"
  case doorOpen = "5"
  case startShopping = "6"
  case endShopping = "7"
  case doorClose = "8"
  case end = "9"
  case cancel = "0"
}

func orderStatus(type: String) -> String {
  var status: String!

  switch type {
  case "1":
    status = "NewRequest"
  case "2":
    status = "Assigned"
  case "3":
    status = "OnRoute"
  case "4":
    status = "Parked"
  case "5":
    status = "DoorOpen"
  case "6":
    status = "StartShopping"
  case "7":
    status = "EndShopping"
  case "8":
    status = "DoorClose"
  case "9":
    status = "End"
  case "0":
    status = "Cancel"
  default:
    break
  }

  return status
}

// var ExpenseTypeArr = [113, "General", 114, "Project", 115, "Group", 116, "Travel"] as [Any]

func expenseType(type: Int) -> String {
  var expType: String!

  switch type {
  case 113:
    expType = "General"
  case 114:
    expType = "Project"
  case 115:
    expType = "Group"
  case 116:
    expType = "Travel"
  default:
    break
  }

  return expType
}

func status(status: Int) -> String {
  var expStatus: String!
  switch status {
  case 1:
    expStatus = "In-Process"
  case 2:
    expStatus = "Accepted"
  case 3:
    expStatus = "Declined"
  case 4:
    expStatus = "Resend"
  default:
    break
  }

  return expStatus
}

func statusColor(status: Int) -> UIColor {
  var statusCol: UIColor!
  switch status {
  case 1:
    // In-Process
    statusCol = Colors.shared.logoBlue
  case 2:
    // Accepted
    statusCol = Colors.shared.green
  case 3:
    // Declined
    statusCol = Colors.shared.darkRed
  case 4:
    // Resend
    statusCol = .orange
  default:
    break
  }

  return statusCol
}

func requestStateString(state: String) -> String {
  var stateString: String!
  switch state {
  case "1":
    // status on end
    stateString = "Idle"
  case "2":
    // status on BEING ASSIGNED
    stateString = "Incoming Request"
  case "3":
    // status on EN ROUTE
    stateString = "En Route"
  case "4":
    // status on ARRIVED
    stateString = "Arrived"
  case "5":
    // status on DOORS OPENING
    stateString = "Doors Opening"
  case "6":
    // status on SHOPPING
    stateString = "Shopping"
  case "7":
    // status on END SHOPPING
    stateString = "End Shopping"
  case "8":
    // status on DOORS CLOSING
    stateString = "Doors Closing"
  case "9":
    // status on CHECK BASKET
    stateString = "Check Basket"
  default:
    // it would be 0 END
    stateString = "Idle"
  }

  return stateString
}

func requestStateButtonString(state: String) -> String {
  var stateString: String!
  switch state {
  case "1":
    // status on end
    stateString = "START ENGAGEMENT"
  case "2":
    // status on BEING ASSIGNED
    stateString = "START ENGAGEMENT"
  case "3":
    // status on EN ROUTE
    stateString = "CONFIRM ARRIVAL"
  case "4":
    // status on ARRIVED
    stateString = "CONFIRM OPEN DOORS"
  case "5":
    // status on DOORS OPENING
    stateString = "CONFIRM OPEN DOORS"
  case "6":
    // status on SHOPPING
    stateString = "CONFIRM CLOSE DOORS"
  case "7":
    // status on END SHOPPING
    stateString = "CONFIRM CLOSE DOORS"
  case "8":
    // status on DOORS CLOSING
    stateString = "CONFIRM CLOSE DOORS"
  case "9":
    // status on CHECK BASKET
    stateString = "CONFIRM RECEIPT"
  default:
    // it would be 0 END
    stateString = "START ENGAGEMENT"
  }

  return stateString
}

func getWidthHeightByText(width: CGFloat?, text: String?, fontSize: CGFloat?, fontName: fontName?)
  -> CGRect
{
  let approximateWidthOfText = width!
  let size = CGSize(width: approximateWidthOfText, height: 1000)
  let attributes = [NSAttributedString.Key.font: UIFont(name: fontName!.rawValue, size: fontSize!)!]
  let estimateFrame = NSString(string: text!).boundingRect(
    with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)

  return estimateFrame
}

func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
  UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
  image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
  let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
  UIGraphicsEndImageContext()
  return newImage
}

// Signout user
protocol SignOutUserDelegate {
  func didUserSignedOut(isSignedOut: Bool?)
}

var signOutDelegate: SignOutUserDelegate?

func signOutUser(fromLogin: Bool?) {
  // Check if firebase auth current user is already exist then signout
  if let user = Auth.auth().currentUser {

    loggerDebug.notice("signOutUser: Current user: \(String(describing: user.phoneNumber))")

    do {
      try Auth.auth().signOut()

      AppUser.sharedInstance.saveData(false, forKey: .loggedIn)
      deleteUserInfoFromCrashlytics()

      let defaults = UserDefaults.standard
      let dictionary = defaults.dictionaryRepresentation()
      dictionary.keys.forEach { key in
        defaults.removeObject(forKey: key)
      }

      AppUser.sharedInstance.removeData(forKey: .tokenDate)

      // Update global metadata
      loggerDebug[metadataKey: "userName"] = nil
      // loggerDebug[metadataKey: "userPhoneNumber"] = nil
      loggerDebug[metadataKey: "userEmail"] = nil

      loggerEvents[metadataKey: "userName"] = nil
      // loggerEvents[metadataKey: "userPhoneNumber"] = nil
      loggerEvents[metadataKey: "userEmail"] = nil

      // AppUser.sharedInstance.removeData(forKey: .authVerificationID)
      // signOutDelegate?.didUserSignedOut(isSignedOut: true)
      // if let _ = KeyChain.load(key: .authVerificationID) {
      //     KeyChain.removeItem(key: .authVerificationID)
      // }

      if !fromLogin! {
        KeyChain.logout()

        // Add Logged out event
        // Intercom.logEvent(withName: "Logged out")

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginViewController: LoginVC =
          storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC

        var navigationController = UINavigationController()
        navigationController = UINavigationController(rootViewController: loginViewController)

        // It removes all view controllers from the navigation controller then sets the new root view controller and it pops.
        if let window = UIApplication.shared.keyWindow {
          window.rootViewController = navigationController
        }

        // Navigation bar is hidden
        navigationController.isNavigationBarHidden = true
      }
      // Intercom.logout()

    } catch let signOutError as NSError {
      loggerDebug.error("Error signing out: \(signOutError.localizedDescription)")
    }
  } else {
    loggerDebug.warning("User does not exist.")
  }
}

func getCorrectPath(path: String?) -> String {
  var fullURL = path!

  if fullURL.contains("dashboard.robomart.co") {
    fullURL = fullURL.replacingOccurrences(of: "http:", with: "https:")

  } else {
    if fullURL.contains("https:") {
      fullURL = "\(fullURL)"
    } else {
      fullURL = "\(baseURL)/\(fullURL)"
    }
  }

  return fullURL
}

private func deleteUserInfoFromCrashlytics() {
  let keysAndValues =
    [
      "clientId": "",
      "userName": "",
      "userId": "",
      "userEmail": "",
      "userPhone": "",
    ] as [String: Any]

  Crashlytics.crashlytics().setCustomKeysAndValues(keysAndValues)
}
