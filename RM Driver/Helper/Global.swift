//
//  Global.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 12/13/20.
//

import AVFoundation
import Firebase
import Foundation
import UIKit

// Audio file player when doors open
var player: AVAudioPlayer?

enum audioName: String {
  case welcomeAudio = "WelcomeAudio"
  case tritone = "Tritone"
  case incomingRequest = "Incoming_request"
  case openDoor = "Open_Door"
  case closeDoor = "Close_Door"
  case restockRequired = "Restock_Required"
}

func playSound(audioName: audioName?) {
  guard let url = Bundle.main.url(forResource: audioName!.rawValue, withExtension: "mp3") else {
    loggerDebug.warning("Can't find \(audioName!.rawValue).mp3")
    return
  }

  do {
    try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
    try AVAudioSession.sharedInstance().setActive(true)

    // The following line is required for the player to work on iOS 11. Change the file type accordingly
    player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

    // iOS 10 and earlier require the following line:
    // player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3)

    guard let player = player else {
      loggerDebug.warning("Player is nil")
      return
    }

    player.play()

  } catch let error {
    loggerDebug.error("\(error.localizedDescription)")
  }
}

// Get FCM token
func getFCMToken<T: Decodable>(completion: @escaping (T?) -> Void) {

  Messaging.messaging().token { token, error in
    if let error = error {
      loggerDebug.error("Error fetching FCM registration token: \(error)")
      completion(nil)

    } else if let token = token {
      loggerDebug.trace("FCM registration token: \(token)")
      // Saving token to keychain
      let tokenToSave = KeyChain.stringToData(string: token)
      _ = KeyChain.save(key: .fcmtoken, data: tokenToSave)

      completion(token as? T)

    }
  }

  //    InstanceID.instanceID().instanceID { result, error in
  //        if let error = error {
  //            logger.error("Error fetching remote instance ID: \(error.localizedDescription)")
  //            completion(nil)
  //        } else if let result = result {
  //            logger.info("FCM Token: Remote instance ID token: \(result.token)")
  //
  //            // Saving token to keychain
  //            let token = KeyChain.StringToData(string: result.token)
  //            _ = KeyChain.save(key: .fcmtoken, data: token)
  //
  //
  //            completion(result.token as? T)
  //
  //        }
  //    }

}
