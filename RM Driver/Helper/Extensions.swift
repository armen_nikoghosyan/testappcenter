//
//  Extensions.swift
//  Robomart
//
//  Created by Macbook Pro on 19/04/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
  enum JPEGQuality: CGFloat {
    case lowest = 0
    case low = 0.25
    case medium = 0.5
    case high = 0.75
    case highest = 1
  }

  func getCropRatio() -> CGFloat {
    let widthRatio = CGFloat(size.width / size.height)
    return widthRatio
  }

  public func imageRotatedByDegrees(degrees: CGFloat) -> UIImage {
    // Calculate the size of the rotated view's containing box for our drawing space
    let rotatedViewBox = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
    let t = CGAffineTransform(rotationAngle: degrees * CGFloat.pi / 180)
    rotatedViewBox.transform = t
    let rotatedSize: CGSize = rotatedViewBox.frame.size
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize)
    let bitmap: CGContext = UIGraphicsGetCurrentContext()!
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    bitmap.translateBy(x: rotatedSize.width / 2, y: rotatedSize.height / 2)
    // Rotate the image context
    bitmap.rotate(by: degrees * CGFloat.pi / 180)
    // Now, draw the rotated/scaled image into the context
    bitmap.scaleBy(x: 1.0, y: -1.0)
    bitmap.draw(
      cgImage!,
      in: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height))
    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return newImage
  }

  func resized(withPercentage percentage: CGFloat) -> UIImage? {
    let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
    return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
      _ in draw(in: CGRect(origin: .zero, size: canvas))
    }
  }

  func resized(toWidth width: CGFloat) -> UIImage? {
    let canvas = CGSize(width: width, height: CGFloat(ceil(width / size.width * size.height)))
    return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
      _ in draw(in: CGRect(origin: .zero, size: canvas))
    }
  }
}

extension UIView {
  /// Rounds the given set of corners to the specified radius
  ///
  /// - parameter corners: Corners to round
  /// - parameter radius:  Radius to round to
  func round(corners: UIRectCorner, radius: CGFloat) {
    _ = _round(corners: corners, radius: radius)
  }

  /// Rounds the given set of corners to the specified radius with a border

  /// - parameter corners:     Corners to round
  /// - parameter radius:      Radius to round to
  /// - parameter borderColor: The border color
  /// - parameter borderWidth: The border width
  func round(corners: UIRectCorner, radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
    let mask = _round(corners: corners, radius: radius)
    addBorder(mask: mask, borderColor: borderColor, borderWidth: borderWidth)
  }

  /// Fully rounds an autolayout view (e.g. one with no known frame) with the given diameter and border

  /// - parameter diameter:    The view's diameter
  /// - parameter borderColor: The border color
  /// - parameter borderWidth: The border width

  func fullyRound(diameter: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
    layer.masksToBounds = true
    layer.cornerRadius = diameter / 2
    layer.borderWidth = borderWidth
    layer.borderColor = borderColor.cgColor
  }

  // func roundCorners(corners: UIRectCorner, radius: CGFloat) {
  //     let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
  //     let mask = CAShapeLayer()
  //     mask.path = path.cgPath
  //     self.layer.mask = mask
  // }

  @discardableResult func _round(corners: UIRectCorner, radius: CGFloat) -> CAShapeLayer {
    let path = UIBezierPath(
      roundedRect: bounds, byRoundingCorners: corners,
      cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    layer.mask = mask
    return mask
  }

  func addBorder(mask: CAShapeLayer, borderColor: UIColor, borderWidth: CGFloat) {
    let borderLayer = CAShapeLayer()
    borderLayer.path = mask.path
    borderLayer.fillColor = UIColor.clear.cgColor
    borderLayer.strokeColor = borderColor.cgColor
    borderLayer.lineWidth = borderWidth
    borderLayer.frame = bounds
    layer.addSublayer(borderLayer)
  }
}

/// Has safe area
///
/// with notch: 44.0 on iPhone X, XS, XS Max, XR.
///
/// without notch: 20.0 on iPhone 8 on iOS 12+.
///
var hasSafeArea: Bool {
  guard #available(iOS 11.0, *),
    let topPadding = UIApplication.shared.keyWindow?.safeAreaInsets.top, topPadding > 24
  else {
    loggerDebug.warning("ios 11.0 and more is not available, or topPadding <= 24")
    return false
  }
  return true
}

extension NSMutableAttributedString {
  @discardableResult func bold(_ text: String, font: fontName, fontSize: CGFloat?)
    -> NSMutableAttributedString
  {
    let attrs: [NSAttributedString.Key: Any] = [
      .font: UIFont(name: font.rawValue, size: fontSize!)!
    ]
    let boldString = NSMutableAttributedString(string: text, attributes: attrs)
    append(boldString)

    return self
  }

  @discardableResult func normal(_ text: String) -> NSMutableAttributedString {
    let normal = NSAttributedString(string: text)
    append(normal)

    return self
  }

  public func setAsLink(textToFind: String, linkURL: String) -> Bool {
    let foundRange = mutableString.range(of: textToFind)
    if foundRange.location != NSNotFound {
      addAttribute(.link, value: linkURL, range: foundRange)
      return true
    }
    return false
  }
}

extension UIColor {
  func as1ptImage() -> UIImage {
    UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
    setFill()
    UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
    let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    UIGraphicsEndImageContext()
    return image
  }
}

extension UITapGestureRecognizer {
  func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
    // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
    let layoutManager = NSLayoutManager()
    let textContainer = NSTextContainer(size: CGSize.zero)
    let textStorage = NSTextStorage(attributedString: label.attributedText!)

    // Configure layoutManager and textStorage
    layoutManager.addTextContainer(textContainer)
    textStorage.addLayoutManager(layoutManager)

    // Configure textContainer
    textContainer.lineFragmentPadding = 0.0
    textContainer.lineBreakMode = label.lineBreakMode
    textContainer.maximumNumberOfLines = label.numberOfLines
    let labelSize = label.bounds.size
    textContainer.size = labelSize

    // Find the tapped character location and compare it to the specified range
    let locationOfTouchInLabel = location(in: label)
    let textBoundingBox = layoutManager.usedRect(for: textContainer)
    // let textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
    // (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
    let textContainerOffset = CGPoint(
      x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
      y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)

    // let locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
    // locationOfTouchInLabel.y - textContainerOffset.y);
    let locationOfTouchInTextContainer = CGPoint(
      x: locationOfTouchInLabel.x - textContainerOffset.x,
      y: locationOfTouchInLabel.y - textContainerOffset.y)
    let indexOfCharacter = layoutManager.characterIndex(
      for: locationOfTouchInTextContainer, in: textContainer,
      fractionOfDistanceBetweenInsertionPoints: nil)
    return NSLocationInRange(indexOfCharacter, targetRange)
  }
}

extension UINavigationController {
  override open func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    let backButton = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
    topViewController?.navigationItem.backBarButtonItem = backButton
    topViewController?.navigationController?.navigationBar.tintColor = .black
    topViewController?.navigationController?.navigationBar.barTintColor = .white
    topViewController?.navigationController?.navigationBar.titleTextAttributes = [
      .foregroundColor: UIColor.black, .font: UIFont(name: "AvenirNext-Regular", size: 20)!,
    ]

    var backButtonImage = UIImage(named: "back-thin-icon")
    backButtonImage = backButtonImage?.withAlignmentRectInsets(
      UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0))
    topViewController?.navigationController?.navigationBar.backIndicatorImage = backButtonImage
    topViewController?.navigationController?.navigationBar.backIndicatorTransitionMaskImage =
      backButtonImage
  }
}

extension String {
  // Phone number formatting
  func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
    var pureNumber = replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
    for index in 0..<pattern.count {
      guard index < pureNumber.count else {
        loggerDebug.warning("index >= pureNumber.count (\(pureNumber.count))")
        return pureNumber
      }

      //            let stringIndex = String.Index(encodedOffset: index)
      let stringIndex = String.Index(utf16Offset: index, in: pureNumber)
      let patternCharacter = pattern[stringIndex]
      guard patternCharacter != replacmentCharacter else {
        loggerDebug.warning("patternCharacter == replacmentCharacter")
        continue
      }
      pureNumber.insert(patternCharacter, at: stringIndex)
    }
    return pureNumber
  }

  /// This method makes it easier extract a substring by character index where a character is viewed as a human-readable character (grapheme cluster).
  internal func substring(start: Int, offsetBy: Int) -> String? {
    guard let substringStartIndex = index(startIndex, offsetBy: start, limitedBy: endIndex) else {
      loggerDebug.warning("substringStartIndex is nil")
      return nil
    }

    guard let substringEndIndex = index(startIndex, offsetBy: start + offsetBy, limitedBy: endIndex)
    else {
      loggerDebug.warning("substringEndIndex is nil")
      return nil
    }

    return String(self[substringStartIndex..<substringEndIndex])
  }
}

func format(phoneNumber: String, shouldRemoveLastDigit: Bool = false) -> String {
  guard !phoneNumber.isEmpty else {
    loggerDebug.warning("Phone number is empty")
    return ""
  }
  guard let regex = try? NSRegularExpression(pattern: "[\\s-\\(\\)]", options: .caseInsensitive)
  else {
    loggerDebug.warning("regex doesn't match pattern")
    return ""
  }
  let r = NSString(string: phoneNumber).range(of: phoneNumber)
  var number = regex.stringByReplacingMatches(
    in: phoneNumber, options: .init(rawValue: 0), range: r, withTemplate: "")

  if number.count > 10 {
    let tenthDigitIndex = number.index(number.startIndex, offsetBy: 10)
    number = String(number[number.startIndex..<tenthDigitIndex])
  }

  if shouldRemoveLastDigit {
    let end = number.index(number.startIndex, offsetBy: number.count - 1)
    number = String(number[number.startIndex..<end])
  }

  if number.count < 7 {
    let end = number.index(number.startIndex, offsetBy: number.count)
    let range = number.startIndex..<end
    number = number.replacingOccurrences(
      of: "(\\d{3})(\\d+)", with: "($1) $2", options: .regularExpression, range: range)

  } else {
    let end = number.index(number.startIndex, offsetBy: number.count)
    let range = number.startIndex..<end
    number = number.replacingOccurrences(
      of: "(\\d{3})(\\d{3})(\\d+)", with: "($1) $2-$3", options: .regularExpression, range: range)
  }

  return number
}

extension UITextField {
  func setLeftPaddingPoints(_ amount: CGFloat) {
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: frame.size.height))
    leftView = paddingView
    leftViewMode = .always
  }

  func setRightPaddingPoints(_ amount: CGFloat) {
    let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: frame.size.height))
    rightView = paddingView
    rightViewMode = .always
  }
}

extension UIImage {
  var tonal: UIImage {
    let context = CIContext(options: nil)
    let currentFilter = CIFilter(name: "CIPhotoEffectTonal")!
    currentFilter.setValue(CIImage(image: self), forKey: kCIInputImageKey)
    let output = currentFilter.outputImage!
    let cgImage = context.createCGImage(output, from: output.extent)!
    let processedImage = UIImage(cgImage: cgImage, scale: scale, orientation: imageOrientation)

    return processedImage
  }

  /// Creates a circular outline image.
  class func outlinedEllipse(size: CGSize, color: UIColor, lineWidth: CGFloat = 1.0) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    guard let context = UIGraphicsGetCurrentContext() else {
      loggerDebug.warning("The current graphics context is nil")
      return nil
    }

    context.setStrokeColor(color.cgColor)
    context.setLineWidth(lineWidth)
    // Inset the rect to account for the fact that strokes are
    // centred on the bounds of the shape.
    let rect = CGRect(origin: .zero, size: size).insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
    context.addEllipse(in: rect)
    context.strokePath()

    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return image
  }
}

extension UITableView {
  func deselectSelectedRow(animated: Bool) {
    if let indexPathForSelectedRow = self.indexPathForSelectedRow {
      deselectRow(at: indexPathForSelectedRow, animated: animated)
    }
  }
}

extension UIPageControl {
  func customPageControl(dotFillColor: UIColor, dotBorderColor: UIColor, dotBorderWidth: CGFloat) {
    for (pageIndex, dotView) in subviews.enumerated() {
      if currentPage == pageIndex {
        dotView.backgroundColor = dotFillColor
        dotView.layer.cornerRadius = dotView.frame.size.height / 2
      } else {
        dotView.backgroundColor = .clear
        dotView.layer.cornerRadius = dotView.frame.size.height / 2
        dotView.layer.borderColor = dotBorderColor.cgColor
        dotView.layer.borderWidth = dotBorderWidth
      }
    }
  }
}

extension Bundle {
  var versionNumber: String? {
    return infoDictionary?["CFBundleShortVersionString"] as? String
  }
  var buildVersionNumber: String? {
    return infoDictionary?["CFBundleVersion"] as? String
  }
}
