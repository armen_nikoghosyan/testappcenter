//
//  DateService.swift
//  Robomart
//
//  Created by Macbook Pro on 08/04/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import Foundation

struct DateService {
  static let shared = DateService()

  private init() {}

  func dateCalc(new: String) -> String {
    if let time = Int(new) {
      let newTime = Double(time)
      let x = (newTime / 1000)
      let date = NSDate(timeIntervalSince1970: x)
      let formatter = DateFormatter()
      formatter.dateStyle = .medium
      formatter.timeStyle = .short
      formatter.dateFormat = "dd MMM, yyyy 'at' h:mm a"
      return formatter.string(for: date)!
    } else {
      return ""
    }
  }

  func getDate(new: String) -> String {
    if let time = Int(new) {
      let newTime = Double(time)
      let x = (newTime / 1000)
      let date = NSDate(timeIntervalSince1970: x)
      let formatter = DateFormatter()
      formatter.dateStyle = .medium
      formatter.dateFormat = "dd MMM, yyyy"
      return formatter.string(for: date)!
    } else {
      return ""
    }
  }

  func getTime(new: String) -> String {
    if let time = Int(new) {
      let newTime = Double(time)
      let x = (newTime / 1000)
      let date = NSDate(timeIntervalSince1970: x)
      let formatter = DateFormatter()
      formatter.timeStyle = .short
      return formatter.string(for: date)!
    } else {
      return ""
    }
  }

  func getDay(new: String) -> String {
    if let time = Int(new) {
      let newTime = Double(time)
      let x = (newTime / 1000)
      let date = NSDate(timeIntervalSince1970: x)
      let formatter = DateFormatter()
      formatter.dateFormat = "dd"
      return formatter.string(for: date)!
    } else {
      return ""
    }
  }

  func getMonth(new: String) -> String {
    if let time = Int(new) {
      let newTime = Double(time)
      let x = (newTime / 1000)
      let date = NSDate(timeIntervalSince1970: x)
      let formatter = DateFormatter()
      formatter.dateFormat = "MMM"
      return formatter.string(for: date)!
    } else {
      return ""
    }
  }

  func getMonthInNumber(new: String) -> String {
    if let time = Int(new) {
      let newTime = Double(time)
      let x = (newTime / 1000)
      let date = NSDate(timeIntervalSince1970: x)
      let formatter = DateFormatter()
      formatter.dateFormat = "MM"
      return formatter.string(for: date)!
    } else {
      return ""
    }
  }

  func getYear(new: String) -> String {
    if let time = Int(new) {
      let newTime = Double(time)
      let x = (newTime / 1000)
      let date = NSDate(timeIntervalSince1970: x)
      let formatter = DateFormatter()
      formatter.dateFormat = "YYYY"
      return formatter.string(for: date)!
    } else {
      return ""
    }
  }

  func getFormattedCurrency(amount: Float) -> String {
    let numberFormatter = NumberFormatter()
    numberFormatter.numberStyle = .currency
    numberFormatter.locale = Locale.current
    numberFormatter.currencySymbol = ""
    numberFormatter.maximumFractionDigits = 2
    numberFormatter.minimumFractionDigits = 2

    if let formattedAmount = numberFormatter.string(from: NSNumber(value: amount)) {
      return formattedAmount
    }

    return ""
  }
}
