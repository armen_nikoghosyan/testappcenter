//
//  Colors.swift
//  Robomart
//
//  Created by Macbook Pro on 12/04/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
  static let shared = Colors()
  var buttonBlue = UIColor(red: 0 / 255.0, green: 121 / 255.0, blue: 211 / 255.0, alpha: 1.0)
  var buttonDarkBlue = UIColor(red: 0 / 255.0, green: 141 / 255.0, blue: 231 / 255.0, alpha: 1.0)
  var logoBlue = UIColor(red: 59 / 255.0, green: 129 / 255.0, blue: 165 / 255.0, alpha: 1.0)
  var maroon = UIColor(red: 154 / 255.0, green: 27 / 255.0, blue: 27 / 255.0, alpha: 1.0)
  var yellow = UIColor(red: 255 / 255.0, green: 210 / 255.0, blue: 0 / 255.0, alpha: 1.0)
  var lightYellow = UIColor(red: 251 / 255.0, green: 254 / 255.0, blue: 212 / 255.0, alpha: 1.0)
  var brwon = UIColor(red: 153 / 255.0, green: 102 / 255.0, blue: 0 / 255.0, alpha: 1.0)
  var green = UIColor(red: 183 / 255.0, green: 200 / 255.0, blue: 41 / 255.0, alpha: 1.0)
  var lightgreen = UIColor(red: 86 / 255.0, green: 174 / 255.0, blue: 0 / 255.0, alpha: 1.0)
  var darkGreen = UIColor(red: 102 / 255.0, green: 102 / 255.0, blue: 0 / 255.0, alpha: 1.0)
  var darkRed = UIColor(red: 182 / 255.0, green: 10 / 255.0, blue: 10 / 255.0, alpha: 1.0)
  var teal = UIColor(red: 12 / 255.0, green: 164 / 255.0, blue: 187 / 255.0, alpha: 1.0)
  var normalRed = UIColor(red: 255 / 255.0, green: 0 / 255.0, blue: 0 / 255.0, alpha: 1.0)
}
