//
//  BillItemCell.swift
//  Robomart
//
//  Created by Macbook Pro on 12/07/2019.
//  Copyright © 2019 Fahad Naqvi. All rights reserved.
//

import UIKit

class BillItemCell: UITableViewCell {
  let productName = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Bananas")
  let productImage = Components.shared.customImageView(
    cornerRadius: 15, clipToBounds: true, contentMode: .scaleAspectFit)

  let productQuantityLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "1x")

  let productPriceLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "0.0"
  )

  var billItem: OrderItem! {
    didSet {
      productQuantityLabel.text = "\(billItem.qty!)x"
      //            ProductImage.image = UIImage(named: billItem.itemImage!)

      if let productImageURL = URL(string: billItem.img!) {
        productImage.kf.indicatorType = .activity
        productImage.kf.setImage(with: productImageURL)

      } else {
        productImage.image = #imageLiteral(resourceName: "default-avatar")
      }

      productName.text = billItem.productName!
      //            let totalPrice = billItem.price! * Double(billItem.qty!)
      productPriceLabel.text = String(format: "$%.2f", billItem.price!)  //"$\(billItem.price!)"
    }
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    selectionStyle = .none

    addSubview(productQuantityLabel)
    productQuantityLabel.textColor = .init(white: 0.5, alpha: 1)
    productQuantityLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive =
      true
    productQuantityLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30).isActive =
      true

    addSubview(productImage)
    productImage.leadingAnchor.constraint(
      equalTo: productQuantityLabel.trailingAnchor, constant: 15
    ).isActive = true
    productImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    productImage.widthAnchor.constraint(equalToConstant: 25).isActive = true
    productImage.heightAnchor.constraint(equalToConstant: 25).isActive = true

    addSubview(productName)
    //        ProductName.backgroundColor = .red
    productName.textColor = .init(white: 0.5, alpha: 1)
    productName.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    productName.leadingAnchor.constraint(equalTo: productImage.trailingAnchor, constant: 15)
      .isActive = true
    //        ProductName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -100).isActive = true

    addSubview(productPriceLabel)
    productPriceLabel.textColor = .init(white: 0.5, alpha: 1)
    productPriceLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    productPriceLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -30).isActive =
      true

    productName.trailingAnchor.constraint(equalTo: productPriceLabel.leadingAnchor, constant: -10)
      .isActive = true
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
