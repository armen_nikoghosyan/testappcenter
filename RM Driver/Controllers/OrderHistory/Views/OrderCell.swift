//
//  CityCell.swift
//  Robomart
//
//  Created by Macbook Pro on 09/07/2019.
//  Copyright © 2019 Robomart. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
  let radioImage = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFill)
  let userImage = Components.shared.customImageView(
    cornerRadius: 20, clipToBounds: true, contentMode: .scaleAspectFill)
  let arrowImage = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFit)

  let orderTitleLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Groceries")

  let dateLabel = Components.shared.customLabel(
    fontSize: 12, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Pakistan")
  let priceLabel = Components.shared.customLabel(
    fontSize: 16, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0,
    text: "$24.99")

  let borderView = Components.shared.customView()

  let statusLabel: PaddingLabel = {
    let label = PaddingLabel(padding: UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5))
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Paid"
    label.textColor = UIColor(white: 0.3, alpha: 1)
    label.font = UIFont(name: "Avenir-Black", size: 8)
    label.textAlignment = .center
    label.layer.cornerRadius = 2
    label.backgroundColor = UIColor(white: 1, alpha: 1)
    label.layer.borderColor = UIColor(white: 0.6, alpha: 1).cgColor
    label.layer.borderWidth = 1
    return label
  }()

  var orderTitleLabelCenterYConstraints: NSLayoutConstraint!

  var order: OrderHistoryRecord! {
    didSet {
      if let imageURL = URL(string: order.typeImageURL!) {
        userImage.kf.indicatorType = .activity
        userImage.kf.setImage(with: imageURL)
      } else {
        userImage.image = UIImage(named: "default-avatar")
      }

      orderTitleLabel.text = order.typeName!
      priceLabel.text = "$\(DateService.shared.getFormattedCurrency(amount: Float(order.total!)))"
      dateLabel.text = DateService.shared.getDate(new: order.requestTime!)

      statusLabel.text = order.status!

      //            if users.designation! != "" {
      //                userNameLabelCenterYConstraints.constant = -8
      //            }else{
      //                userNameLabelCenterYConstraints.constant = 0
      //            }
    }
  }

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none
    backgroundColor = .white
    setupUI()
  }

  func setupUI() {
    addSubview(userImage)
    userImage.layer.borderWidth = 1
    userImage.layer.borderColor = UIColor(white: 0.6, alpha: 1).cgColor
    userImage.image = UIImage(named: "produce")
    userImage.contentMode = .scaleAspectFill
    userImage.backgroundColor = .init(white: 0.9, alpha: 1)
    userImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    userImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 35).isActive = true
    userImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
    userImage.heightAnchor.constraint(equalToConstant: 40).isActive = true

    addSubview(arrowImage)
    arrowImage.image = UIImage(named: "right-arrow")
    arrowImage.backgroundColor = .clear
    arrowImage.tintColor = .init(white: 0.7, alpha: 1)
    arrowImage.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    arrowImage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -35).isActive = true
    arrowImage.widthAnchor.constraint(equalToConstant: 10).isActive = true
    arrowImage.heightAnchor.constraint(equalToConstant: 15).isActive = true

    addSubview(orderTitleLabel)
    orderTitleLabel.textColor = .init(white: 0, alpha: 1)
    //        orderTitleLabelCenterYConstraints = orderTitleLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -8)
    //        orderTitleLabelCenterYConstraints?.isActive = true
    orderTitleLabel.topAnchor.constraint(equalTo: userImage.topAnchor, constant: 5).isActive = true
    orderTitleLabel.leadingAnchor.constraint(equalTo: userImage.trailingAnchor, constant: 10)
      .isActive = true
    //        orderTitleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -45).isActive = true

    addSubview(dateLabel)
    dateLabel.textColor = .init(white: 0.6, alpha: 1)
    dateLabel.topAnchor.constraint(equalTo: orderTitleLabel.bottomAnchor, constant: 0).isActive =
      true
    dateLabel.leadingAnchor.constraint(equalTo: orderTitleLabel.leadingAnchor, constant: 0).isActive =
      true
    //        dateLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -45).isActive = true

    addSubview(priceLabel)
    priceLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -8).isActive = true
    priceLabel.trailingAnchor.constraint(equalTo: arrowImage.leadingAnchor, constant: -15).isActive =
      true

    addSubview(statusLabel)
    statusLabel.text = "PAID"
    statusLabel.trailingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 0).isActive =
      true
    statusLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 0).isActive = true
    statusLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true

    addSubview(borderView)
    borderView.backgroundColor = .init(white: 0.9, alpha: 1)
    borderView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    borderView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 35).isActive = true
    borderView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -35).isActive = true
    borderView.heightAnchor.constraint(equalToConstant: 1).isActive = true
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)

    radioImage.image = selected ? UIImage(named: "radio-selected") : UIImage(named: "radio")
  }
}
