//
//  ReceiptVC.swift
//  Robomart
//
//  Created by Syed Fahad ahmed naqvi on 21/05/2018.
//  Copyright © 2018 Fahad Naqvi. All rights reserved.
//

import Firebase
import Intercom
import Stripe
import SwiftyJSON
import UIKit

class ReceiptVC: UIViewController {
  private let billItemCellId = "billItemCellId"

  var checkoutItemsArray: [DataSnapshot] = []
  let customNavigationbar = CustomNavigation()

  let mapImage = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFill)
  //    let nextButton = Components.shared.customButton(title: "DONE", fontName: .avenirNextRegular, fontSize: 18, imageName: "")

  let mainScrollView = Components.shared.customScrollView()
  let containerView = Components.shared.customView()
  let mainTitle = Components.shared.customLabel(
    fontSize: 18, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0, text: "")

  let productImageView: UIImageView = {
    let imageView = UIImageView()
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.image = UIImage(named: "produce")
    imageView.layer.cornerRadius = 40
    imageView.layer.borderColor = UIColor(white: 0.6, alpha: 1).cgColor
    imageView.layer.borderWidth = 1
    imageView.clipsToBounds = true

    //        imageView.alpha = 0

    return imageView
  }()

  let bottomLine = Components.shared.customView()
  let billBottomLine = Components.shared.customView()
  let disclaimerLabel = Components.shared.customLabel(
    fontSize: 11, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text:
      "We have charged your card on file this total amount. If you feel there has been a mistake, please click the helpline."
  )
  let cardTextLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Visa Ending in ")

  let receiptNumberLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0,
    text: "Receipt #")
  let dateAndTimeLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0, text: "")
  let vehicleNumberLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0,
    text: "Robomart Cameo 7NVM786")

  let totalInclTaxTitleLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextBold, fontAlignment: .left, numberOfLines: 0,
    text: "Total Incl. Tax")
  let taxLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0, text: "")
  let totalInclTaxLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextBold, fontAlignment: .right, numberOfLines: 0, text: "")

  let hailingFeeTitleLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextMedium, fontAlignment: .left, numberOfLines: 0,
    text: "Hailing Fee")
  let hailingFeeLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextMedium, fontAlignment: .right, numberOfLines: 0, text: "")

  let taxTitleLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0, text: "TAX")
  let taxPercentageLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "")

  let taxPriceLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "")

  let subTotalTitleLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextMedium, fontAlignment: .left, numberOfLines: 0,
    text: "Subtotal")
  let subTotalMidTitleLabel = Components.shared.customLabel(
    fontSize: 15, fontName: .avenirNextRegular, fontAlignment: .right, numberOfLines: 0, text: "")

  let subTotalPriceLabel = Components.shared.customLabel(
    fontSize: 13, fontName: .avenirNextMedium, fontAlignment: .right, numberOfLines: 0, text: "")

  let statusLabel: PaddingLabel = {
    let label = PaddingLabel(padding: UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10))
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = "Paid"
    label.textColor = UIColor(white: 0.3, alpha: 1)
    label.font = UIFont(name: "Avenir-Black", size: 12)
    label.textAlignment = .center
    label.layer.cornerRadius = 2
    label.backgroundColor = UIColor(white: 1, alpha: 1)
    label.layer.borderColor = UIColor(white: 0.6, alpha: 1).cgColor
    label.layer.borderWidth = 1
    return label
  }()

  let billItemsListTV = Components.shared.customUITableView()
  var billItemsListTVHeightConstraints: NSLayoutConstraint!
  var cellHeight: CGFloat? = 35

  var billItemsBGTextLabel = Components.shared.customLabel(
    fontSize: 16, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 0,
    text: "No items were purchased")
  var billItemsListArray = [OrderItem]()

  let userDefaults = UserDefaults.standard

  var ref: DatabaseReference!
  //    let user : User? = AppUser.sharedInstance.getData(forKey: .currentUser)

  var orderID = ""
  var fromOrderHistory: Bool? = false

  var productImageViewTopConstraints: NSLayoutConstraint?

  override func viewDidLoad() {
    super.viewDidLoad()
    ref = Database.database().reference().child("ActiveCustomers")

    customNavigationBarSetup()
    setupUI()

    getOrderDetails()
    //        userRemoved()

    //        receiptViewAction()
  }

  // custom navigation bar init
  func customNavigationBarSetup() {
    view.addSubview(customNavigationbar)
    customNavigationbar.delegate = self

    if fromOrderHistory! {
      customNavigationbar.backButton.setImage(UIImage(named: "back-thin-icon"), for: .normal)
    } else {
      customNavigationbar.backButton.setImage(
        #imageLiteral(resourceName: "close-icon"), for: .normal)
    }

    customNavigationbar.rightBarButton.imageEdgeInsets = UIEdgeInsets(
      top: 8, left: 8, bottom: 8, right: 8)
    customNavigationbar.rightBarButton.addTarget(
      self, action: #selector(helpAction(_:)), for: .touchUpInside)
    customNavigationbar.backgroundColor = .init(white: 0.95, alpha: 1)
    customNavigationbar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    customNavigationbar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    customNavigationbar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
      .isActive = true
    customNavigationbar.heightAnchor.constraint(equalToConstant: 70).isActive = true
  }

  // open intercom chat
  @objc func helpAction(_: UIButton) {
    Intercom.presentMessenger()
  }

  // view receipt when order completed
  private func receiptViewAction() {

    //        DispatchQueue.main.async {
    //            APIManager.sharedInstance.get(endpoint: .recieptViewed) { (res, error) in
    //
    //                if let err = error {
    //                    logger.error("\(err.localizedDescription)")
    //                } else {
    //                    do {
    //                        let json = try JSON(data: res!)
    //                        logger.info("ReceiptVC: json data: \(json)")
    //                    } catch let jsonError {
    //                        logger.error("\(jsonError.localizedDescription)")
    //                    }
    //                }
    //
    //            }
    //        }

  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    loggerDebug.debug("closing receipt")
    if !fromOrderHistory! {

      // Add intercom Receipt generated event
      // Intercom.logEvent(withName: "Shopping started", metaData: [ "robomart type": currentRobomart!.typeName!, "robomart name":  currentRobomart!.vehicleName!])

    }
  }

  // Setup page UI
  func setupUI() {
    view.addSubview(mainScrollView)
    // mainScrollView.backgroundColor = UIColor .blue
    mainScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    mainScrollView.topAnchor.constraint(equalTo: customNavigationbar.bottomAnchor, constant: 0)
      .isActive = true
    mainScrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0).isActive = true
    mainScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    // mainScrollView.heightAnchor.constraint(equalToConstant: view.frame.size.height).isActive = true

    mainScrollView.addSubview(containerView)
    // containerView.backgroundColor = UIColor.red
    containerView.topAnchor.constraint(equalTo: mainScrollView.topAnchor, constant: 0).isActive =
      true
    containerView.leadingAnchor.constraint(equalTo: mainScrollView.leadingAnchor, constant: 0)
      .isActive = true
    containerView.trailingAnchor.constraint(equalTo: mainScrollView.trailingAnchor, constant: 0)
      .isActive = true
    containerView.bottomAnchor.constraint(equalTo: mainScrollView.bottomAnchor, constant: 0)
      .isActive = true
    containerView.widthAnchor.constraint(equalTo: mainScrollView.widthAnchor, constant: 0).isActive =
      true

    containerView.addSubview(mapImage)
    mapImage.backgroundColor = .init(white: 0.8, alpha: 1)
    mapImage.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0).isActive = true
    mapImage.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
    mapImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    mapImage.heightAnchor.constraint(equalToConstant: 160).isActive = true

    containerView.addSubview(receiptNumberLabel)
    receiptNumberLabel.textColor = .init(white: 0.7, alpha: 1)
    receiptNumberLabel.topAnchor.constraint(equalTo: mapImage.bottomAnchor, constant: 15).isActive =
      true
    receiptNumberLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =
      true

    containerView.addSubview(statusLabel)
    statusLabel.text = ""
    statusLabel.isHidden = true
    statusLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
    statusLabel.topAnchor.constraint(equalTo: receiptNumberLabel.bottomAnchor, constant: 10)
      .isActive = true
    statusLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true

    containerView.addSubview(productImageView)
    productImageView.backgroundColor = .init(white: 0.7, alpha: 1)
    productImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =
      true
    productImageViewTopConstraints = productImageView.topAnchor.constraint(
      equalTo: statusLabel.bottomAnchor, constant: 15)
    productImageViewTopConstraints?.isActive = true
    productImageView.widthAnchor.constraint(equalToConstant: 80).isActive = true
    productImageView.heightAnchor.constraint(equalToConstant: 80).isActive = true

    containerView.addSubview(mainTitle)
    mainTitle.text = mainTitle.text?.uppercased()
    mainTitle.topAnchor.constraint(equalTo: productImageView.bottomAnchor, constant: 15).isActive =
      true
    mainTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true

    containerView.addSubview(dateAndTimeLabel)
    dateAndTimeLabel.textColor = .init(white: 0.7, alpha: 1)
    dateAndTimeLabel.topAnchor.constraint(equalTo: mainTitle.bottomAnchor, constant: 5).isActive =
      true
    dateAndTimeLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =
      true

    containerView.addSubview(vehicleNumberLabel)
    vehicleNumberLabel.textColor = .init(white: 0.3, alpha: 1)
    vehicleNumberLabel.topAnchor.constraint(equalTo: dateAndTimeLabel.bottomAnchor, constant: 5)
      .isActive = true
    vehicleNumberLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =
      true

    setupTV()

    containerView.addSubview(billBottomLine)
    billBottomLine.backgroundColor = .init(white: 0.8, alpha: 1)
    billBottomLine.topAnchor.constraint(equalTo: billItemsListTV.bottomAnchor, constant: 10)
      .isActive = true
    billBottomLine.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive =
      true
    billBottomLine.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive =
      true
    billBottomLine.heightAnchor.constraint(equalToConstant: 1).isActive = true

    view.addSubview(subTotalTitleLabel)
    subTotalTitleLabel.topAnchor.constraint(equalTo: billBottomLine.bottomAnchor, constant: 10)
      .isActive = true
    subTotalTitleLabel.leadingAnchor.constraint(equalTo: billBottomLine.leadingAnchor, constant: 0)
      .isActive = true

    //        view.addSubview(SubTotalMidTitleLabel)
    //        SubTotalMidTitleLabel.topAnchor.constraint(equalTo: billBottomLine.bottomAnchor, constant: 10).isActive = true
    //
    view.addSubview(subTotalPriceLabel)
    subTotalPriceLabel.topAnchor.constraint(equalTo: billBottomLine.bottomAnchor, constant: 10)
      .isActive = true
    subTotalPriceLabel.trailingAnchor.constraint(
      equalTo: billBottomLine.trailingAnchor, constant: 0
    ).isActive = true

    //        view.addSubview(TaxTitleLabel)
    //        TaxTitleLabel.topAnchor.constraint(equalTo: SubTotalTitleLabel.bottomAnchor, constant: 10).isActive = true
    //        TaxTitleLabel.leadingAnchor.constraint(equalTo: billBottomLine.leadingAnchor, constant: 0).isActive = true
    //
    //        view.addSubview(TaxPercentageLabel)
    //        TaxPercentageLabel.topAnchor.constraint(equalTo: SubTotalPriceLabel.bottomAnchor, constant: 10).isActive = true
    //
    //        view.addSubview(TaxPriceLabel)
    //        TaxPriceLabel.topAnchor.constraint(equalTo: SubTotalPriceLabel.bottomAnchor, constant: 10).isActive = true
    //        TaxPriceLabel.trailingAnchor.constraint(equalTo: billBottomLine.trailingAnchor, constant: 0).isActive = true
    //
    containerView.addSubview(hailingFeeTitleLabel)
    hailingFeeTitleLabel.topAnchor.constraint(
      equalTo: subTotalTitleLabel.bottomAnchor, constant: 10
    ).isActive = true
    hailingFeeTitleLabel.leadingAnchor.constraint(
      equalTo: billBottomLine.leadingAnchor, constant: 0
    ).isActive = true

    containerView.addSubview(hailingFeeLabel)
    hailingFeeLabel.topAnchor.constraint(equalTo: subTotalTitleLabel.bottomAnchor, constant: 10)
      .isActive = true
    hailingFeeLabel.trailingAnchor.constraint(equalTo: billBottomLine.trailingAnchor, constant: 0)
      .isActive = true

    containerView.addSubview(totalInclTaxTitleLabel)
    totalInclTaxTitleLabel.topAnchor.constraint(
      equalTo: hailingFeeTitleLabel.bottomAnchor, constant: 10
    ).isActive = true
    totalInclTaxTitleLabel.leadingAnchor.constraint(
      equalTo: billBottomLine.leadingAnchor, constant: 0
    ).isActive = true

    containerView.addSubview(taxLabel)
    taxLabel.topAnchor.constraint(equalTo: hailingFeeTitleLabel.bottomAnchor, constant: 10).isActive =
      true
    taxLabel.centerXAnchor.constraint(equalTo: billBottomLine.centerXAnchor, constant: 0).isActive =
      true

    //        TaxPercentageLabel.trailingAnchor.constraint(equalTo: TaxLabel.trailingAnchor, constant: 0).isActive = true
    //        SubTotalMidTitleLabel.trailingAnchor.constraint(equalTo: TaxLabel.trailingAnchor, constant: 0).isActive = true

    containerView.addSubview(totalInclTaxLabel)
    totalInclTaxLabel.topAnchor.constraint(equalTo: hailingFeeTitleLabel.bottomAnchor, constant: 10)
      .isActive = true
    //        priceLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0).isActive = true
    totalInclTaxLabel.trailingAnchor.constraint(equalTo: billBottomLine.trailingAnchor, constant: 0)
      .isActive = true

    containerView.addSubview(bottomLine)
    bottomLine.backgroundColor = .init(white: 0.8, alpha: 1)
    bottomLine.topAnchor.constraint(equalTo: totalInclTaxLabel.bottomAnchor, constant: 10).isActive =
      true
    bottomLine.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
    bottomLine.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive =
      true
    bottomLine.heightAnchor.constraint(equalToConstant: 1).isActive = true

    containerView.addSubview(cardTextLabel)
    cardTextLabel.textColor = .init(white: 0.5, alpha: 1)
    cardTextLabel.topAnchor.constraint(equalTo: bottomLine.bottomAnchor, constant: 15).isActive =
      true
    cardTextLabel.leadingAnchor.constraint(equalTo: bottomLine.leadingAnchor, constant: 0).isActive =
      true

    containerView.addSubview(disclaimerLabel)
    disclaimerLabel.textColor = .init(white: 0.6, alpha: 1)
    disclaimerLabel.topAnchor.constraint(equalTo: cardTextLabel.bottomAnchor, constant: 10).isActive =
      true
    disclaimerLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive =
      true
    disclaimerLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive =
      true
    disclaimerLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -10)
      .isActive = true

    if fromOrderHistory! {
      // statusLabel.isHidden = false
      productImageViewTopConstraints?.isActive = false
      productImageViewTopConstraints = productImageView.topAnchor.constraint(
        equalTo: statusLabel.bottomAnchor, constant: 15)
      productImageViewTopConstraints?.isActive = true
    } else {
      statusLabel.isHidden = true
      productImageViewTopConstraints?.isActive = false
      productImageViewTopConstraints = productImageView.topAnchor.constraint(
        equalTo: receiptNumberLabel.bottomAnchor, constant: 15)
      productImageViewTopConstraints?.isActive = true
    }
  }

  // Setup tableview for items
  func setupTV() {
    billItemsListTV.register(BillItemCell.self, forCellReuseIdentifier: billItemCellId)

    billItemsListTV.delegate = self
    billItemsListTV.dataSource = self

    billItemsListTV.separatorStyle = .none
    billItemsListTV.keyboardDismissMode = .interactive
    billItemsListTV.alwaysBounceVertical = false
    billItemsListTV.isScrollEnabled = false

    containerView.addSubview(billItemsListTV)
    billItemsListTV.backgroundColor = .init(white: 1, alpha: 1)
    billItemsListTV.topAnchor.constraint(equalTo: vehicleNumberLabel.bottomAnchor, constant: 15)
      .isActive = true
    billItemsListTV.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    billItemsListTV.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive =
      true
    billItemsListTVHeightConstraints = billItemsListTV.heightAnchor.constraint(equalToConstant: 250)
    billItemsListTVHeightConstraints?.isActive = true
    // BillItemsListTV.bottomAnchor.constraint(equalTo: billBottomLine.topAnchor, constant: 0).isActive = true

    billItemsListTV.addSubview(billItemsBGTextLabel)
    billItemsBGTextLabel.isHidden = true
    billItemsBGTextLabel.textColor = .init(white: 0.7, alpha: 1)
    billItemsBGTextLabel.centerXAnchor.constraint(
      equalTo: billItemsListTV.centerXAnchor, constant: 0
    ).isActive = true
    billItemsBGTextLabel.centerYAnchor.constraint(
      equalTo: billItemsListTV.centerYAnchor, constant: 0
    ).isActive = true
  }

  // Get order details
  func getOrderDetails() {

    APIManager.sharedInstance.get(endpoint: .getOrder(id: "\(orderID)")) { res, error in
      if let err = error {
        loggerDebug.error("\(err.localizedDescription)")
        AlertService.shared.alert(
          in: self, message: "Failed to get Order Details : \(err.localizedDescription)")
      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace("\(json)")

          let response = try JSONDecoder().decode(Order.self, from: res!)
          let orderDetails = response.data!

          loggerDebug.debug("order details : \(orderDetails)")

          self.mainTitle.text = orderDetails.typeName ?? ""

          if let img = orderDetails.mapImageURL {
            if img != "" {
              loggerDebug.trace("Image URL : \(img)")
              if let imageURL = URL(string: img) {
                self.mapImage.kf.indicatorType = .activity
                self.mapImage.kf.setImage(with: imageURL)
              } else {
                self.mapImage.image = UIImage(named: "default-img")
              }
            } else {
              self.mapImage.image = UIImage(named: "default-img")
            }
          } else {
            self.mapImage.image = UIImage(named: "default-img")
          }

          if let catImage = orderDetails.typeImageURL {
            if let imageURL = URL(string: catImage) {
              self.productImageView.kf.indicatorType = .activity
              self.productImageView.kf.setImage(with: imageURL)
            } else {
              self.productImageView.image = UIImage(named: "default-img")
            }

          } else {
            self.productImageView.image = UIImage(named: "default-img")
          }

          let date = DateService.shared.getDate(new: (orderDetails.requestTime!))
          let time = DateService.shared.getTime(new: (orderDetails.requestTime!))

          self.dateAndTimeLabel.text = "\(date) - \(time)"
          self.receiptNumberLabel.text = "Receipt # \(orderDetails.receiptNo ?? "")"

          if self.fromOrderHistory! {
            self.statusLabel.isHidden = false
            self.statusLabel.text = orderDetails.status ?? ""
          }

          self.vehicleNumberLabel.text = "Robomart \(orderDetails.vehicleName ?? "")"
          self.cardTextLabel.text = "Visa Ending in \(orderDetails.cardLast4 ?? "")"

          self.billItemsListArray = orderDetails.items!  // itemsList.records!

          self.billItemsListArray = self.billItemsListArray.sorted(by: {
            $0.productName! < $1.productName!
          })

          let height = self.cellHeight! * CGFloat(self.billItemsListArray.count)

          if height > self.cellHeight! {
            self.billItemsListTVHeightConstraints?.constant =
              self.cellHeight! * CGFloat(self.billItemsListArray.count)
          } else {
            self.billItemsListTVHeightConstraints?.constant = 100
          }

          if self.billItemsListArray.count > 0 {
            self.billItemsBGTextLabel.isHidden = true
          } else {
            self.billItemsBGTextLabel.isHidden = false
          }

          self.subTotalPriceLabel.text =
            "$\(DateService.shared.getFormattedCurrency(amount: Float(orderDetails.subtotal!)))"
          // ^ String(format: "$%.2f", orderDetails.subtotal!)//"$\(orderDetails!.subtotal!)"
          self.totalInclTaxLabel.text =
            "$\(DateService.shared.getFormattedCurrency(amount: Float(orderDetails.total!)))"
          //String(format: "$%.2f", orderDetails.total!)//"$\(orderDetails!.total!)"

          self.taxPriceLabel.text =
            "$\(DateService.shared.getFormattedCurrency(amount: Float(orderDetails.tax!)))"
          //String(format: "$%.2f", orderDetails.tax!)//"$\(orderDetails!.tax!)" // DateService.shared.getFormattedCurrency(amount: itemsList.tax!)

          self.hailingFeeLabel.text =
            "$\(DateService.shared.getFormattedCurrency(amount: Float(orderDetails.hailingFee!)))"
          //String(format: "$%.2f", orderDetails.hailingFee!)//"$\(orderDetails!.hailingFee!)"
          self.billItemsListTV.reloadData()

          if !self.fromOrderHistory! {

            // Add intercom Receipt generated event
            Intercom.logEvent(
              withName: "Receipt generated",
              metaData: [
                "robomart type": orderDetails.typeName ?? "", "total $": orderDetails.total ?? 0,
                "receipt no.": orderDetails.receiptNo ?? "",
              ])
          }

        } catch let jsonError {
          loggerDebug.error("Failed to get order details : \(jsonError.localizedDescription)")
          // AlertService.shared.alert(in: self, message: "Failed to get Order Details : \(jsonError.localizedDescription)")
        }
      }
    }
  }

  // Deleting user from firebase
  //    @objc func deleteUserAction() {
  //        print("ReceiptVC: delete user action")
  //        userDefaults.removeObject(forKey: "order_id")
  //
  //        let user = AppUser.sharedInstance.getCurrentUser()
  //
  //        let refForDel = Database.database().reference().child("ActiveCustomers/\(user!.id!)")
  //
  //        refForDel.removeValue { error, _ in
  //
  //            print(error)
  //        }
  //        if fromOrderHistory! {
  //            _ = navigationController?.popViewController(animated: true)
  //
  //        } else {
  //            presentingViewController?.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
  //        }
  //    }

  func userRemoved() {
    loggerDebug.notice("ReceiptVC: user Removed")

    ref.observe(.childRemoved) { snap in

      loggerDebug.debug("Removed : \(snap.key)")

      // if snap.key == self.user?.id! {
      //     self.dismiss(animated: true, completion: nil)
      //     self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
      // }
    }
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
}

// MARK: UITableViewDelegate and UITableViewDataSource

extension ReceiptVC: UITableViewDelegate, UITableViewDataSource {
  func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
    return billItemsListArray.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell =
      tableView.dequeueReusableCell(withIdentifier: billItemCellId, for: indexPath) as! BillItemCell

    // let product = productListArray[indexPath.row]
    cell.backgroundColor = .white
    cell.billItem = billItemsListArray[indexPath.row]

    // cell.ProductImage.image = UIImage(named: product.itemImage!)
    // cell.ProductName.text = product.itemName!

    return cell
  }

  func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
    return cellHeight!
  }
}

// MARK: NavigationDelegate

extension ReceiptVC: NavigationDelegate {
  func isButtonPressed(_ isBack: Bool?) {
    if isBack! {
      AppUser.sharedInstance.removeData(forKey: .orderID)
      // userDefaults.removeObject(forKey: "order_id")

      // let user = AppUser.sharedInstance.getCurrentUser()
      //
      // let refForDel = Database.database().reference().child("ActiveCustomers/\(user!.id!)")
      //
      // refForDel.removeValue { error, _ in
      //
      //     print(error)
      // }

      if fromOrderHistory! {
        _ = navigationController?.popViewController(animated: true)
        // self.view.window!.layer.add(Components.shared.pageTransition(type: .push, subType: .fromLeft), forKey: nil)
        // self.dismiss(animated: false, completion: nil)
      } else {
        presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
      }
    } else {
      loggerDebug.warning("Help needed!!")
    }
  }
}
