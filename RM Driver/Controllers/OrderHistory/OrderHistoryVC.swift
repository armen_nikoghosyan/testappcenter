//
//  OrderHistoryVC.swift
//  Robomart
//
//  Created by Fahad Naqvi on 7/29/19.
//  Copyright © 2019 Fahad Naqvi. All rights reserved.
//

import SwiftyJSON
import UIKit

class OrderHistoryVC: UIViewController {
  private let orderListCellId = "orderListCellId"
  private let shimmerCellId = "shimmerCellId"

  let backButton: UIButton = {
    let button = UIButton(type: .custom)

    button.setTitle("", for: .normal)
    button.tintColor = UIColor.white
    button.backgroundColor = UIColor.clear
    button.setImage(UIImage(named: "back-icon"), for: .normal)
    button.imageView?.contentMode = .scaleAspectFit
    button.contentHorizontalAlignment = .left
    button.imageEdgeInsets = UIEdgeInsets(top: 10, left: -10, bottom: 10, right: 0)

    return button
  }()

  lazy var orderListTV = Components.shared.customUITableView()

  var isLoading: Bool? = true
  var shimmerCount: Int? = 10

  var orderList = [OrderHistoryRecord]()
  var selectedOrder: OrderHistoryRecord!

  let customNavigationbar = CustomNavigation()
  //    var progressHUD: ProgressHUD?

  var page: Int? = 1

  var vehicleName = ""
  override func viewDidLoad() {
    super.viewDidLoad()

    navigationItem.hidesBackButton = true
    navigationItem.title = "Order History"

    //        customBackButton()
    customNavigationBarSetup()
    setupTableView()

    getOrderHistory(page: page!)
    //        self.OrderListTV.reloadData()

    // Create and add the view to the screen.
    //        progressHUD = ProgressHUD(text: "", view: self.view)
    //        self.view.addSubview(progressHUD!)
    //        progressHUD!.show()
    guard let vehicleID: String = AppUser.sharedInstance.getData(forKey: .vehicleID) else {
      loggerDebug.warning("Vehicle ID does not exist.")
      return
    }

    APIManager.sharedInstance.get(endpoint: .getAssignedRobomrtWithName(id: vehicleID)) {
      res, error in

      if let err = error {
        loggerDebug.error("\(err.localizedDescription)")
      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace("AssignedRobomrtWithName: \(json)")

          let success = json["success"].boolValue

          if !success {
            // If vehicle is not assigned then user will be signout
            signOutUser(fromLogin: false)
          } else {
            // Convert json data into model
            let response = try JSONDecoder().decode(VehicleWithName.self, from: res!)
            loggerDebug.trace("AssignedRobomrtWithName: \(response.data!)")

            AppUser.sharedInstance.saveData(response.data, forKey: .vehicleName)
            self.vehicleName = response.data!.name ?? ""
            //            self.filterOrderList()

          }

        } catch let jsonError {
          loggerDebug.error("\(jsonError.localizedDescription)")
        }
      }
    }
  }

  func setupTableView() {
    orderListTV.register(OrderCell.self, forCellReuseIdentifier: orderListCellId)
    orderListTV.register(ShimmerOrderHistoryCell.self, forCellReuseIdentifier: shimmerCellId)

    orderListTV.delegate = self
    orderListTV.dataSource = self

    orderListTV.separatorStyle = .none
    orderListTV.keyboardDismissMode = .interactive
    orderListTV.allowsMultipleSelection = false  // isMultiSelection!

    view.addSubview(orderListTV)
    orderListTV.backgroundColor = .init(white: 0.95, alpha: 1)
    orderListTV.topAnchor.constraint(equalTo: customNavigationbar.bottomAnchor, constant: 0)
      .isActive = true
    orderListTV.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
    orderListTV.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    orderListTV.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
  }

  func customBackButton() {
    backButton.translatesAutoresizingMaskIntoConstraints = true
    backButton.tintColor = .init(white: 0, alpha: 1)
    backButton.frame = CGRect(x: 0, y: 0, width: 35, height: 35)
    backButton.contentHorizontalAlignment = .left
    backButton.addTarget(self, action: #selector(back(sender:)), for: UIControl.Event.touchUpInside)

    let backButtonItem = UIBarButtonItem(customView: backButton)
    navigationItem.leftBarButtonItem = backButtonItem
  }

  // MARK: CUSTOM NAVIGATION BAR SETUP

  func customNavigationBarSetup() {
    view.addSubview(customNavigationbar)
    customNavigationbar.delegate = self
    customNavigationbar.navigationTitleLabel.text = "Orders"
    customNavigationbar.rightBarButton.setImage(UIImage(named: ""), for: .normal)
    customNavigationbar.rightBarButton.isEnabled = false
    customNavigationbar.backButton.setImage(UIImage(named: "back-thin-icon"), for: .normal)
    customNavigationbar.backButton.isEnabled = true
    customNavigationbar.backgroundColor = .init(white: 0.95, alpha: 1)
    customNavigationbar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    customNavigationbar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    customNavigationbar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
      .isActive = true
    customNavigationbar.heightAnchor.constraint(equalToConstant: 70).isActive = true
  }

  @objc func back(sender _: UIButton) {
    _ = navigationController?.popViewController(animated: true)
    //        self.dismiss(animated: true, completion: nil)
  }

  // Marker : Get Order History
  func getOrderHistory(page: Int?) {
    loggerDebug.trace("Page count : \(page!)")
    //        let params = []

    //        let orderID = userDefaults.string(forKey: "order_id")

    //        print("this is order id: \(getOrderURL)?order_id=\(String(describing: orderID!))")
    //        let orderURL = "\(getOrderHistoryURL)"

    APIManager.sharedInstance.get(endpoint: .getAllForDriver(page: page!)) { res, error in

      if let err = error {
        AlertService.shared.alert(in: self, message: "Failed to get all Orders- \(err)")
        loggerDebug.error("Failed to get all Orders- \(err)")
      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace("\(json)")

          let response = try JSONDecoder().decode(OrderHistory.self, from: res!)

          if !response.success! {
            loggerDebug.error("Failed to get categories!")
            AlertService.shared.alert(
              in: self, message: "Failed - \(String(describing: response.error))")
          } else {
            loggerDebug.debug("Your got product list!")

            let orderData = response.data!

            if orderData.records!.count > 0 {
              self.isLoading = false

              self.orderList += orderData.records!
              self.orderList.removeAll { $0.vehicleName != self.vehicleName }
              // self.progressHUD!.hide()
              self.orderListTV.reloadData()
            }
          }

        } catch let jsonError {
          AlertService.shared.alert(
            in: self, message: "Failed to get all Orders - \(jsonError.localizedDescription)")
          loggerDebug.error("Failed to get all Orders - \(jsonError.localizedDescription)")
        }
      }
    }
  }
}

extension OrderHistoryVC: UITableViewDelegate, UITableViewDataSource {
  func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
    if orderList.count > 0 {
      return orderList.count
    }
    return shimmerCount!
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if orderList.count > 0 {
      let cell =
        tableView.dequeueReusableCell(withIdentifier: orderListCellId, for: indexPath) as! OrderCell

      cell.order = orderList[indexPath.row]

      return cell
    }

    let cell =
      tableView.dequeueReusableCell(withIdentifier: shimmerCellId, for: indexPath)
      as! ShimmerOrderHistoryCell

    return cell
  }

  func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
    return 80
  }

  func tableView(_: UITableView, didSelectRowAt indexPath: IndexPath) {
    if orderList.count > 0 {
      selectedOrder = orderList[indexPath.row]
      performSegue(withIdentifier: "receipt", sender: self)
    }
  }

  func tableView(_: UITableView, didDeselectRowAt _: IndexPath) {}

  func tableView(_: UITableView, willDisplay _: UITableViewCell, forRowAt indexPath: IndexPath) {
    if !isLoading! {
      if indexPath.row + 1 == orderList.count {
        DispatchQueue.global(qos: .userInteractive).async {
          self.page! += 1
          self.getOrderHistory(page: self.page!)
        }
      }
    }
  }

  override func prepare(for segue: UIStoryboardSegue, sender _: Any?) {
    if let dest = segue.destination as? ReceiptVC {
      dest.orderID = selectedOrder.orderID!
      dest.fromOrderHistory = true
    }
  }
}

extension OrderHistoryVC: NavigationDelegate {
  func isButtonPressed(_ isBack: Bool?) {
    if isBack! {
      _ = navigationController?.popViewController(animated: true)
    } else {
      dismiss(animated: true, completion: nil)
    }
  }
}
