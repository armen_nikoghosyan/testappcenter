//
//  LoginVC.swift
//  RobomartDriver
//
//  Created by Fahad Naqvi on 10/24/20.
//

import Alamofire
import Firebase
import FirebaseAuth
import Intercom
import SwiftyJSON
import UIKit

class LoginVC: UIViewController {
  let emailInputBackground = Components.shared.customView()
  let emailInput = Components.shared.customUITextField(
    placeholder: "example@domain.com", fontName: .helveticaNeue, fontSize: 18,
    textAlignment: .center, borderSyle: .none, secureText: false)

  let passwordInputBackground = Components.shared.customView()
  let passwordInput = Components.shared.customUITextField(
    placeholder: "Password", fontName: .helveticaNeue, fontSize: 18, textAlignment: .center,
    borderSyle: .none, secureText: true)

  let loginButton = Components.shared.customButton(
    title: "Login", fontName: .avenirNextRegular, fontSize: 18, imageName: "")
  let loginButtonActivityIndictor = UIActivityIndicatorView(
    style: UIActivityIndicatorView.Style.gray)

  var emailInputBackgroundYConstraint: NSLayoutConstraint!
  var bottomConstraint: NSLayoutConstraint!

  override func viewDidLoad() {
    super.viewDidLoad()

    // Setup UI initiate: Creating UI by code
    setupUI()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    // Hide navigation bar
    navigationController?.setNavigationBarHidden(true, animated: true)

    // Add keyboard observer
    NotificationCenter.default.addObserver(
      self, selector: #selector(handleKeyboardNotification),
      name: UIResponder.keyboardWillShowNotification, object: nil)
    NotificationCenter.default.addObserver(
      self, selector: #selector(handleKeyboardNotification),
      name: UIResponder.keyboardWillHideNotification, object: nil)
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    emailInput.becomeFirstResponder()
  }
}

extension LoginVC {
  // Setup UI function
  private func setupUI() {
    view.addSubview(emailInputBackground)
    emailInputBackground.layer.cornerRadius = 5
    emailInputBackground.backgroundColor = UIColor(white: 0.95, alpha: 1)
    emailInputBackgroundYConstraint = emailInputBackground.centerYAnchor.constraint(
      equalTo: view.centerYAnchor, constant: -50)
    emailInputBackgroundYConstraint.isActive = true
    emailInputBackground.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
      .isActive = true
    emailInputBackground.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
      .isActive = true
    emailInputBackground.heightAnchor.constraint(equalToConstant: 50).isActive = true

    emailInputBackground.addSubview(emailInput)
    emailInput.delegate = self
    emailInput.keyboardType = .emailAddress
    emailInput.autocapitalizationType = .none
    emailInput.topAnchor.constraint(equalTo: emailInputBackground.topAnchor, constant: 5).isActive =
      true
    emailInput.leadingAnchor.constraint(equalTo: emailInputBackground.leadingAnchor, constant: 15)
      .isActive = true
    emailInput.trailingAnchor.constraint(
      equalTo: emailInputBackground.trailingAnchor, constant: -15
    ).isActive = true
    emailInput.bottomAnchor.constraint(equalTo: emailInputBackground.bottomAnchor, constant: -5)
      .isActive = true

    view.addSubview(passwordInputBackground)
    passwordInputBackground.layer.cornerRadius = 5
    passwordInputBackground.backgroundColor = UIColor(white: 0.95, alpha: 1)
    passwordInputBackground.topAnchor.constraint(
      equalTo: emailInputBackground.bottomAnchor, constant: 20
    ).isActive = true
    passwordInputBackground.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20)
      .isActive = true
    passwordInputBackground.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20)
      .isActive = true
    passwordInputBackground.heightAnchor.constraint(equalToConstant: 50).isActive = true

    passwordInputBackground.addSubview(passwordInput)
    passwordInput.delegate = self
    passwordInput.isEnabled = false
    passwordInput.topAnchor.constraint(equalTo: passwordInputBackground.topAnchor, constant: 5)
      .isActive = true
    passwordInput.leadingAnchor.constraint(
      equalTo: passwordInputBackground.leadingAnchor, constant: 15
    ).isActive = true
    passwordInput.trailingAnchor.constraint(
      equalTo: passwordInputBackground.trailingAnchor, constant: -15
    ).isActive = true
    passwordInput.bottomAnchor.constraint(
      equalTo: passwordInputBackground.bottomAnchor, constant: -5
    ).isActive = true

    loginButton.addSubview(loginButtonActivityIndictor)
    loginButtonActivityIndictor.translatesAutoresizingMaskIntoConstraints = false
    loginButtonActivityIndictor.centerXAnchor.constraint(
      equalTo: loginButton.centerXAnchor, constant: 0
    ).isActive = true
    loginButtonActivityIndictor.centerYAnchor.constraint(
      equalTo: loginButton.centerYAnchor, constant: 0
    ).isActive = true
    loginButtonActivityIndictor.widthAnchor.constraint(equalToConstant: 40).isActive = true
    loginButtonActivityIndictor.heightAnchor.constraint(equalToConstant: 40).isActive = true

    view.addSubview(loginButton)
    loginButton.setTitleColor(.init(white: 0.8, alpha: 1), for: .normal)
    loginButton.layer.cornerRadius = 25.0
    loginButton.backgroundColor = .init(white: 1, alpha: 1)
    loginButton.isEnabled = false

    loginButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.20).cgColor
    loginButton.layer.shadowOffset = CGSize(width: 0.0, height: 1.5)
    loginButton.layer.shadowOpacity = 1.0
    loginButton.layer.shadowRadius = 5.0
    loginButton.layer.masksToBounds = false

    loginButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive = true
    loginButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true

    if #available(iOS 11.0, *) {
      bottomConstraint = loginButton.bottomAnchor.constraint(
        equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20)
      bottomConstraint.isActive = true
    } else {
      bottomConstraint = loginButton.bottomAnchor.constraint(
        equalTo: view.bottomAnchor, constant: -20)
      bottomConstraint.isActive = true
    }

    loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    loginButton.addTarget(self, action: #selector(loginAction(_:)), for: .touchUpInside)
  }

  // login button click action
  @objc private func loginAction(_ sender: UIButton) {
    let emailText = emailInput.text!
    let passText = passwordInput.text!

    loginButton.isEnabled = false
    loginButton.setTitleColor(.init(white: 0.8, alpha: 1), for: .normal)
    loginButton.setTitle("", for: .normal)
    loginButtonActivityIndictor.startAnimating()

    // check if any text field is empty
    if emailText == "" {
      AlertService.shared.alert(in: self, message: "Please enter your email")
      emailInput.becomeFirstResponder()
      return
    } else if passText == "" {
      AlertService.shared.alert(in: self, message: "Please enter your password")
      passwordInput.becomeFirstResponder()
      return
    }

    loggerDebug.notice("All good. User filled all fields.")

    Auth.auth().signIn(withEmail: emailText, password: passText) { authResult, error in
      // guard let strongSelf = self else { return }

      if let err = error {
        self.loginButton.isEnabled = true
        self.loginButton.setTitleColor(.init(white: 0.3, alpha: 1), for: .normal)
        self.loginButton.setTitle("Login", for: .normal)
        self.loginButtonActivityIndictor.stopAnimating()
        AlertService.shared.alert(in: self, message: "\(err.localizedDescription)")
        loggerDebug.error(
          "Error occurred while sign in \(String(describing: err.localizedDescription))")
        return
      }

      authResult?.user.getIDToken(completion: { token, error in

        if let err = error {
          AlertService.shared.alert(
            in: self, message: "\(err.localizedDescription)")
          self.emailInput.text = ""
          self.passwordInput.text = ""
          self.emailInput.becomeFirstResponder()
          self.passwordInput.isEnabled = false
          self.loginButton.isEnabled = true
          self.loginButton.setTitleColor(.init(white: 0.3, alpha: 1), for: .normal)
          self.loginButton.setTitle("Login", for: .normal)
          self.loginButtonActivityIndictor.stopAnimating()
          loggerDebug.error(
            "Error occurred in get ID token phase \(String(describing: err.localizedDescription))")

        } else {
          loggerDebug.notice("Sign in successfully \(token!)")
          loggerEvents.info("log_in")

          self.emailInput.isEnabled = false
          self.passwordInput.isEnabled = false

          // Saving token to keychain
          let token = KeyChain.stringToData(string: token!)
          _ = KeyChain.save(key: .fbtoken, data: token)

          signOutUser(fromLogin: true)

          self.getCustomToken()
        }

      })
    }
  }

  private func getCustomToken() {
    let fullUrl = baseURL + EndPoints.getCustomToken.path

    guard let getFBToken = KeyChain.load(key: .fbtoken) else {
      loggerDebug.error("fbtoken is nil")
      return
    }

    let fbToken = KeyChain.dataToString(data: getFBToken)

    loggerDebug.trace("\(fullUrl)")

    let header: HTTPHeaders? = ["token": "", "fbtoken": fbToken]

    Alamofire.request(
      fullUrl, method: .post, parameters: [:], encoding: JSONEncoding.default, headers: header
    ).responseJSON { response in
      // handle JSON
      switch response.result {
      case .success:

        do {
          let json = try JSON(data: response.data!)
          loggerDebug.trace("Get Custom token : \(json)")

          let resp = try JSONDecoder().decode(CustomToken.self, from: response.data!)

          if resp.success! {
            // Saving token to keychain
            // let token = KeyChain.StringToData(string: resp.data!)
            // _ = KeyChain.save(key: .fbtoken, data: token)

            Auth.auth().signIn(withCustomToken: resp.data!) { res, error in
              if let err = error {
                AlertService.shared.alert(
                  in: self, message: "\(err.localizedDescription)")
                loggerDebug.error("\(String(describing: err.localizedDescription))")
              } else {
                res?.user.getIDToken(completion: { token, error in
                  if let err = error {
                    AlertService.shared.alert(
                      in: self, message: "\(err.localizedDescription)")
                    loggerDebug.error("\(String(describing: err.localizedDescription))")
                  } else {
                    self.emailInput.isEnabled = true
                    self.passwordInput.isEnabled = true

                    self.loginButton.setTitle("Login", for: .normal)
                    self.loginButtonActivityIndictor.stopAnimating()

                    // Saving token to keychain
                    let token = KeyChain.stringToData(string: resp.data!)
                    _ = KeyChain.save(key: .fbtoken, data: token)

                    AppUser.sharedInstance.saveData(true, forKey: .loggedIn)
                    self.setupUserInfoIntoCrashlytics()

                    // Get FCM token and send it to the server for push notification
                    getFCMToken { (fcmToken: String?) in

                      // AlertService.shared.alert(in: self, message: "\(fcmToken)")

                      if let token = fcmToken {
                        loggerDebug.info("FCM Token: \(token)")

                        let params = ["device_token": "\(token)"]

                        APIManager.sharedInstance.post(
                          endpoint: .updatePushNotificationToken, parameters: params
                        ) { res, error in

                          if let err = error {
                            loggerDebug.error("\(err.localizedDescription)")
                            AlertService.shared.alert(
                              in: self, message: "\(err.localizedDescription)")
                            loggerDebug.error("\(String(describing: error))")
                          } else {
                            do {
                              let json = try JSON(data: res!)
                              loggerDebug.trace("FCM response: \(json)")
                            } catch let jsonError {
                              loggerDebug.error("\(jsonError.localizedDescription)")
                              AlertService.shared.alert(
                                in: self,
                                message: "\(jsonError.localizedDescription)")
                            }

                            loggerDebug.info("FCM token update successful.")
                          }
                        }

                        let tokenData = Data(fcmToken!.utf8)

                        // Set device token for push notifications.
                        Intercom.setDeviceToken(tokenData)

                      } else {
                        loggerDebug.error("token does not exist...")
                      }
                    }

                    // get assigned robomart to the driver
                    DispatchQueue.main.async {
                      self.getAssignedRobomart { (success: Bool?) in
                        if success! {
                          self.performSegue(withIdentifier: "mainView", sender: self)
                        } else {
                          signOutUser(fromLogin: true)

                          self.passwordInput.text = ""
                          self.passwordInput.isEnabled = true
                          self.passwordInput.becomeFirstResponder()
                          self.loginButton.isEnabled = true
                          self.loginButton.setTitleColor(.init(white: 0.3, alpha: 1), for: .normal)
                        }
                      }
                    }
                  }
                })
              }
            }
          } else {
            AlertService.shared.alert(in: self, message: "\(resp.error!)")
            loggerDebug.error("\(resp.error!)")
          }

        } catch let jsonError {
          loggerDebug.error("\(jsonError.localizedDescription)")
          AlertService.shared.alert(
            in: self, message: "\(jsonError.localizedDescription)")
        }

      case .failure(let error):
        loggerDebug.error("\(error.localizedDescription)")
        self.loginButton.isEnabled = true
        self.loginButton.setTitleColor(.init(white: 0.3, alpha: 1), for: .normal)
        AlertService.shared.alert(
          in: self, message: "\(error.localizedDescription)")
      }
    }
  }

  // get assigned robomart
  private func getAssignedRobomart<T: Decodable>(completion: @escaping (T?) -> Void) {
    APIManager.sharedInstance.get(endpoint: .getAssignedRobomrt) { res, error in

      if let err = error {
        loggerDebug.error("\(err.localizedDescription)")

        completion(false as? T)
      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace("\(json)")

          // converting json data into model
          let response = try JSONDecoder().decode(AssignedRobomart.self, from: res!)

          if response.success! {
            let vID = response.data!

            loggerDebug.info("\(vID)")

            AppUser.sharedInstance.saveData(vID, forKey: .vehicleID)

            completion(true as? T)

          } else {
            // AlertService.shared.alert(in: self, message: "\(response.error!)")
            loggerDebug.error("\(response.error!)")
            let responsePrompt = UIAlertController(
              title: "Robomart", message: "\(response.error!)",
              preferredStyle: UIAlertController.Style.alert)

            responsePrompt.addAction(
              UIAlertAction(
                title: "Ok", style: .default,
                handler: { (_: UIAlertAction!) in
                  completion(false as? T)
                }))

            self.present(responsePrompt, animated: true, completion: nil)
          }

        } catch let jsonError {
          loggerDebug.error("\(jsonError.localizedDescription)")
          completion(false as? T)
        }
      }
    }
  }

  // keyboard handler to show or hide
  @objc func handleKeyboardNotification(notification: NSNotification) {
    if let userInfo = notification.userInfo {
      let keyboardSize = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        .size

      let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification

      UIView.animate(withDuration: 0.5) {
        self.emailInputBackgroundYConstraint.constant =
          isKeyboardShowing ? -keyboardSize.height / 2 - 25 : -50
        self.bottomConstraint.constant = isKeyboardShowing ? -keyboardSize.height : -20
      }

      view.layoutIfNeeded()
    }
  }

  // check if email format is valid
  func isValidEmail(emailStr: NSString?) -> Bool {
    // print("validate calendar: \(testStr)")
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

    let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: emailStr)
  }
}

// MARK: UITextFieldDelegate

extension LoginVC: UITextFieldDelegate {
  func textFieldDidBeginEditing(_ textField: UITextField) {
    textField.placeholder = ""
  }

  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField == emailInput {
      textField.placeholder = "Enter your email"
    } else {
      textField.placeholder = "Enter your password"
    }
  }

  func textField(
    _ textField: UITextField, shouldChangeCharactersIn range: NSRange,
    replacementString string: String
  ) -> Bool {
    let stringVerify: NSString? =
      ((textField.text as NSString?)?.replacingCharacters(in: range, with: string) as NSString?)!
    let findRange: NSRange? = stringVerify!.range(of: "^\\s*", options: .regularExpression)
    let result: NSString? =
      ((stringVerify! as NSString?)?.replacingCharacters(in: findRange!, with: "") as NSString?)!

    if textField == emailInput {
      if string == " " {
        return false
      }

      if isValidEmail(emailStr: result!) {
        loggerDebug.debug("Email Verified and Next Button Enabled!")
        passwordInput.isEnabled = true

      } else {
        loggerDebug.debug("Next Button Disabled!")
        passwordInput.isEnabled = false
        loginButton.isEnabled = false
        loginButton.setTitleColor(.init(white: 0.8, alpha: 1), for: .normal)
      }

    } else {
      if emailInput.text! != "" || passwordInput.text! != "" {
        if result!.length >= 3 {
          loginButton.isEnabled = true
          loginButton.setTitleColor(.init(white: 0.3, alpha: 1), for: .normal)
        }
      }
    }

    loggerDebug.trace("UITextField delegate!!  \(result!)")

    return true
  }

  func setupUserInfoIntoCrashlytics() {
    if let user = Auth.auth().currentUser {
      let keysAndValues =
        [
          "clientId": UIDevice.current.identifierForVendor as Any,
          "userName": user.displayName as Any,
          "userId": user.uid as Any,
          "userEmail": user.email as Any,
          "userPhone": user.phoneNumber as Any,
        ] as [String: Any]

      Crashlytics.crashlytics().setCustomKeysAndValues(keysAndValues)
    }
  }
}
