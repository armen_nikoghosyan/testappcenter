//
//  SettingsMenuCell.swift
//  Robomart
//
//  Created by Fahad Naqvi on 3/4/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
  let menuTextLabel = Components.shared.customLabel(
    fontSize: 16, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Bananas")
  let menuIcon = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFit)
  let rightArrowIcon = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFit)
  let statusIcon = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFit)
  let bottomBorder = Components.shared.customView()

  let activityIndictorForStatus = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)

  var menuIconLeadingConstraint: NSLayoutConstraint?

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    backgroundColor = .white
    selectionStyle = .none

    addSubview(menuIcon)
    menuIcon.tintColor = .init(white: 0, alpha: 1)
    menuIconLeadingConstraint = menuIcon.leadingAnchor.constraint(
      equalTo: leadingAnchor, constant: 50)
    menuIconLeadingConstraint!.isActive = true
    menuIcon.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    menuIcon.widthAnchor.constraint(equalToConstant: 20).isActive = true
    menuIcon.heightAnchor.constraint(equalToConstant: 20).isActive = true

    addSubview(menuTextLabel)
    menuTextLabel.textColor = .init(white: 0.5, alpha: 1)
    menuTextLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    menuTextLabel.leadingAnchor.constraint(equalTo: menuIcon.trailingAnchor, constant: 25).isActive =
      true

    addSubview(statusIcon)
    statusIcon.isHidden = true
    statusIcon.image = UIImage(named: "warning-icon")
    statusIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -45).isActive = true
    statusIcon.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    statusIcon.widthAnchor.constraint(equalToConstant: 15).isActive = true
    statusIcon.heightAnchor.constraint(equalToConstant: 15).isActive = true

    addSubview(activityIndictorForStatus)
    activityIndictorForStatus.isHidden = true
    activityIndictorForStatus.translatesAutoresizingMaskIntoConstraints = false
    activityIndictorForStatus.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -45)
      .isActive = true
    activityIndictorForStatus.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive =
      true
    activityIndictorForStatus.widthAnchor.constraint(equalToConstant: 15).isActive = true
    activityIndictorForStatus.heightAnchor.constraint(equalToConstant: 15).isActive = true

    addSubview(rightArrowIcon)
    rightArrowIcon.isHidden = true
    rightArrowIcon.image = UIImage(named: "right-arrow")
    rightArrowIcon.tintColor = .init(white: 0.5, alpha: 1)
    rightArrowIcon.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25).isActive = true
    rightArrowIcon.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0).isActive = true
    rightArrowIcon.widthAnchor.constraint(equalToConstant: 15).isActive = true
    rightArrowIcon.heightAnchor.constraint(equalToConstant: 15).isActive = true

    addSubview(bottomBorder)
    bottomBorder.backgroundColor = .init(white: 0.95, alpha: 1)
    bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    bottomBorder.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 25).isActive = true
    bottomBorder.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -25).isActive = true
    bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true
  }

  @available(*, unavailable)
  required init?(coder _: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  //    override func setSelected(_ selected: Bool, animated: Bool) {
  //        super.setSelected(selected, animated: animated)
  //
  //        // Configure the view for the selected state
  //    }
}
