//
//  MenuParentVC.swift
//  Robomart
//
//  Created by Fahad Naqvi on 5/7/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import UIKit

class MenuParentVC: UIViewController {

  //    var cardUpdateDelegate: CardUpdateDelegate?
  //    var settingsDelegate: SettingsDelegate?
  var pageTitle: String?

  override func viewDidLoad() {
    super.viewDidLoad()

    setup()
  }

  override func viewWillDisappear(_ animated: Bool) {
    loggerDebug.debug("MenuParentVC: viewWillDisappear")
    //        settingsDelegate?.didReturn(isReturn: true)
  }

  private func setup() {
    let settingsVC = MenuVC()
    settingsVC.pageTItle = pageTitle!
    //        settingsVC.cardUpdateDelegate = self
    addChild(settingsVC)
  }
}

//extension MenuParentVC: CardUpdateDelegate {
//    func didUpdateCard(cardLast4: String?) {
//        logger.info("SettingsParentVC: didUpdateCard")
//        cardUpdateDelegate?.didUpdateCard?(cardLast4: cardLast4!)
//    }
//}
