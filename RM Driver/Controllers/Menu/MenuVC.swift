//
//  MenuVC.swift
//  Robomart
//
//  Created by Fahad Naqvi on 3/4/20.
//  Copyright © 2020 Fahad Naqvi. All rights reserved.
//

import FirebaseAuth
import Intercom
import UIKit

// protocol SettingsDelegate {
//     func didReturn(isReturn: Bool?)
// }

protocol menuDelegate: AnyObject {
  func menuDelegate(identifier: String?)
}

class MenuVC: UIViewController {
  let menuListTV = Components.shared.customUITableView()
  private let menuCellId = "menuCellId"
  let customNavigationbar = CustomNavigation()
  let versionLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "Version 0.1.0")
  let copyrightLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextRegular, fontAlignment: .left, numberOfLines: 0,
    text: "© 2020. Robomart, Inc.")

  weak var delegate: menuDelegate?

  var pageTItle: String?  //= "Menu"
  // var cardUpdateDelegate: CardUpdateDelegate?

  // var cardListArray: [CardSourcesData]? = []
  // var cardData: CardDataModel?

  let menuData = [
    // SideMenu(name: "Personal Info", icon: "user-info-icon", identifier: "ProfileVC"),
    // SideMenu(name: "Payment", icon: "card-icon", identifier: "CardsListVC"),
    SideMenu(name: "Orders", icon: "order-icon", identifier: "OrderHistoryVC"),
    // SideMenu(name: "Notifications", icon: "notification-icon", identifier: "NotificationsSettingsVC"),
    // SideMenu(name: "Invites", icon: "credit-icon", identifier: "InvitesVC"),
    //     SideMenu(name:"Credits", icon: "credit-icon", identifier: "CreditsVC"),
    // SideMenu(name: "Helpline", icon: "help-icon", identifier: "help"),
    SideMenu(name: "Log Out", icon: "logout-icon", identifier: "logout"),
  ]

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white

    Intercom.unreadConversationCount()

    // NotificationCenter.default.addObserver(self,
    // selector: #selector(self.updateUnreadCount(_:)),
    //     name: NSNotification.Name.IntercomUnreadConversationCountDidChange,
    //   object: nil)
    navigationController?.setNavigationBarHidden(true, animated: false)
    // navigationItem.leftBarButtonItem = barButton(imageName: "back-thin-icon", selector: #selector(dismissView(_:)))

    customNavigationBarSetup()
    setupUI()
    setupTV()
    // getAllCards()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    loggerDebug.debug("Settings will disappear")

  }
}

extension MenuVC {
  @objc private func dismissView(_: Any) {}

  func customNavigationBarSetup() {
    view.addSubview(customNavigationbar)
    customNavigationbar.delegate = self

    customNavigationbar.rightBarButton.setImage(
      #imageLiteral(resourceName: "close-icon"), for: .normal)
    customNavigationbar.backButton.setImage(
      #imageLiteral(resourceName: "back-thin-icon"), for: .normal)
    customNavigationbar.backButton.isEnabled = false
    customNavigationbar.backButton.isHidden = true
    customNavigationbar.backgroundColor = .init(white: 0.95, alpha: 1)
    customNavigationbar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    customNavigationbar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    customNavigationbar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
      .isActive = true
    customNavigationbar.heightAnchor.constraint(equalToConstant: 70).isActive = true

    guard
      let vehicleName: VehicleWithNameData = AppUser.sharedInstance.getData(forKey: .vehicleName)
    else {
      loggerDebug.error("Vehicle name is nil")
      customNavigationbar.navigationTitleLabel.text = "Menu"
      return
    }
    customNavigationbar.navigationTitleLabel.text = "\(vehicleName.name ?? "Menu")"  //"Menu"
  }

  func setupTV() {
    menuListTV.register(MenuCell.self, forCellReuseIdentifier: menuCellId)

    menuListTV.delegate = self
    menuListTV.dataSource = self

    menuListTV.separatorStyle = .none
    menuListTV.keyboardDismissMode = .interactive

    view.addSubview(menuListTV)
    menuListTV.backgroundColor = .white
    menuListTV.topAnchor.constraint(equalTo: customNavigationbar.bottomAnchor, constant: 0).isActive =
      true
    menuListTV.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
    menuListTV.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    menuListTV.bottomAnchor.constraint(equalTo: versionLabel.topAnchor, constant: -10).isActive =
      true

    menuListTV.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
  }

  func setupUI() {
    view.addSubview(copyrightLabel)
    copyrightLabel.textColor = .init(white: 0.5, alpha: 1)

    if hasSafeArea {
      copyrightLabel.bottomAnchor.constraint(
        equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -20
      ).isActive = true
    } else {
      copyrightLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -20).isActive =
        true
    }

    copyrightLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive =
      true
    copyrightLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive =
      true

    // Get year to show in copyrights text
    let getCurrentYear = Calendar.current.dateComponents([.year], from: Date()).year!
    self.copyrightLabel.text = "© \(getCurrentYear). Robomart, Inc."

    view.addSubview(versionLabel)
    versionLabel.textColor = .init(white: 0.8, alpha: 1)
    versionLabel.bottomAnchor.constraint(equalTo: copyrightLabel.topAnchor, constant: 0).isActive =
      true
    versionLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50).isActive = true
    versionLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50).isActive =
      true

    #if DEBUG
      versionLabel.text =
        "Version \(Bundle.main.versionNumber!) (\(Bundle.main.buildVersionNumber!))"
    #else
      versionLabel.text = "Version \(versionNumber)"
    #endif
  }
}

extension MenuVC: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in _: UITableView) -> Int {
    return 1
  }

  func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
    return menuData.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell =
      tableView.dequeueReusableCell(withIdentifier: menuCellId, for: indexPath) as! MenuCell

    //        cell.rightArrowIcon.isHidden = false
    cell.menuIcon.image = UIImage(named: "\(menuData[indexPath.row].icon!)")
    cell.menuTextLabel.text = menuData[indexPath.row].name!

    return cell
  }

  func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
    return 60
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    // self.dismiss(animated: true, completion: nil)
    // self.delegate?.menuDelegate(identifier: menuData[indexPath.row].identifier!)
    // self.performSegue(withIdentifier: menuData[indexPath.row].identifier!, sender: nil)

    let thisCell = tableView.cellForRow(at: indexPath)
    thisCell?.backgroundColor = .init(white: 0.95, alpha: 1)

    DispatchQueue.main.asyncAfter(deadline: .now()) {
      UIView.animate(
        withDuration: 0.5,
        animations: {
          tableView.cellForRow(at: indexPath)?.backgroundColor = .white
        })
    }

    if menuData[indexPath.row].identifier! == "ProfileVC" {
      performSegue(withIdentifier: "profile", sender: self)

    } else if menuData[indexPath.row].identifier! == "OrderHistoryVC" {
      performSegue(withIdentifier: "orderHistory", sender: self)

      //    } else if menuData[indexPath.row].identifier! == "logout" {
      //      // signOutDelegate = self
      //
      //      // Will show prompt to confirm logout
      //      let confirmLogoutPrompt = UIAlertController(
      //        title: "Robomart", message: "Are you sure you want to logout?", preferredStyle: .alert)
      //
      //      confirmLogoutPrompt.view.tintColor = .gray
      //
      //      confirmLogoutPrompt.addAction(
      //        UIAlertAction(
      //          title: "Yes", style: .default,
      //          handler: { (action: UIAlertAction!) in
      //            loggerEvents.info("log_out")
      //            signOutUser(fromLogin: false)
      //
      //          }))
      //
      //      confirmLogoutPrompt.addAction(
      //        UIAlertAction(
      //          title: "No", style: .cancel,
      //          handler: { (action: UIAlertAction!) in
      //            loggerDebug.info("Handle Cancel Logic here")
      //
      //          }))
      //
      //      present(confirmLogoutPrompt, animated: true, completion: nil)
      //
    } else if menuData[indexPath.row].identifier! == "logout" {
      // signOutDelegate = self

      // Will show prompt to confirm logout
      let confirmLogoutPrompt = UIAlertController(
        title: "Robomart", message: "Are you sure you want to logout?", preferredStyle: .alert)

      confirmLogoutPrompt.view.tintColor = .gray

      confirmLogoutPrompt.addAction(
        UIAlertAction(
          title: "Yes", style: .default,
          handler: { (action: UIAlertAction!) in
            loggerEvents.info("log_out")
            signOutUser(fromLogin: false)
          }))

      confirmLogoutPrompt.addAction(
        UIAlertAction(
          title: "No", style: .cancel,
          handler: { (action: UIAlertAction!) in
            loggerDebug.info("Handle Cancel Logic here")

          }))

      present(confirmLogoutPrompt, animated: true, completion: nil)

    } else {
      // TODO: Add code or remove else section
    }
  }

  func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    let thisCell = tableView.cellForRow(at: indexPath)
    thisCell?.backgroundColor = .white
    tableView.deselectRow(at: indexPath, animated: true)
  }

  func scrollViewWillBeginDecelerating(_: UIScrollView) {
    loggerDebug.debug("Scrollview start")
  }
}

extension MenuVC: NavigationDelegate {
  func isButtonPressed(_ isBack: Bool?) {
    _ = !isBack! ? dismiss(animated: true, completion: nil) : nil
  }
}

//extension MenuVC: SettingsDelegate {
//    func didReturn(isReturn _: Bool?) {
////        self.getAllCards()
//    }
//}

//extension MenuVC: SignOutUserDelegate {
//    func didUserSignedOut(isSignedOut: Bool?) {
//        print("User signed out successful!")
//
//        if isSignedOut! {
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let loginViewController: ViewController = storyboard.instantiateViewController(withIdentifier: "landingView") as! ViewController
//
//            var navigationController = UINavigationController()
//            navigationController = UINavigationController(rootViewController: loginViewController)
//
//            // It removes all view controllers from the navigation controller then sets the new root view controller and it pops.
//            if let window = UIApplication.shared.keyWindow {
//                window.rootViewController = navigationController
//            }
//
//            // Navigation bar is hidden
//            navigationController.isNavigationBarHidden = true
//        }
//    }
//}
