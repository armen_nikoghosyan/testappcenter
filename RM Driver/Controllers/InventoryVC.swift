//
//  InventoryVC.swift
//  RM Driver
//
//  Created by Fahad Naqvi on 12/2/20.
//

import Alamofire
import Firebase
import SwiftyJSON
import UIKit

class InventoryVC: UIViewController {
  private let itemsCellId = "itemsCellId"

  let customNavigationbar = CustomNavigation()

  let inventoryListTV = Components.shared.customUITableView()

  let subHeaderView = Components.shared.customView()

  let updatedOnHeaderLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextDemiBold, fontAlignment: .left, numberOfLines: 0,
    text: "Ping")
  let updatedOnLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextMedium, fontAlignment: .left, numberOfLines: 0, text: "")

  let toggleButtonHeaderLabel = Components.shared.customLabel(
    fontSize: 14, fontName: .avenirNextDemiBold, fontAlignment: .right, numberOfLines: 0,
    text: "Difference")
  let toggleButton: UISwitch = {
    let switchBtn = UISwitch()
    switchBtn.translatesAutoresizingMaskIntoConstraints = false
    switchBtn.isOn = false
    switchBtn.setOn(false, animated: true)
    switchBtn.onTintColor = .black
    return switchBtn
  }()

  var allInventoryList = [InventoryProduct]()
  var currentInventoryList = [InventoryProduct]()
  var inventoryList = [InventoryProduct]()

  // initiate firebase with active vehicle
  let vehicleRef = Database.database().reference().child("Vehicles")

  var callAPITimer: Timer?
  var callAPITimerOnError: Timer?

  var isSorted: Bool? = false
  var apiHitCounter: Int? = 0

  override func viewDidLoad() {
    super.viewDidLoad()

    // initiate custome navigation bar
    customNavigationBarSetup()

    // initiate UI
    setupUI()

    // initiate tableview UI
    setupTV()

    callAPITimer = Timer.scheduledTimer(
      timeInterval: 1.0,
      target: self,
      selector: #selector(timerAction),
      userInfo: nil,
      repeats: true)

    if let vehicleID: String = AppUser.sharedInstance.getData(forKey: .vehicleID) {
      vehicleRef.child(vehicleID).updateChildValues(["current_inventory_requested": true])
    }

    //        DispatchQueue.main.async {
    //            self.GetInventory()
    //        }
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    //        if let vehicleID: String = AppUser.sharedInstance.getData(forKey: .vehicleID) {
    //            vehicleRef.child(vehicleID).updateChildValues(["current_inventory_requested": false])
    //        }
    //
    //        callAPITimer?.invalidate()
    if let vehicleID: String = AppUser.sharedInstance.getData(forKey: .vehicleID) {
      vehicleRef.child(vehicleID).updateChildValues(["current_inventory_requested": false])
    }
    callAPITimer?.invalidate()
  }
}

extension InventoryVC {
  // setup UI
  func setupUI() {
    view.addSubview(subHeaderView)
    subHeaderView.topAnchor.constraint(equalTo: customNavigationbar.bottomAnchor, constant: 0)
      .isActive = true
    subHeaderView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
    subHeaderView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive =
      true
    //        subHeaderView.heightAnchor.constraint(equalToConstant: 60).isActive = true

    subHeaderView.addSubview(toggleButtonHeaderLabel)
    toggleButtonHeaderLabel.topAnchor.constraint(equalTo: subHeaderView.topAnchor, constant: 5)
      .isActive = true
    toggleButtonHeaderLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25)
      .isActive = true

    subHeaderView.addSubview(toggleButton)
    toggleButton.topAnchor.constraint(equalTo: toggleButtonHeaderLabel.bottomAnchor, constant: 5)
      .isActive = true
    toggleButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -25).isActive =
      true
    toggleButton.bottomAnchor.constraint(equalTo: subHeaderView.bottomAnchor, constant: -10)
      .isActive = true
    toggleButton.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)

    subHeaderView.addSubview(updatedOnHeaderLabel)
    updatedOnHeaderLabel.topAnchor.constraint(equalTo: subHeaderView.topAnchor, constant: 5)
      .isActive = true
    updatedOnHeaderLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25)
      .isActive = true

    subHeaderView.addSubview(updatedOnLabel)
    updatedOnLabel.centerYAnchor.constraint(equalTo: toggleButton.centerYAnchor, constant: 0)
      .isActive = true
    updatedOnLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 25).isActive =
      true
    updatedOnLabel.trailingAnchor.constraint(equalTo: toggleButton.leadingAnchor, constant: -20)
      .isActive = true
  }

  // Setup table view UI
  func setupTV() {
    view.addSubview(inventoryListTV)
    inventoryListTV.delegate = self
    inventoryListTV.dataSource = self

    //        inventoryListTV.isScrollEnabled = false

    inventoryListTV.backgroundColor = .init(white: 0.95, alpha: 1)
    inventoryListTV.separatorStyle = .none
    inventoryListTV.keyboardDismissMode = .interactive

    inventoryListTV.register(InventoryItemCell.self, forCellReuseIdentifier: itemsCellId)
    inventoryListTV.topAnchor.constraint(equalTo: subHeaderView.bottomAnchor, constant: 0).isActive =
      true
    inventoryListTV.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    inventoryListTV.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive =
      true
    inventoryListTV.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive = true
    //        inventoryListTV.heightAnchor.constraint(equalToConstant: 0).isActive = true
  }

  // Custom navigation bar setup
  func customNavigationBarSetup() {
    view.addSubview(customNavigationbar)
    customNavigationbar.delegate = self
    customNavigationbar.navigationTitleLabel.text = "Inventory"
    customNavigationbar.rightBarButton.setImage(
      #imageLiteral(resourceName: "close-icon"), for: .normal)
    customNavigationbar.backButton.setImage(
      #imageLiteral(resourceName: "back-thin-icon"), for: .normal)
    customNavigationbar.backButton.isEnabled = false
    customNavigationbar.backButton.isHidden = true
    customNavigationbar.backgroundColor = .init(white: 0.95, alpha: 1)
    customNavigationbar.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    customNavigationbar.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    customNavigationbar.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
      .isActive = true
    customNavigationbar.heightAnchor.constraint(equalToConstant: 70).isActive = true
  }

  // Get inventory
  // func GetInventory(endpoint: EndPoints, completion: @escaping (Bool?, [InventoryProduct]?) -> ())

  private func getInventory() {
    APIManager.sharedInstance.get(endpoint: .getInventoryCounts) { res, error in

      if let err = error {
        loggerDebug.error("\(err.localizedDescription)")

      } else {
        self.apiHitCounter = 0
        self.callAPITimerOnError?.invalidate()

        do {
          _ = try JSON(data: res!)
          //                    logger.info("\(json)")

          // converting json data into model
          let response = try JSONDecoder().decode(Inventory.self, from: res!)

          if response.success! {
            self.updatedOnLabel.text = "\(response.data!.updatedOn!.updatedOn!) second(s)"

            self.allInventoryList = response.data!.products!

            self.inventoryList.removeAll()

            if self.isSorted! {
              for item in self.allInventoryList {
                if item.unionQty! != item.baselineQty! {
                  self.inventoryList.append(item)
                }
              }
            } else {
              self.inventoryList = self.allInventoryList
            }

            self.inventoryListTV.reloadData()

          } else {
            loggerDebug.error("\(response.error!)")
          }

        } catch let jsonError {
          loggerDebug.error("\(jsonError.localizedDescription)")
        }
      }
    }
  }

  // Switch button action
  @objc func switchValueDidChange(_ sender: UISwitch) {
    //        logger.info("Get Inventory : \(sender.isOn)")
    if sender.isOn {
      loggerDebug.debug("Get Inventory is on : \(sender.isOn)")

      isSorted = true

      inventoryList.removeAll()

      for item in allInventoryList {
        if item.unionQty! != item.baselineQty! {
          inventoryList.append(item)
        }
      }

      inventoryListTV.reloadData()

    } else {
      loggerDebug.debug("Get Inventory is on : \(sender.isOn)")

      isSorted = false

      inventoryList.removeAll()
      inventoryList = allInventoryList
      inventoryListTV.reloadData()
    }
  }

  @objc func timerAction(_: Timer) {
    DispatchQueue.main.async {
      self.getInventory()
    }
  }
}

extension InventoryVC: UITableViewDelegate, UITableViewDataSource {
  func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
    if inventoryList.count > 0 {
      return inventoryList.count
    }

    return 0
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell =
      tableView.dequeueReusableCell(withIdentifier: itemsCellId, for: indexPath)
      as! InventoryItemCell

    if inventoryList.count > 0 {
      cell.inventoryProduct = inventoryList[indexPath.row]

      if inventoryList[indexPath.row].unionQty! != inventoryList[indexPath.row].baselineQty! {
        cell.quantityHeaderLabel.textColor = .red
        cell.currQuantityHeaderLabel.textColor = .red
        cell.quantityLabel.textColor = .red
        cell.currQuantityLabel.textColor = .red
        cell.unionQuantityLabel.textColor = .red
        cell.unionQuantityHeaderLabel.textColor = .red
      } else if inventoryList[indexPath.row].unionQty! == inventoryList[indexPath.row].baselineQty!,
        inventoryList[indexPath.row].currentQty! != inventoryList[indexPath.row].baselineQty!
      {
        cell.quantityHeaderLabel.textColor = .blue
        cell.currQuantityHeaderLabel.textColor = .blue
        cell.quantityLabel.textColor = .blue
        cell.currQuantityLabel.textColor = .blue
        cell.unionQuantityLabel.textColor = .blue
        cell.unionQuantityHeaderLabel.textColor = .blue

      } else {
        cell.quantityHeaderLabel.textColor = .init(white: 0.4, alpha: 1)
        cell.currQuantityHeaderLabel.textColor = .init(white: 0.4, alpha: 1)
        cell.quantityLabel.textColor = .init(white: 0.4, alpha: 1)
        cell.currQuantityLabel.textColor = .init(white: 0.4, alpha: 1)
        cell.unionQuantityLabel.textColor = .init(white: 0.4, alpha: 1)
        cell.unionQuantityHeaderLabel.textColor = .init(white: 0.4, alpha: 1)
      }
    }

    return cell
  }
}

extension InventoryVC: NavigationDelegate {
  func isButtonPressed(_ isBack: Bool?) {
    _ = !isBack! ? dismiss(animated: true, completion: nil) : nil
  }
}
