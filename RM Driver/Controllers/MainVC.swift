//
//  MainVC.swift
//  RobomartDriver
//
//  Created by Fahad Naqvi on 10/25/20.
//

import Alamofire
import Firebase
import GEOSwift
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import UIKit

class MainVC: UIViewController {
  var mapView: GMSMapView?
  var mapPadding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
  var mapViewBottomConstraint: NSLayoutConstraint?
  var camera: GMSCameraPosition!
  var locationManager = CLLocationManager()
  var centerMapCoordinate: CLLocationCoordinate2D!

  let lat = 37.779593871626
  let long = -122.283288314939
  let zoom: Float = 16

  var centerPinMarker: GMSMarker?
  var robomartMarker: GMSMarker?
  var usertMarker: GMSMarker?
  var polyline: GMSPolyline?
  var path: GMSPath?

  // Zone variables
  let fillingPath = GMSMutablePath()
  // Polygon for the map
  var gmsPolygon: GMSPolygon?
  var gmsPolygonForBG: GMSPolygon?
  // Paths array
  var gmsPaths = [GMSPath]()

  let robomartMarkerImage = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFit)
  let userMarkerImage = Components.shared.customImageView(
    cornerRadius: 0, clipToBounds: true, contentMode: .scaleAspectFit)

  let resetLocationButton = Components.shared.customButton(
    title: "", fontName: .avenirNextMedium, fontSize: 14, imageName: "reset_loc")
  var resetLocationButtonBottomConstraint: NSLayoutConstraint?

  let statusView = Components.shared.customView()
  let statusLabel = Components.shared.customLabel(
    fontSize: 12, fontName: .avenirNextRegular, fontAlignment: .center, numberOfLines: 2, text: "")

  let processButton = Components.shared.customButton(
    title: "", fontName: .avenirNextRegular, fontSize: 16, imageName: nil)
  let buttonActivityIndictor = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.white)

  let cancelOrderButton = Components.shared.customButton(
    title: "Cancel Order", fontName: .avenirNextRegular, fontSize: 14, imageName: nil)
  var cancelOrderButtonConstraint: NSLayoutConstraint?

  let menuButton = Components.shared.customButton(
    title: "Menu", fontName: .avenirNextDemiBold, fontSize: 14, imageName: nil)
  let inventoryButton = Components.shared.customButton(
    title: "Inventory", fontName: .avenirNextDemiBold, fontSize: 14, imageName: nil)

  let mapsButton = Components.shared.customButton(
    title: "Maps", fontName: .avenirNextDemiBold, fontSize: 14, imageName: nil)

  // Initiate firebase with active vehicle
  let vehicleRef = Database.database().reference().child("Vehicles")

  var customReceiptView = CustomReceiptView()

  var unlockSlider = AURUnlockSlider()

  var cancelPromptInput = Components.shared.customUITextField(
    placeholder: "Text", fontName: .avenirNextRegular, fontSize: 14, textAlignment: .left,
    borderSyle: .none, secureText: false)

  // var textField: UITextField? //why we need to use this ?
  var loadingView: LoadingView!

  var orderID: String? = ""
  var userID: String? = ""

  var activeSlideButtonTimer: Timer?

  var customInfoWindow = CustomInfoWindow()
  var customInfoWindowXConstraint: NSLayoutConstraint?
  var customInfoWindowYConstraint: NSLayoutConstraint?
  var tappedMarker = GMSMarker()

  var vehicleCoordinates: CLLocationCoordinate2D?
  var userCoordinates: CLLocationCoordinate2D?

  var isOrderCanceled: Bool = false

  var receiptGenerationTimer: Timer?

  #if DEBUG
    var receiptCountdownTime = 5
  #else
    var receiptCountdownTime = 90
  #endif

  override func viewDidLoad() {
    super.viewDidLoad()
    // Setup UI initiate: Creating UI by code
    setupUI()

    // Check firebase
    getFirebaseUser()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    DispatchQueue.main.async {
      self.getAllZones()
    }

    // Show navigation bar
    navigationController?.setNavigationBarHidden(true, animated: true)

    DispatchQueue.main.async {
      self.getAssignedRobomartWithName()
    }
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)

    customReceiptView.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(customReceiptView)
    customReceiptView.isHidden = true
    customReceiptView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    customReceiptView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive =
      true
    customReceiptView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive =
      true
    customReceiptView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0).isActive =
      true
  }
}

// if let type = snapshot.childSnapshot(forPath: "type").value {
//
// logger.info("Checking type: \(type)")
//
// if type as? String == "2" {
// self.statusLabel.text = "Restock Request"
// }
// }

extension MainVC {
  private func setupUI() {
    // Inititate mapview
    createMapView()

    // Check permission for location access
    initializeTheLocationManager()
    view.addSubview(processButton)
    processButton.isHidden = false
    processButton.isEnabled = false
    processButton.layer.cornerRadius = 10
    processButton.setTitleColor(.white, for: .normal)
    processButton.tag = 1
    processButton.backgroundColor = .init(white: 0.8, alpha: 1)
    // ^ .black // Colors.shared.buttonBlue
    processButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive =
      true
    processButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive =
      true

    processButton.bottomAnchor.constraint(
      equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -35
    ).isActive = true

    processButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
    processButton.addTarget(self, action: #selector(processAction(_:)), for: .touchUpInside)

    // Initialize activity indicator for continue button
    processButton.addSubview(buttonActivityIndictor)
    buttonActivityIndictor.translatesAutoresizingMaskIntoConstraints = false
    buttonActivityIndictor.centerXAnchor.constraint(
      equalTo: processButton.centerXAnchor, constant: 0
    ).isActive = true
    buttonActivityIndictor.centerYAnchor.constraint(
      equalTo: processButton.centerYAnchor, constant: 0
    ).isActive = true
    buttonActivityIndictor.widthAnchor.constraint(equalToConstant: 40).isActive = true
    buttonActivityIndictor.heightAnchor.constraint(equalToConstant: 40).isActive = true

    view.addSubview(cancelOrderButton)
    cancelOrderButton.isHidden = true
    cancelOrderButton.layer.cornerRadius = 15
    cancelOrderButton.backgroundColor = .red
    cancelOrderButton.setTitleColor(.white, for: .normal)
    cancelOrderButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
    cancelOrderButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
    cancelOrderButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive =
      true
    cancelOrderButtonConstraint = cancelOrderButton.bottomAnchor.constraint(
      equalTo: processButton.topAnchor, constant: -15)
    cancelOrderButtonConstraint?.isActive = true

    cancelOrderButton.addTarget(self, action: #selector(cancelOrderAction(_:)), for: .touchUpInside)

    view.addSubview(menuButton)
    menuButton.backgroundColor = .white
    menuButton.setTitleColor(.gray, for: .normal)
    menuButton.layer.cornerRadius = 15

    menuButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).cgColor
    menuButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    menuButton.layer.shadowOpacity = 1.0
    menuButton.layer.shadowRadius = 2.0
    menuButton.layer.masksToBounds = false

    if hasSafeArea {
      menuButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10)
        .isActive = true
    } else {
      menuButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 35).isActive = true
    }

    menuButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive = true
    menuButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
    menuButton.heightAnchor.constraint(equalToConstant: 30).isActive = true

    menuButton.addTarget(self, action: #selector(menuAction(_:)), for: .touchUpInside)

    // add button for maps
    mapsButton.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(mapsButton)
    mapsButton.backgroundColor = .white
    mapsButton.setTitleColor(.gray, for: .normal)
    mapsButton.layer.cornerRadius = 15

    mapsButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).cgColor
    mapsButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    mapsButton.layer.shadowOpacity = 1.0
    mapsButton.layer.shadowRadius = 2.0
    mapsButton.layer.masksToBounds = false

    if hasSafeArea {
      mapsButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10)
        .isActive = true
    } else {
      mapsButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 35).isActive = true
    }
    let centerXConstraint = NSLayoutConstraint(
      item: mapsButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX,
      multiplier: 1.0, constant: 0.0)
    NSLayoutConstraint.activate([centerXConstraint])

    // directionButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive = true
    mapsButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
    mapsButton.heightAnchor.constraint(equalToConstant: 30).isActive = true

    mapsButton.addTarget(self, action: #selector(openMaps(_:)), for: .touchUpInside)

    view.addSubview(inventoryButton)
    inventoryButton.backgroundColor = .white
    inventoryButton.setTitleColor(.gray, for: .normal)
    inventoryButton.layer.cornerRadius = 15

    inventoryButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.50).cgColor
    inventoryButton.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
    inventoryButton.layer.shadowOpacity = 1.0
    inventoryButton.layer.shadowRadius = 2.0
    inventoryButton.layer.masksToBounds = false

    if hasSafeArea {
      inventoryButton.topAnchor.constraint(
        equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10
      ).isActive = true
    } else {
      inventoryButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 35).isActive = true
    }

    inventoryButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive =
      true
    inventoryButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
    inventoryButton.heightAnchor.constraint(equalToConstant: 30).isActive = true

    inventoryButton.addTarget(
      self, action: #selector(checkInventoryAction(_:)), for: .touchUpInside)

    // Status View
    view.addSubview(statusView)
    statusView.isHidden = false
    statusView.layer.cornerRadius = 5
    statusView.backgroundColor = .init(white: 0, alpha: 0.7)
    statusView.topAnchor.constraint(equalTo: menuButton.bottomAnchor, constant: 10).isActive = true
    statusView.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
    statusView.leadingAnchor.constraint(greaterThanOrEqualTo: view.leadingAnchor, constant: 60)
      .isActive = true
    statusView.trailingAnchor.constraint(lessThanOrEqualTo: view.trailingAnchor, constant: -60)
      .isActive = true

    statusView.tag = 101

    let tap = UITapGestureRecognizer(target: self, action: #selector(statusClickAction(_:)))
    statusView.addGestureRecognizer(tap)
    // Status label
    view.addSubview(statusLabel)
    statusLabel.isHidden = false
    statusLabel.textColor = .white
    statusLabel.topAnchor.constraint(equalTo: statusView.topAnchor, constant: 5).isActive = true
    statusLabel.leadingAnchor.constraint(equalTo: statusView.leadingAnchor, constant: 10).isActive =
      true
    statusLabel.trailingAnchor.constraint(equalTo: statusView.trailingAnchor, constant: -10)
      .isActive = true
    statusLabel.bottomAnchor.constraint(equalTo: statusView.bottomAnchor, constant: -5).isActive =
      true

    // Main location pin in center
    view.addSubview(robomartMarkerImage)
    robomartMarkerImage.image = UIImage(named: "robomart-marker")
    robomartMarkerImage.isHidden = true
    robomartMarkerImage.centerXAnchor.constraint(equalTo: (mapView?.centerXAnchor)!, constant: 0)
      .isActive = true
    robomartMarkerImage.centerYAnchor.constraint(equalTo: (mapView?.centerYAnchor)!, constant: 0)
      .isActive = true
    robomartMarkerImage.widthAnchor.constraint(equalToConstant: 26).isActive = true
    robomartMarkerImage.heightAnchor.constraint(equalToConstant: 52).isActive = true

    // Main location pin in center
    view.addSubview(userMarkerImage)
    userMarkerImage.image = UIImage(named: "location-icon")
    userMarkerImage.isHidden = true
    userMarkerImage.centerXAnchor.constraint(equalTo: (mapView?.centerXAnchor)!, constant: 0)
      .isActive = true
    userMarkerImage.centerYAnchor.constraint(equalTo: (mapView?.centerYAnchor)!, constant: -20)
      .isActive = true
    userMarkerImage.widthAnchor.constraint(equalToConstant: 40).isActive = true
    userMarkerImage.heightAnchor.constraint(equalToConstant: 40).isActive = true
  }

  // Get order for address
  private func getOrder(orderID: String?) {
    APIManager.sharedInstance.get(endpoint: .getOrder(id: orderID!)) { res, error in

      if let err = error {
        loggerDebug.error("\(err.localizedDescription)")
      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace("Order details: \(json)")

          // Convert json data into model
          let response = try JSONDecoder().decode(Order.self, from: res!)

          loggerDebug.trace("Order Details: \(response.data!)")

          AppUser.sharedInstance.saveData(response, forKey: .currentOrder)

        } catch let jsonError {
          loggerDebug.error("\(jsonError.localizedDescription)")
        }
      }
    }
  }

  // Re-center map if map dragged
  @objc private func statusClickAction(_ sender: UIGestureRecognizer) {
    loggerDebug.debug("\(String(describing: sender.view?.tag))")
    let vehicleState = sender.view?.tag

    if vehicleState! == 101 {
      resetLocationAction(nil)
    } else if vehicleState! == 102 {  // incoming request: zoom on consumer and driver
      if userCoordinates != nil && vehicleCoordinates != nil {
        if self.usertMarker == nil {
          self.setupUserMarker()
          self.userMarkerImage.isHidden = false
          self.usertMarker!.map = self.mapView
        } else {
          self.userMarkerImage.isHidden = false
          self.usertMarker!.map = self.mapView
        }
        zoomMapWithTwoPoints(start: vehicleCoordinates, end: userCoordinates)
      }
    } else if vehicleState! == 100 || vehicleState! == 104 || vehicleState! == 105
      || vehicleState! == 106 || vehicleState! == 107 || vehicleState! == 108
      || vehicleState! == 109
    {
      //idle, arrived, doors opening, shopping,
      //end shopping, doors closing, check basket: zoom robomart
      if vehicleCoordinates != nil {
        zoomMapToPoint(point: vehicleCoordinates!)
      }
    } else {
      loggerDebug.trace(
        "Checking coordinates: vehicle: \(String(describing: vehicleCoordinates)) & user: \(String(describing: userCoordinates)))"
      )

      if vehicleCoordinates != nil, userCoordinates != nil {
        // Get map zoom initiate
        zoomMapWithoutPath(start: vehicleCoordinates!, end: userCoordinates!)
      }
    }
  }

  // Get assigned robomart with name
  func getAssignedRobomartWithName() {
    guard let vehicleID: String = AppUser.sharedInstance.getData(forKey: .vehicleID) else {
      loggerDebug.warning("Vehicle ID does not exist.")
      return
    }

    APIManager.sharedInstance.get(endpoint: .getAssignedRobomrtWithName(id: vehicleID)) {
      res, error in

      if let err = error {
        loggerDebug.error("\(err.localizedDescription)")
      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace("AssignedRobomrtWithName: \(json)")

          let success = json["success"].boolValue

          if !success {
            // If vehicle is not assigned then user will be signout
            signOutUser(fromLogin: false)
          } else {
            // Convert json data into model
            let response = try JSONDecoder().decode(VehicleWithName.self, from: res!)
            loggerDebug.trace("AssignedRobomrtWithName: \(response.data!)")

            AppUser.sharedInstance.saveData(response.data, forKey: .vehicleName)
            self.menuButton.setTitle("\(response.data!.name ?? "Menu")", for: .normal)
          }

        } catch let jsonError {
          loggerDebug.error("\(jsonError.localizedDescription)")
        }
      }
    }
  }

  // Logout action
  @objc private func menuAction(_ sender: UIButton) {
    loggerDebug.debug("Menu button clicked!")

    if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(
      withIdentifier: "MenuParentVC") as? MenuParentVC
    {

      guard
        let vehicleName: VehicleWithNameData = AppUser.sharedInstance.getData(forKey: .vehicleName)
      else {
        loggerDebug.warning("Vehicle name does not exits.")
        return
      }

      vc.pageTitle = vehicleName.name ?? ""

      present(vc, animated: true, completion: nil)
    }
  }

  // Show inventory view
  @objc private func checkInventoryAction(_ sender: UIButton) {
    performSegue(withIdentifier: "inventory", sender: self)
    // if #available(iOS 13.0, *) {
    //     let vc = (storyboard?.instantiateViewController(identifier: "InventoryVC"))! as InventoryVC
    //     vc.isModalInPresentation = true
    //     present(vc, animated: true)
    //
    // } else {
    //     // Fallback on earlier versions
    // }
  }

  // Maps button click handler
  @objc private func openMaps(_ sender: UIButton) {
    let customAlert =
      self.storyboard?.instantiateViewController(withIdentifier: "MapAlert") as! ChooseMap
    customAlert.providesPresentationContextTransitionStyle = true
    customAlert.definesPresentationContext = true
    customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
    customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
    customAlert.lat = userCoordinates!.latitude
    customAlert.lon = userCoordinates!.longitude

    self.present(customAlert, animated: true, completion: nil)

  }

  // Cancel order action
  @objc private func cancelOrderAction(_ sender: Any?) {
    AlertService.shared.alertWithTextField(
      vc: self, title: "Robomart", message: "Pleae type 'CANCEL' if you want to cancel the order.",
      placeholder: ""
    ) { result in

      if result == "CANCEL" {
        self.isOrderCanceled = true
        loggerEvents.info("cancel_engagement")
        self.loadingView = LoadingView(uiView: self.view, message: "Canceling order...")
        if let loaderView = self.loadingView {  // If loadingView already exists
          if loaderView.isHidden() {
            loaderView.show()  // To show activity indicator
          }
        }

        let uid: String? = AppUser.sharedInstance.getData(forKey: .userID)
        let oid: String? = AppUser.sharedInstance.getData(forKey: .orderID)

        if uid != nil, oid != nil {
          self.userID = uid!
          self.orderID = oid!
        }

        self.customInfoWindow.removeFromSuperview()

        loggerDebug.trace(
          "Checking userid: \(String(describing: self.userID)) and Order id: \(String(describing: self.orderID))"
        )

        DispatchQueue.main.async {
          APIManager.sharedInstance.delete(
            endpoint: .cancelByDriver(consumerID: self.userID!, orderID: self.orderID!),
            parameters: nil
          ) { _, error in

            if let err = error {
              loggerDebug.error("Unable to cancel: \(err.localizedDescription)")
              AlertService.shared.alert(in: self, message: "\(err.localizedDescription)")
            }

            AppUser.sharedInstance.removeData(forKey: .userID)
            AppUser.sharedInstance.removeData(forKey: .orderID)

            if let loaderView = self.loadingView {  // If loadingView already exists
              loaderView.hide()  // To show activity indicator
            }
          }
        }
      } else if result == "NO" {
        if let loaderView = self.loadingView {  // If loadingView already exists
          loaderView.hide()  // To show activity indicator
        }

      } else {
        self.cancelOrderAction(nil)
      }
    }
  }

  // Process button actions
  @objc private func processAction(_ sender: UIButton) {
    sender.isEnabled = false

    processButton.setTitle("", for: .normal)
    buttonActivityIndictor.startAnimating()

    if sender.tag == 2 {
      // Slide to unlock button initiated
      createUnlockSlider(update: false, isHidden: false) { _ in
        // Assign tag to slider button
        self.unlockSlider.sliderText = "SLIDE TO CONFIRM ARRIVAL"
        self.unlockSlider.tag = 3
      }

      processAPICall(endpoint: .startEnRoute, param: nil) { completed, error in
        if !completed! {
          loggerDebug.error("Error: \(error!)")
          // Stop/hide loader animation
          self.processButton.setTitle(
            requestStateButtonString(state: "\(sender.tag)"), for: .normal)
          self.processButton.isEnabled = true

          // Slide to unlock button initiated
          self.createUnlockSlider(update: false, isHidden: true) { _ in
            // Assign tag to slider button
            self.unlockSlider.sliderText = "SLIDE TO CONFIRM ARRIVAL"
            self.unlockSlider.tag = 3
          }
        }

        // Stop/hide loader animation
        // self.processButton.setTitle(RequestStateButtonString(state: "\(sender.tag)"), for: .normal)
        self.buttonActivityIndictor.stopAnimating()

        self.processButton.isHidden = true

        // slide to unlock button initiated
        self.createUnlockSlider(update: true, isHidden: false) { _ in
          // Assign tag to slider button
          self.unlockSlider.sliderText = "SLIDE TO CONFIRM ARRIVAL"
          self.unlockSlider.tag = 3
        }
      }

    } else if sender.tag == 3 {
      let params: Parameters = ["status": "Arrived"]

      processAPICall(endpoint: .setRobomartStatus, param: params) { completed, error in
        if !completed! {
          loggerDebug.error("Error: \(error!)")
          self.processButton.isEnabled = true
          AlertService.shared.alert(in: self, message: "\(error!)")
        }

        // Stop/hide loader animation
        self.processButton.setTitle(requestStateButtonString(state: "\(sender.tag)"), for: .normal)
        self.buttonActivityIndictor.stopAnimating()
      }

    } else if sender.tag == 5 {
      let params: Parameters = ["command": "open"]
      // Send log that we try open the door
      loggerEvents.info("open_door")
      processAPICall(endpoint: .openAndCloseDoors, param: params) { completed, error in
        if !completed! {
          loggerDebug.error("Error: \(error!)")
          self.processButton.isEnabled = true
          AlertService.shared.alert(in: self, message: "\(error!)")
        }

        // Stop/hide loader animation
        self.processButton.setTitle(requestStateButtonString(state: "\(sender.tag)"), for: .normal)
        self.buttonActivityIndictor.stopAnimating()
      }

    } else if sender.tag == 8 {
      let params: Parameters = ["command": "close"]
      // Send log that we try close the door
      loggerEvents.info("close_door")
      processAPICall(endpoint: .openAndCloseDoors, param: params) { completed, error in
        if !completed! {
          loggerDebug.error("Error: \(error!)")
          self.processButton.isEnabled = true
          AlertService.shared.alert(in: self, message: "\(error!)")
        }

        // Stop/hide loader animation
        self.processButton.setTitle(requestStateButtonString(state: "\(sender.tag)"), for: .normal)
        self.buttonActivityIndictor.stopAnimating()
      }

    } else if sender.tag == 9 {
      // Stop calling receipt api if receipt is generated
      customReceiptView.timer?.invalidate()

      let confirmReceiptPrompt = UIAlertController(
        title: "Confirm Receipt", message: "Are you sure, You want to generate a receipt?",
        preferredStyle: .alert)

      confirmReceiptPrompt.view.tintColor = .gray

      confirmReceiptPrompt.addAction(
        UIAlertAction(
          title: "Yes", style: .default,
          handler: { (_: UIAlertAction!) in

            // Stop calling receipt api if receipt is generated
            self.customReceiptView.timer?.invalidate()

            self.processAPICall(endpoint: .confirmReceipt, param: nil) { completed, error in
              if !completed! {
                loggerDebug.error("Error: \(error!)")

                self.processButton.isEnabled = true

                // If API returns error then rerun receipt timer
                self.customReceiptView.runGetReceiptTimer()

                // AlertService.shared.alert(in: self, message: "\(error!)")
              } else {
                self.customReceiptView.isHidden = true
                self.customReceiptView.hide()
              }

              // Stop/hide loader animation

              self.processButton.setTitle(
                requestStateButtonString(state: "\(sender.tag)"), for: .normal)
              self.buttonActivityIndictor.stopAnimating()
            }

          }))

      confirmReceiptPrompt.addAction(
        UIAlertAction(
          title: "No", style: .cancel,
          handler: { (_: UIAlertAction!) in
            loggerDebug.debug("Handle Cancel Logic here")

            // If API returns error then rerun receipt timer
            self.customReceiptView.runGetReceiptTimer()

            // Stop/hide loader animation
            self.processButton.setTitle(
              requestStateButtonString(state: "\(sender.tag)"), for: .normal)
            self.processButton.isEnabled = true
            self.buttonActivityIndictor.stopAnimating()
          }))

      present(confirmReceiptPrompt, animated: true, completion: nil)

    } else {
      loggerDebug.debug("Other tag: \(sender.tag)")

      // Stop/hide loader animation
      processButton.setTitle(requestStateButtonString(state: "\(sender.tag)"), for: .normal)
      buttonActivityIndictor.stopAnimating()
    }
  }

  @objc func runGenerationTimerCountdown() {
    loggerDebug.debug("Receipt timer starts...")
    buttonActivityIndictor.stopAnimating()
    receiptGenerationTimer = Timer.scheduledTimer(
      timeInterval: 1.0,
      target: self,
      selector: #selector(runCountDown(_:)),
      userInfo: nil,
      repeats: true)
  }

  @objc func runCountDown(_ sender: Timer) {
    receiptCountdownTime -= 1
    processButton.setTitle("\(receiptCountdownTime)", for: .normal)
    if receiptCountdownTime == 0 {
      receiptGenerationTimer?.invalidate()
      processButton.setTitle("", for: .normal)
      #if DEBUG
        self.receiptCountdownTime = 5
      #else
        self.receiptCountdownTime = 90
      #endif

      DispatchQueue.main.async {
        self.processReceiptAction()
      }
    }
  }

  // Will call automatically when countdown completed to generate receipt
  @objc func processReceiptAction() {
    // Stop calling receipt api if receipt is generated
    customReceiptView.timer?.invalidate()

    processButton.setTitle("", for: .normal)
    buttonActivityIndictor.startAnimating()

    processAPICall(endpoint: .confirmReceipt, param: nil) { completed, error in
      if !completed! {
        loggerDebug.error("Error: \(error!)")

        self.processButton.isEnabled = true

        // If API returns error then rerun receipt timer
        self.customReceiptView.runGetReceiptTimer()

        // AlertService.shared.alert(in: self, message: "\(error!)")
      } else {
        self.customReceiptView.isHidden = true
        self.customReceiptView.hide()
      }

      // Stop/hide loader animation

      self.buttonActivityIndictor.stopAnimating()
    }
  }

  // Process API call
  private func processAPICall(
    endpoint: EndPoints, param: Parameters?, completion: @escaping (Bool?, String?) -> Void
  ) {
    let para = param ?? [:]

    loggerDebug.trace("\(para)")

    APIManager.sharedInstance.post(endpoint: endpoint, parameters: para) { res, error in

      if let err = error {
        loggerDebug.error("\(err.localizedDescription)")
        completion(false, "\(err.localizedDescription)")
        AlertService.shared.alert(in: self, message: "\(err.localizedDescription)")

      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace("\(json)")

          let succes = json["success"].boolValue
          let resError = json["error"].stringValue

          if succes {
            completion(true, nil)
          } else {
            completion(false, "\(resError)")
          }

        } catch let jsonError {
          loggerDebug.error("\(jsonError.localizedDescription)")
          completion(false, "\(jsonError.localizedDescription)")
        }
      }
    }
  }

  // Setup Mapview UI
  private func createMapView() {
    // Initialize google maps camera position

    loggerDebug.debug("Map test: checking Camera: createMapView")
    camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: zoom)

    // Initialize mapview with camera
    mapView = GMSMapView.map(withFrame: .zero, camera: camera)

    // Assign camera to mapview camera
    // mapView?.camera = camera
    mapView?.isMyLocationEnabled = true
    mapView?.settings.myLocationButton = false
    mapView?.settings.compassButton = true

    view.addSubview(mapView!)
    mapView?.translatesAutoresizingMaskIntoConstraints = false

    mapView?.topAnchor.constraint(equalTo: view.topAnchor, constant: 0).isActive = true
    mapView?.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0).isActive = true
    mapView?.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0).isActive = true
    mapViewBottomConstraint = mapView?.bottomAnchor.constraint(
      equalTo: view.bottomAnchor, constant: 0)
    mapViewBottomConstraint?.isActive = true

    mapView?.delegate = self

    mapPadding = UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20)

    if hasSafeArea {
      mapPadding.bottom = 10
    } else {
      mapPadding.bottom = 30
    }

    mapView?.padding = mapPadding

    do {
      // Set the map style by passing the URL of the local file.
      if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
        mapView?.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
      } else {
        loggerDebug.error("Unable to find style.json")
      }
    } catch {
      loggerDebug.error(
        "One or more of the map styles failed to load. \(error.localizedDescription)")
    }

    mapView?.addSubview(resetLocationButton)
    resetLocationButton.isHidden = false
    resetLocationButton.backgroundColor = .init(white: 1, alpha: 1)
    resetLocationButton.layer.borderWidth = 1
    resetLocationButton.layer.borderColor = UIColor(white: 0.80, alpha: 1).cgColor
    resetLocationButton.tintColor = .black
    resetLocationButton.layer.cornerRadius = 5
    resetLocationButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)

    if hasSafeArea {
      resetLocationButtonBottomConstraint = resetLocationButton.bottomAnchor.constraint(
        equalTo: mapView!.safeAreaLayoutGuide.bottomAnchor, constant: -100)
      resetLocationButtonBottomConstraint?.isActive = true
    } else {
      resetLocationButtonBottomConstraint = resetLocationButton.bottomAnchor.constraint(
        equalTo: mapView!.bottomAnchor, constant: -125)
      resetLocationButtonBottomConstraint?.isActive = true
    }

    // resetLocationButton.centerYAnchor.constraint(equalTo: self.mapView!.centerYAnchor, constant: -20).isActive = true
    resetLocationButton.trailingAnchor.constraint(equalTo: mapView!.trailingAnchor, constant: -15)
      .isActive = true
    resetLocationButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
    resetLocationButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    resetLocationButton.addTarget(
      self, action: #selector(resetLocationAction(_:)), for: .touchUpInside)
  }

  // Recenter current location on map
  @objc func resetLocationAction(_: UIButton?) {
    guard let lat = mapView?.myLocation?.coordinate.latitude,
      let lng = mapView?.myLocation?.coordinate.longitude
    else {
      loggerDebug.warning("latitude or longtitude is nil")
      return
    }

    loggerDebug.debug("resetLocationAction: lat: \(lat) and lng: \(lng)")
    loggerDebug.debug("Map test: checking Camera: resetLocationAction")
    let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: zoom)
    // self.mapView?.camera = camera
    mapView!.animate(to: camera)
  }

  // Check firebase user
  private func getFirebaseUser() {
    loggerDebug.debug("getFirebaseUser")

    if let vehicleID: String = AppUser.sharedInstance.getData(forKey: .vehicleID) {
      loggerDebug.trace("Vehicle id check: \(vehicleID)")

      vehicleRef.child(vehicleID).observeSingleEvent(of: .value) { snapshot in

        if !snapshot.exists() {
          AlertService.shared.alert(in: self, message: "Vehicle does not exist.")
        } else {
          if let latStr = snapshot.childSnapshot(forPath: "location/lat").value as? String,
            let lngStr = snapshot.childSnapshot(forPath: "location/lng").value as? String,
            let angleStr = snapshot.childSnapshot(forPath: "location/angle").value as? String,
            let lat = Double(latStr),
            let lng = Double(lngStr),
            let angle = Double(angleStr)
          {

            if let userID = snapshot.childSnapshot(forPath: "customer_id").value as? String {
              loggerDebug.debug("Saving UserID: \(userID)")
              AppUser.sharedInstance.saveData(userID, forKey: .userID)

            } else {
              loggerDebug.warning("Saving UserID: user not available")
            }

            loggerDebug.debug("Vehicle lat: \(lat) and long: \(lng)")

            // Check if robomart marker is nil then initiate the marker
            if self.robomartMarker == nil {
              self.robomartMarker = GMSMarker()
            }

            self.robomartMarker?.isTappable = false
            self.robomartMarkerImage.isHidden = false
            let pinMarkerView = self.robomartMarkerImage
            pinMarkerView.contentMode = .scaleAspectFit
            self.robomartMarker!.iconView = pinMarkerView

            // Reset robomart market position and angle
            self.robomartMarker!.position = CLLocationCoordinate2D(
              latitude: lat,
              longitude: lng)
            self.robomartMarker!.rotation = angle
            self.robomartMarker!.map = self.mapView
            self.robomartMarker!.groundAnchor = CGPoint(x: 0.5, y: 0.5)

          }

          let vehicleState = snapshot.childSnapshot(forPath: "request_state").value as? String

          loggerDebug.trace(
            "Checking vehicle state: \(String(describing: vehicleState))")

          // If restock is active then remove the process view
          if let isRestockActive: Bool = AppUser.sharedInstance.getData(forKey: .isRestockActive) {
            if isRestockActive {
              // Remove processing loader
              if let loaderView = self.loadingView {  // If loadingView already exists
                loaderView.hide()  // To show activity indicator

                // Set isRestockActive value to false
                AppUser.sharedInstance.saveData(false, forKey: .isRestockActive)

                AlertService.shared.alert(in: self, message: "Restock completed.")
              }
            }
          }

          // Check for route
          let route = self.vehicleRef.child("\(vehicleID)/route")

          self.statusChangeAction(dataSnapshot: snapshot)

          // Add observer on route chile change
          route.observe(.childChanged) { routeSnapshot in

            if !snapshot.exists() {
              AlertService.shared.alert(in: self, message: "Route does not exist.")
            } else {
              loggerDebug.trace("Checking route")

              self.vehicleRef.child(vehicleID).observeSingleEvent(of: .value) { vSnapshot in
                if !vSnapshot.exists() { return }

                let vehicleState =
                  vSnapshot.childSnapshot(forPath: "request_state").value as? String

                loggerDebug.trace(
                  "Checking route, request_state: \(String(describing: vehicleState))"
                )

                if vehicleState! == "3" {
                  if routeSnapshot.key == "polyline" {
                    let polyline = routeSnapshot.value as? String

                    loggerDebug.trace(
                      "Checking route, Polyline value: \(String(describing: polyline))"
                    )

                    // Get polyline from firebase
                    self.getPolylineFromFirebase(snapshot: snapshot, polyline: polyline!)

                    self.robomartMarker!.map = self.mapView

                  }
                }
              }
            }
          }

          // Check for Location
          let location = self.vehicleRef.child("\(vehicleID)/location")

          location.observe(.childChanged) { locationSnapshot in

            if !locationSnapshot.exists() {
              AlertService.shared.alert(in: self, message: "Location does not exist.")
            } else {
              loggerDebug.trace("Checking snapshot key: \(locationSnapshot.key)")

              self.vehicleRef.child("\(vehicleID)/location").observeSingleEvent(of: .value) {
                snap in

                if !snap.exists() {
                  AlertService.shared.alert(in: self, message: "Vehicle does not exist.")
                } else {
                  let lat = snap.childSnapshot(forPath: "lat").value as? String
                  let lng = snap.childSnapshot(forPath: "lng").value as? String
                  let angle = snap.childSnapshot(forPath: "angle").value as? String

                  let uid: String? = AppUser.sharedInstance.getData(forKey: .userID)

                  if uid != nil {
                    loggerDebug.debug(
                      "Saving UserID: from local: \(String(describing: uid))")
                    self.userID = uid!
                  } else {
                    if let userID = snapshot.childSnapshot(forPath: "customer_id").value as? String
                    {
                      self.userID = userID
                      loggerDebug.debug("Saving UserID: \(userID)")
                      AppUser.sharedInstance.saveData(userID, forKey: .userID)
                    } else {
                      loggerDebug.warning("Saving UserID: does not exist")
                      self.userID = ""
                    }
                  }

                  if lat != nil, lng != nil, angle != nil {
                    self.robomartMarker!.position = CLLocationCoordinate2D(
                      latitude: Double((lat as AnyObject).doubleValue),
                      longitude: Double((lng as AnyObject).doubleValue))
                    self.robomartMarker!.rotation = Double((angle as AnyObject).doubleValue)

                    self.robomartMarker!.map = self.mapView

                    self.vehicleRef.child(vehicleID).observeSingleEvent(of: .value) { vSnapshot in
                      if !vSnapshot.exists() { return }

                      let vehicleState =
                        vSnapshot.childSnapshot(forPath: "request_state").value as? String

                      loggerDebug.trace(
                        "Checking vehicle state: \(String(describing: vehicleState))")

                      if vehicleState! == "3" || vehicleState! == "4" {
                        // If vehicle state is 3 or 4 then do nothing
                        loggerDebug.debug("Vehicle state: \(vehicleState ?? "nil")")
                      } else {
                        let camera = GMSCameraPosition.camera(
                          withLatitude: Double((lat as AnyObject).doubleValue),
                          longitude: Double((lng as AnyObject).doubleValue), zoom: self.zoom)
                        self.mapView!.animate(to: camera)
                      }
                    }

                    let oid = snapshot.childSnapshot(forPath: "order_id").value as? String

                    if oid != nil {
                      if self.usertMarker == nil {
                        // if self.userID! != nil {

                        loggerDebug.debug("Reset UI: Setting up user marker")
                        DispatchQueue.main.async {
                          // Get user data from firebase
                          self.getUser(userID: self.userID!) { _ in

                            self.userMarkerImage.isHidden = false
                            self.usertMarker!.map = self.mapView

                            self.robomartMarker!.map = self.mapView
                          }
                        }

                      } else {
                        loggerDebug.debug("Reset UI: adding marker to map")
                        self.usertMarker!.map = self.mapView
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }

      // Add observer to check vehicle state
      vehicleRef.child(vehicleID).observe(.childChanged) { snapshot in

        if snapshot.key == "request_state" {
          self.statusChangeAction(dataSnapshot: snapshot)

          let vehicleState = snapshot.value as? String

          switch vehicleState {
          case "1":
            // Status on end
            loggerDebug.debug("State 1")
            self.statusLabel.text = "Idle"

          case "2":
            // Status on BEING ASSIGNED
            loggerDebug.debug("State 2 BEING ASSIGNED")
            self.statusLabel.text = ""  // "Incoming Request"

          case "3":
            // Status on EN ROUTE
            loggerDebug.debug("State 3 EN ROUTE")
            self.statusLabel.text = "En Route"

          case "4":
            // Status on ARRIVED
            loggerDebug.debug("State 4 ARRIVED")
            self.statusLabel.text = "Arrived"

          case "5":
            // Status on DOORS OPENING
            loggerDebug.debug("State 5 DOORS OPENING")
            self.statusLabel.text = "Doors Opening"

          case "6":
            // Status on SHOPPING
            loggerDebug.debug("State 6 SHOPPING")
            self.statusLabel.text = "Shopping"

          case "7":
            // Status on END SHOPPING
            loggerDebug.debug("State 7 END SHOPPING")
            self.statusLabel.text = "End Shopping"

          case "8":
            // Status on DOORS CLOSING
            loggerDebug.debug("State 8 DOORS CLOSING")
            self.statusLabel.text = "Doors Closing"

          case "9":
            // Status on CHECK BASKET
            loggerDebug.debug("State 9 CHECK BASKET")
            self.statusLabel.text = "Check Basket"

          default:
            // It would be 0 END
            loggerDebug.debug("State 0 END")
            self.statusLabel.text = "Idle"
          }
        }
      }
    }
  }

  // MARK: DRAW PATH FROM SERVER STRING

  func addPolyLineWithEncodedStringInMap(encodedString: String, roboLat: String?, roboLng: String?)
  {
    loggerDebug.trace("encoded String path : \(encodedString)")

    if encodedString == "" {
      loggerDebug.warning("path is empty here!")
      camera = GMSCameraPosition.camera(
        withLatitude: Double(((roboLat!) as AnyObject).doubleValue),
        longitude: Double(((roboLng!) as AnyObject).doubleValue), zoom: zoom)
      mapView?.camera = camera
    }

    if let polylineNew = polyline {
      polylineNew.map?.clear()  // = nil
    }

    // let path = GMSPath(fromEncodedPath: encodedString)!
    path = GMSPath(fromEncodedPath: encodedString)!
    loggerDebug.trace("this is path : \(String(describing: path))")
    polyline = GMSPolyline(path: path!)
    polyline!.strokeWidth = 4
    polyline!.strokeColor = UIColor.darkGray

    polyline!.map = mapView

    let bounds = GMSCoordinateBounds(path: path!)
    // mapView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 120.0))
    mapView!.animate(
      with: GMSCameraUpdate.fit(
        bounds, with: UIEdgeInsets(top: 125, left: 100, bottom: 165, right: 100)))
  }

  // Zoom on user location and robomart when path is not on the map
  private func zoomMapWithoutPath(start: CLLocationCoordinate2D?, end: CLLocationCoordinate2D?) {
    // let bounds = GMSCoordinateBounds(coordinate: start!, coordinate: end!)
    var bounds: GMSCoordinateBounds?
    if path != nil && path!.count() > 0 {
      bounds = GMSCoordinateBounds(path: path!)
    } else {
      bounds = GMSCoordinateBounds(coordinate: start!, coordinate: end!)
    }
    // let camera = GMSCameraUpdate.fit(bounds, withPadding: 100.0)
    // let camera = GMSCameraUpdate.fit(bounds, with: UIEdgeInsets(top: 125, left: 100, bottom: 165, right: 100))
    mapView!.animate(
      with: GMSCameraUpdate.fit(
        bounds!, with: UIEdgeInsets(top: 125, left: 100, bottom: 165, right: 100)))

  }

  private func zoomMapToPoint(point: CLLocationCoordinate2D?) {
    guard let lat = point?.latitude, let lng = point?.longitude else {
      loggerDebug.warning("latitude or longtitude is nil")
      return
    }
    loggerDebug.debug("resetLocationAction: lat: \(lat) and lng: \(lng)")
    loggerDebug.debug("Map test: checking Camera: resetLocationAction")
    let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: zoom)
    mapView!.animate(to: camera)
  }

  private func zoomMapWithTwoPoints(start: CLLocationCoordinate2D?, end: CLLocationCoordinate2D?) {
    let bounds = GMSCoordinateBounds(coordinate: start!, coordinate: end!)
    mapView!.animate(
      with: GMSCameraUpdate.fit(
        bounds, with: UIEdgeInsets(top: 125, left: 100, bottom: 165, right: 100)))

  }

  // Call events if state is changed
  private func statusChangeAction(dataSnapshot: DataSnapshot) {
    if let vehicleID: String = AppUser.sharedInstance.getData(forKey: .vehicleID) {
      vehicleRef.child(vehicleID).observeSingleEvent(of: .value) { [self] snapshot in

        if !snapshot.exists() {
          loggerDebug.error("Vehicle does not exist!")
          return
        }

        if let latStr = snapshot.childSnapshot(forPath: "location/lat").value as? String,
          let lngStr = snapshot.childSnapshot(forPath: "location/lng").value as? String,
          let lat = Double(latStr),
          let lng = Double(lngStr),
          let vehicleState = snapshot.childSnapshot(forPath: "request_state").value as? String
        {

          let uID = snapshot.childSnapshot(forPath: "customer_id").value as? String
          _ = snapshot.childSnapshot(forPath: "type").value as? String

          loggerDebug.debug("vehicle lat: \(lat) and long: \(lng)")

          let vehicleCoordinates = CLLocationCoordinate2DMake(
            Double((lat as AnyObject).doubleValue), Double((lng as AnyObject).doubleValue))

          self.vehicleCoordinates = vehicleCoordinates

          loggerDebug.trace("Checking userid: \(String(describing: uID))")

          if uID != nil {
            AppUser.sharedInstance.saveData(uID, forKey: .userID)

            loggerDebug.debug("Reset UI: Setting up user marker")

            // Get user
            DispatchQueue.main.async {
              // Get user data from firebase
              self.getUser(userID: uID) { user in

                if user == nil {
                  loggerDebug.error(
                    "User does not exist: \(String(describing: user))")
                  return
                }

                let uid: String? = AppUser.sharedInstance.getData(forKey: .userID)
                let oid: String? = AppUser.sharedInstance.getData(forKey: .orderID)

                loggerDebug.trace("Checking User: \(String(describing: user))")

                let userLat = user!.childSnapshot(forPath: "location/lat").value as? Double
                let userLng = user!.childSnapshot(forPath: "location/lng").value as? Double

                let userCoordinates = CLLocationCoordinate2DMake(
                  Double((userLat as AnyObject).doubleValue),
                  Double((userLng as AnyObject).doubleValue))

                self.userCoordinates = userCoordinates

                if uid != nil {
                  self.userID = uid!
                } else {
                  if let userID = snapshot.childSnapshot(forPath: "customer_id").value as? String {
                    AppUser.sharedInstance.saveData(userID, forKey: .orderID)
                    self.userID = userID

                  } else {
                    self.userID = ""
                  }
                }

                if oid != nil {
                  self.orderID = oid!
                } else {
                  if let oID = user!.childSnapshot(forPath: "order_id").value as? String {
                    AppUser.sharedInstance.saveData(oID, forKey: .orderID)
                    self.orderID = oID

                    loggerDebug.trace(
                      "Checking userid: \(String(describing: self.userID)) and orderid: \(String(describing: self.orderID))"
                    )

                    self.getOrder(orderID: self.orderID!)

                  } else {
                    self.orderID = ""
                  }
                }

                loggerDebug.trace(
                  "Checking userid: \(String(describing: self.userID)) and orderid: \(String(describing: self.orderID))"
                )
              }
            }
          }

          self.statusLabel.text = requestStateString(state: vehicleState)
          if vehicleState == "8" || vehicleState == "9" {
            if vehicleState == "8" {
              self.buttonActivityIndictor.startAnimating()
            }

            self.processButton.setTitle("", for: .normal)

          } else {
            self.processButton.setTitle(
              "\(requestStateButtonString(state: vehicleState))", for: .normal)
          }

          self.processButton.tag = Int(vehicleState)!

          // Assign tag to status view to check state
          self.statusView.tag = Int("10\(vehicleState)")!

          self.cancelOrderButton.isHidden = false

          if vehicleState != "9" {
            self.mapsButton.isHidden = true
            if !self.customReceiptView.isHidden {
              self.customReceiptView.isHidden = true
              self.customReceiptView.hide()
            }
          }

          if vehicleState == "5" || vehicleState == "6" || vehicleState == "7"
            || vehicleState == "8" || vehicleState == "9"
          {
            self.mapsButton.isHidden = true

            self.cancelOrderButton.isHidden = true
          }

          if vehicleState == "3" {
            // En Route
            self.mapsButton.isHidden = false

            loggerDebug.trace("Checking status: En Route")

            if self.usertMarker == nil {
              self.setupUserMarker()
              self.userMarkerImage.isHidden = false
              self.usertMarker!.map = self.mapView
            } else {
              self.userMarkerImage.isHidden = false
              self.usertMarker!.map = self.mapView
            }

            // Slide to unlock button initiated
            self.createUnlockSlider(update: true, isHidden: false) { _ in
              // Assign tag to slider button
              self.unlockSlider.sliderText = "SLIDE TO CONFIRM ARRIVAL"
              self.unlockSlider.tag = Int(vehicleState)!
            }

            let polyline = snapshot.childSnapshot(forPath: "route/polyline").value as? String

            if polyline != nil {
              loggerDebug.trace("Polyline value: \(String(describing: polyline))")

              // DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
              // Get polyline from firebase
              self.getPolylineFromFirebase(snapshot: snapshot, polyline: polyline)
              self.robomartMarker!.map = self.mapView
              //                            }

            } else {
              loggerDebug.error("Polyline does not exist.")
            }

          } else if vehicleState == "4" {
            // Arrived

            self.mapsButton.isHidden = false

            self.processButton.isHidden = true

            // Slide to unlock button initiated
            self.createUnlockSlider(update: false, isHidden: false) { _ in
              // Assign tag to slider button
              self.unlockSlider.tag = Int(vehicleState)!
            }

            // let vehicleCoordinates = CLLocationCoordinate2DMake(Double((lat as AnyObject).doubleValue), Double((lng as AnyObject).doubleValue))
            // self.vehicleCoordinates = vehicleCoordinates

            // Get user
            DispatchQueue.main.async {
              loggerDebug.debug("Reset UI: Setting up user marker")
              // Get user data from firebase
              self.getUser(userID: uID) { currentUser in

                guard let user = currentUser else {
                  loggerDebug.error("User is nil")
                  return
                }

                loggerDebug.trace("Checking user type: \(user.childSnapshot(forPath: "type"))")

                let userLat = user.childSnapshot(forPath: "location/lat").value as? Double
                let userLng = user.childSnapshot(forPath: "location/lng").value as? Double

                let userCoordinates = CLLocationCoordinate2DMake(
                  Double((userLat as AnyObject).doubleValue),
                  Double((userLng as AnyObject).doubleValue))

                self.userCoordinates = userCoordinates

                self.polyline?.map?.clear()

                if self.usertMarker == nil {
                  self.setupUserMarker()
                  self.userMarkerImage.isHidden = false
                  self.usertMarker!.map = self.mapView
                } else {
                  self.userMarkerImage.isHidden = false
                  self.usertMarker!.map = self.mapView
                }

                // Get map zoom initiate
                self.zoomMapWithoutPath(start: vehicleCoordinates, end: userCoordinates)
                self.robomartMarker!.map = self.mapView

                // Check if type exists in user node
                if user.childSnapshot(forPath: "type").exists() {
                  let userTYpe = user.childSnapshot(forPath: "type").value as? Int

                  // If type = 2 -> it means restock in progress
                  if userTYpe == 2 {
                    self.loadingView = LoadingView(
                      uiView: self.view, message: "Restock in progress...")
                    if let loaderView = self.loadingView {  // If loadingView already exists
                      if loaderView.isHidden() {
                        loaderView.show()  // To show activity indicator
                      }
                    }

                  } else {
                    if let loaderView = self.loadingView {  // If loadingView already exists
                      if loaderView.isHidden() {
                        loaderView.hide()  // To show activity indicator
                      }
                    }
                  }
                }
              }
            }

          } else if vehicleState == "2" || vehicleState == "5" || vehicleState == "8"
            || vehicleState == "9"
          {
            self.mapsButton.isHidden = true
            // self.statusLabel.text = RequestStateString(state: vehicleState!)

            if vehicleState == "2" {
              self.view.bringSubviewToFront(self.processButton)

              // If restock is active then remove the process view
              if let isRestockActive: Bool = AppUser.sharedInstance.getData(
                forKey: .isRestockActive)
              {
                if isRestockActive {
                  // Remove processing loader
                  if let loaderView = self.loadingView {  // If loadingView already exists
                    loaderView.hide()  // To show activity indicator

                    // Set isRestockActive value to false
                    AppUser.sharedInstance.saveData(false, forKey: .isRestockActive)

                    AlertService.shared.alert(in: self, message: "Restock completed.")
                  }
                } else {
                  if let loaderView = self.loadingView {  // If loadingView already exists
                    loaderView.hide()  // To show activity indicator
                  }
                }
              }

              DispatchQueue.main.async {
                // Reset UI
                self.resetUI(vehicleSnapshot: snapshot, state: vehicleState)
              }

              if uID != nil {
                self.getUser(userID: uID) { userSnapshot in

                  loggerDebug.trace("\(String(describing: userSnapshot))")
                  if userSnapshot != nil {
                    if let type = userSnapshot!.childSnapshot(forPath: "type").value {
                      loggerDebug.trace("Checking type: \(type)")
                      let orderType = type as? Int
                      if orderType != nil, orderType! == 2 {
                        // Play notification sound
                        // playSound(audioName: .tritone)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                          // Restock request audio message
                          playSound(audioName: .restockRequired)
                        }

                        self.statusLabel.text = "Incoming Restock Request"
                        AppUser.sharedInstance.saveData(true, forKey: .isRestockActive)
                      } else {
                        // Play notification sound
                        // playSound(audioName: .tritone)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                          // Incoming request audio message
                          playSound(audioName: .incomingRequest)
                        }

                        self.statusLabel.text = requestStateString(state: vehicleState)
                      }

                    } else {
                      // Play notification sound
                      // playSound(audioName: .tritone)

                      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        // Incoming request audio message
                        playSound(audioName: .incomingRequest)
                      }

                      self.statusLabel.text = requestStateString(state: vehicleState)
                    }
                  }
                }
              } else {
                // Play notification sound
                // playSound(audioName: .tritone)

                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                  // Incoming request audio message
                  playSound(audioName: .incomingRequest)
                }

                self.statusLabel.text = requestStateString(state: vehicleState)
              }

            } else if vehicleState == "9" {
              self.view.bringSubviewToFront(self.processButton)

              self.createUnlockSlider(update: false, isHidden: true) { _ in }

              self.customReceiptView.productListTV.separatorStyle = .none
              self.customReceiptView.isHidden = false
              self.customReceiptView.show()

              DispatchQueue.main.async {
                // Reset UI
                self.resetUI(vehicleSnapshot: snapshot, state: vehicleState)
              }

              self.processButton.isHidden = false
              self.processButton.isEnabled = false
              self.processButton.backgroundColor = .black

              self.runGenerationTimerCountdown()
              // self.processButton.setTitle("\(self.receiptCountdownTime)", for: .normal)

            } else if vehicleState == "5" || vehicleState == "8" {
              // Update markers on status change
              self.updateOnStatusChange(snapshot: snapshot, state: vehicleState)

              if vehicleState == "5" {
                // Play notification sound
                // playSound(audioName: .tritone)
                let topVC = UIApplication.getTopViewController() as? MainVC
                if topVC == nil {
                  loggerDebug.debug("Another view opened")
                  self.dismiss(animated: true, completion: nil)
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                  // Open door audio message
                  playSound(audioName: .openDoor)
                }

                let doorsOpeningPrompt = UIAlertController(
                  title: "Door Status", message: "Open door now", preferredStyle: .alert)

                doorsOpeningPrompt.view.tintColor = .gray

                doorsOpeningPrompt.addAction(
                  UIAlertAction(
                    title: "OK", style: .default,
                    handler: { (_: UIAlertAction!) in
                      // self.processButton.isEnabled = true
                      // self.processButton.backgroundColor = .black

                      let params = ["command": "open"]
                      self.processAPICall(endpoint: .openAndCloseDoors, param: params) {
                        completed, error in
                        if !completed! {
                          loggerDebug.error("Error: \(error!)")
                          AlertService.shared.alert(in: self, message: "\(error!)")
                          return
                        }
                      }

                      // Slide to unlock button initiated
                      // self.createUnlockSlider(update: true, isHidden: false) { (completed) in
                      //     self.unlockSlider.sliderText = "SLIDE TO OPEN DOOR"
                      //     // assign tag to slider button
                      //     self.unlockSlider.tag = Int(vehicleState!)!
                      // }

                      // Play welcome audio message
                      // playSound(audioName: .welcomeAudio)
                    }))

                self.present(doorsOpeningPrompt, animated: true, completion: nil)

              } else if vehicleState == "8" {
                // Play notification sound
                // playSound(audioName: .tritone)

                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                  // Close door audio message
                  playSound(audioName: .closeDoor)
                }

                // Slide to unlock button initiated
                self.createUnlockSlider(update: false, isHidden: false) { _ in
                  // self.unlockSlider.sliderText = "SLIDE TO CLOSE DOOR"
                  // self.unlockSlider.removeFromSuperview()
                  self.unlockSlider.sliderText = "FORCE CLOSE DOOR"
                  // Assign tag to slider button
                  self.unlockSlider.tag = Int(vehicleState)!
                }

                let doorsOpeningPrompt = UIAlertController(
                  title: "Door Status", message: "Close door now", preferredStyle: .alert)

                doorsOpeningPrompt.view.tintColor = .gray

                doorsOpeningPrompt.addAction(
                  UIAlertAction(
                    title: "OK", style: .default,
                    handler: { (_: UIAlertAction!) in

                      let params = ["command": "close"]
                      self.processAPICall(endpoint: .openAndCloseDoors, param: params) {
                        completed, error in
                        if !completed! {
                          loggerDebug.error("Error: \(error!)")
                          AlertService.shared.alert(in: self, message: "\(error!)")
                          return
                        }

                        if self.activeSlideButtonTimer != nil {
                          self.activeSlideButtonTimer?.invalidate()
                        }

                        self.createUnlockSlider(update: false, isHidden: true) { _ in }

                        // self.runGenerationTimerCountdown()
                        self.processButton.isHidden = false
                        self.processButton.isEnabled = false
                        self.processButton.backgroundColor = .black
                        // self.processButton.setTitle("\(self.receiptCountdownTime)", for: .normal)
                        // self.buttonActivityIndictor.stopAnimating()
                      }

                    }))

                self.present(doorsOpeningPrompt, animated: true, completion: nil)
              }

            }

          } else if vehicleState == "0" || vehicleState == "1" || vehicleState == "6"
            || vehicleState == "7"
          {
            self.mapsButton.isHidden = true

            if vehicleState == "0" || vehicleState == "1" {
              // If restock is active then remove the process view
              if let isRestockActive: Bool = AppUser.sharedInstance.getData(
                forKey: .isRestockActive)
              {
                if isRestockActive {
                  // Remove processing loader
                  if let loaderView = self.loadingView {  // If loadingView already exists
                    loaderView.hide()  // To show activity indicator

                    // Set isRestockActive value to false
                    AppUser.sharedInstance.saveData(false, forKey: .isRestockActive)

                    if self.isOrderCanceled {
                      AlertService.shared.alert(in: self, message: "Restock canceled.")
                      self.isOrderCanceled = false
                    } else {
                      AlertService.shared.alert(in: self, message: "Restock completed.")
                    }
                  }
                } else {
                  if let loaderView = self.loadingView {  // If loadingView already exists
                    loaderView.hide()  // To show activity indicator
                  }
                }
              }

              // Reset UI when order complete/delete
              DispatchQueue.main.async {
                self.resetUI(vehicleSnapshot: snapshot, state: vehicleState)
              }

            } else if vehicleState == "6" || vehicleState == "7" {
              // Update markers on status change
              self.updateOnStatusChange(snapshot: snapshot, state: vehicleState)

              // Slide to unlock button initiated
              self.createUnlockSlider(update: false, isHidden: false) { _ in
                self.unlockSlider.sliderText = "FORCE CLOSE DOOR"

                // Assign tag to slider button
                self.unlockSlider.tag = Int(vehicleState)!
              }

              self.activeSlideButtonTimer = Timer.scheduledTimer(
                timeInterval: 7.0,
                target: self,
                selector: #selector(self.activateSlideButtonAction),
                userInfo: Int(vehicleState)!,
                repeats: false)

            } else {
              loggerDebug.debug(
                "Reset UI: statuschangeaction: \(vehicleState)")

              self.processButton.isHidden = false
              self.processButton.isEnabled = false
              self.processButton.backgroundColor = .init(white: 0.8, alpha: 1)
            }
          }

        }

      }
    }
  }

  private func updateOnStatusChange(snapshot: DataSnapshot?, state: String?) {

    if let latStr = snapshot!.childSnapshot(forPath: "location/lat").value as? String,
      let lngStr = snapshot!.childSnapshot(forPath: "location/lng").value as? String,
      let userID = snapshot!.childSnapshot(forPath: "customer_id").value as? String,
      let lat = Double(latStr), let lng = Double(lngStr)
    {

      processButton.isHidden = true

      // Slide to unlock button initiated
      createUnlockSlider(update: false, isHidden: false) { _ in
        // Assign tag to slider button
        self.unlockSlider.tag = Int(state!)!
      }

      let vehicleCoordinates = CLLocationCoordinate2DMake(lat, lng)

      // Get user
      DispatchQueue.main.async {
        loggerDebug.debug("Reset UI: Setting up user marker")
        // Get user data from firebase
        self.getUser(userID: userID) { user in
          self.polyline?.map?.clear()

          if self.usertMarker == nil {
            self.setupUserMarker()
            self.userMarkerImage.isHidden = false
            self.usertMarker!.map = self.mapView
          } else {
            self.userMarkerImage.isHidden = false
            self.usertMarker!.map = self.mapView
          }
          self.robomartMarker!.map = self.mapView

          if let userLatStr = user!.childSnapshot(forPath: "location/lat").value as? String,
            let userLngStr = user!.childSnapshot(forPath: "location/lng").value as? String,
            let userLat = Double(userLatStr), let userLng = Double(userLngStr)
          {
            let userCoordinates = CLLocationCoordinate2DMake(userLat, userLng)

            // Get map zoom initiate
            self.zoomMapWithoutPath(start: vehicleCoordinates, end: userCoordinates)

          }

        }
      }

    }

  }

  // Reset UI
  private func resetUI(vehicleSnapshot: DataSnapshot?, state: String?) {
    let lat = vehicleSnapshot!.childSnapshot(forPath: "location/lat").value!
    let lng = vehicleSnapshot!.childSnapshot(forPath: "location/lng").value!
    let angle = vehicleSnapshot!.childSnapshot(forPath: "location/angle").value!
    // let userID = vehicleSnapshot!.childSnapshot(forPath: "customer_id").value as? String
    // let vehicleState = vehicleSnapshot!.childSnapshot(forPath: "request_state").value as? String

    loggerDebug.debug(
      "Reset UI: \(String(describing: vehicleSnapshot)) & state: \(String(describing: state))"
    )

    AppUser.sharedInstance.removeData(forKey: .userID)
    AppUser.sharedInstance.removeData(forKey: .orderID)

    cancelOrderButton.isHidden = true

    customInfoWindow.removeFromSuperview()

    // Remove polyline from map
    if let polylineNew = polyline {
      polylineNew.map?.clear()  // = nil
    }

    // Reset user marker
    if usertMarker != nil {
      loggerDebug.debug("Reset UI: removing marker")
      userMarkerImage.isHidden = true
      usertMarker?.map?.clear()
      usertMarker = nil
    }

    // Check if robomart marker is nil then initiate the marker
    if robomartMarker == nil {
      robomartMarker = GMSMarker()
    }

    robomartMarkerImage.isHidden = false
    let pinMarkerView = robomartMarkerImage
    pinMarkerView.contentMode = .scaleAspectFit
    robomartMarker!.iconView = pinMarkerView

    // Reseting robomart market position and angle
    robomartMarker!.position = CLLocationCoordinate2D(
      latitude: Double((lat as AnyObject).doubleValue),
      longitude: Double((lng as AnyObject).doubleValue))
    robomartMarker!.rotation = Double((angle as AnyObject).doubleValue)
    robomartMarker!.map = mapView

    let camera = GMSCameraPosition.camera(
      withLatitude: Double((lat as AnyObject).doubleValue),
      longitude: Double((lng as AnyObject).doubleValue), zoom: zoom)
    mapView!.animate(to: camera)

    if state! == "2" {
      processButton.isHidden = false
      processButton.isEnabled = true
      processButton.backgroundColor = .black
    } else if state! == "9" {
      processButton.isHidden = false
      processButton.isEnabled = false
      processButton.backgroundColor = .black
    } else {
      processButton.isHidden = false
      processButton.isEnabled = false
      processButton.backgroundColor = .init(white: 0.8, alpha: 1)
    }

    // Slide to unlock button initiated
    createUnlockSlider(update: false, isHidden: true) { _ in }
  }

  // Call this function to activate slide to unlock button if conusmer wont close the door
  @objc private func activateSlideButtonAction(_ sender: Timer) {
    activeSlideButtonTimer?.invalidate()
    // Slide to unlock button initiated
    createUnlockSlider(update: true, isHidden: false) { _ in

      // self.unlockSlider.sliderText = "SLIDE TO CLOSE DOOR"
      self.unlockSlider.sliderText = "FORCE CLOSE DOOR"

      // Assign tag to slider button
      self.unlockSlider.tag = 8
    }
  }

  // Get polyline from firebase and decode it
  private func getPolylineFromFirebase(snapshot: DataSnapshot, polyline: String?) {
    loggerDebug.trace("getPolylineFromFirebase: Checking: \(snapshot))")

    let lat = snapshot.childSnapshot(forPath: "location/lat").value!
    let lng = snapshot.childSnapshot(forPath: "location/lng").value!
    let userID = snapshot.childSnapshot(forPath: "customer_id").value as? String

    loggerDebug.trace(
      "getPolylineFromFirebase: Checking consumer id: \(String(describing: userID))"
    )

    // if self.usertMarker != nil {
    //     self.usertMarker?.map?.clear()
    //     self.usertMarker = nil
    //
    // }

    let uid: String? = AppUser.sharedInstance.getData(forKey: .userID)

    loggerDebug.trace("Checking user id: \(String(describing: uid))")

    if uid != nil {
      loggerDebug.debug(
        "Reset UI: Setting up user marker, userID: \(String(describing: uid))")
      DispatchQueue.main.async {
        // Get user data from firebase
        self.getUser(userID: uid!) { _ in

          self.userMarkerImage.isHidden = false

          // Add polyline on map
          self.addPolyLineWithEncodedStringInMap(
            encodedString: polyline!, roboLat: lat as? String, roboLng: lng as? String)

          if self.usertMarker == nil {
            self.setupUserMarker()
            self.usertMarker!.map = self.mapView
          } else {
            self.usertMarker!.map = self.mapView
          }

          self.robomartMarker!.map = self.mapView
        }
      }
    }

    // if usertMarker == nil {
    //     if userID != nil {
    //         DispatchQueue.main.async {
    //             // get user data from firebase
    //             self.getUser(userID: userID!) { _ in
    //
    //                 self.usertMarker!.map = self.mapView
    //             }
    //         }
    //     }
    //
    // } else {
    //     usertMarker!.map = mapView
    // }
  }

  // Get user from firebase
  private func getUser(userID: String?, completion: @escaping (DataSnapshot?) -> Void) {
    loggerDebug.debug("User id: \(String(describing: userID))")

    // Initiate firebase with active users
    let userRef = Database.database().reference().child("ActiveCustomers/\(userID!)")

    loggerDebug.debug("User path: \(userRef)")

    userRef.observeSingleEvent(of: .value) { userSnapshot in
      if !userSnapshot.exists() {
        completion(nil)
        loggerDebug.error("Get user: User does not exist")
        // AlertService.shared.alert(in: self, message: "User does not exist.")
      } else {
        loggerDebug.trace("User snapshot key: \(userSnapshot.key)")

        let userLat = userSnapshot.childSnapshot(forPath: "location/lat").value as? Double
        let userLng = userSnapshot.childSnapshot(forPath: "location/lng").value as? Double

        loggerDebug.debug(
          "User location: userlat: \(String(describing: userLat)) and userLng: \(String(describing: userLng))"
        )

        if self.usertMarker != nil {
          self.usertMarker?.map?.clear()
          self.usertMarker = nil
        }

        // Initiate user marker
        self.setupUserMarker()

        if userLat != nil || userLng != nil {
          loggerDebug.debug("User location: lat & lng is available")

          self.usertMarker!.position = CLLocationCoordinate2D(
            latitude: Double(((userLat!) as AnyObject).doubleValue),
            longitude: Double(((userLng!) as AnyObject).doubleValue))
          // self.usertMarker!.map = self.mapView

          completion(userSnapshot)
        }
      }
    }
  }

  // Setup user marker
  private func setupUserMarker() {
    // logger.info("Reset UI: Setting up user marker")

    if usertMarker == nil {
      usertMarker = GMSMarker()
    }

    let pinMarkerView = userMarkerImage
    pinMarkerView.contentMode = .scaleAspectFit
    usertMarker!.iconView = pinMarkerView
  }

  // Slide to unloack button setup
  func createUnlockSlider(update: Bool?, isHidden: Bool?, completion: @escaping (Bool?) -> Void) {
    if isHidden! {
      unlockSlider.removeFromSuperview()
      completion(true)

    } else {
      unlockSlider.removeFromSuperview()
      unlockSlider = AURUnlockSlider()
      unlockSlider.translatesAutoresizingMaskIntoConstraints = false
      unlockSlider.delegate = self
      unlockSlider.sliderCornerRadius = 10
      // unlockSlider.sliderText = "SLIDE TO OPEN DOOR"
      unlockSlider.sliderText = "FORCE CLOSE DOOR"
      unlockSlider.sliderTextFont = UIFont(name: "HelveticaNeue-Thin", size: 17.0)!

      unlockSlider.sliderColor = .clear

      if update! {
        unlockSlider.isUserInteractionEnabled = true
        unlockSlider.sliderTextColor! = .white
        unlockSlider.sliderBackgroundColor = .black  // Colors.shared.buttonBlue
      } else {
        unlockSlider.isUserInteractionEnabled = false
        unlockSlider.sliderTextColor! = .init(white: 1, alpha: 1)
        unlockSlider.sliderBackgroundColor = .init(white: 0.8, alpha: 1)
      }

      view.addSubview(unlockSlider)
      unlockSlider.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 15).isActive =
        true
      unlockSlider.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -15).isActive =
        true

      unlockSlider.bottomAnchor.constraint(
        equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -35
      ).isActive = true

      unlockSlider.heightAnchor.constraint(equalToConstant: 60).isActive = true

      completion(true)
    }
  }
}

// MARK: Google Maps delegate

extension MainVC: GMSMapViewDelegate {
  func mapView(_ mapView: GMSMapView, didChange _: GMSCameraPosition) {
    let latitude = mapView.camera.target.latitude
    let longitude = mapView.camera.target.longitude
    centerMapCoordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    // placeMarkerOnCenter(centerMapCoordinate: centerMapCoordinate)

    let location = CLLocationCoordinate2D(
      latitude: tappedMarker.position.latitude, longitude: tappedMarker.position.longitude)

    customInfoWindowYConstraint?.constant = mapView.projection.point(for: location).y - 107
    customInfoWindowXConstraint?.constant = mapView.projection.point(for: location).x
  }

  func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
    return UIView()
  }

  func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
    loggerDebug.debug("checking marker: didTap")

    let location = CLLocationCoordinate2D(
      latitude: marker.position.latitude, longitude: marker.position.longitude)

    tappedMarker = marker

    var userAddress: String? = ""

    let orderForAddress: Order? = AppUser.sharedInstance.getData(forKey: .currentOrder)

    if let address = orderForAddress {
      userAddress = address.data?.address!
    }

    customInfoWindow.addressLabel.text = userAddress!

    customInfoWindow.titleLabel.textColor = .init(white: 0.3, alpha: 1)

    customInfoWindow.translatesAutoresizingMaskIntoConstraints = false
    // self.view.addSubview(customInfoWindow)
    self.mapView!.addSubview(customInfoWindow)

    loggerDebug.debug("checking marker: didTap. market position: \(marker.infoWindowAnchor.y)")
    customInfoWindowYConstraint = customInfoWindow.centerYAnchor.constraint(
      equalTo: mapView.topAnchor, constant: 0)
    customInfoWindowYConstraint?.isActive = true
    customInfoWindowXConstraint = customInfoWindow.centerXAnchor.constraint(
      equalTo: view.leadingAnchor, constant: 0)
    customInfoWindowXConstraint?.isActive = true
    customInfoWindow.widthAnchor.constraint(lessThanOrEqualToConstant: 300).isActive = true

    customInfoWindowYConstraint?.constant = mapView.projection.point(for: location).y - 107
    customInfoWindowXConstraint?.constant = mapView.projection.point(for: location).x

    customInfoWindow.clipsToBounds = false

    return false
  }

  func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
    loggerDebug.debug("checking marker: didTapAt")
    customInfoWindow.removeFromSuperview()
  }

  func placeMarkerOnCenter(centerMapCoordinate: CLLocationCoordinate2D) {
    if centerPinMarker == nil {
      centerPinMarker = GMSMarker()
    }

    centerPinMarker?.isTappable = false
    centerPinMarker!.icon = imageWithImage(
      image: UIImage(named: "robomart-marker")!, scaledToSize: CGSize(width: 26.0, height: 52.0))
    centerPinMarker!.position = centerMapCoordinate
    centerPinMarker!.groundAnchor = CGPoint(x: 0.5, y: 0.5)
    centerPinMarker!.map = mapView
  }
}

extension GMSMapView {
  func mapStyle(withFilename name: String, andType type: String) {
    do {
      if let styleURL = Bundle.main.url(forResource: name, withExtension: type) {
        mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
      } else {
        loggerDebug.error("Unable to find style.json")
      }
    } catch {
      loggerDebug.error(
        "One or more of the map styles failed to load. \(error.localizedDescription)")
    }
  }
}

// MARK: Location manager delegate

extension MainVC: CLLocationManagerDelegate {
  // Initialize fuction for location manager
  func initializeTheLocationManager() {
    // locationManager.delegate = self
    // locationManager.requestWhenInUseAuthorization()
    // locationManager.startUpdatingLocation()

    if CLLocationManager.locationServicesEnabled() {
      locationManager.delegate = self
      locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters

      let status: CLAuthorizationStatus = CLLocationManager.authorizationStatus()

      loggerDebug.trace("\(CLAuthorizationStatus.authorizedWhenInUse)")

      if status == CLAuthorizationStatus.notDetermined {
        loggerDebug.warning("locationServices notDetermined")
        locationManager.requestAlwaysAuthorization()

      } else if status == CLAuthorizationStatus.authorizedWhenInUse
        || status == CLAuthorizationStatus.authorizedAlways
      {
        loggerDebug.debug(
          "locationServices \(CLAuthorizationStatus.authorizedWhenInUse)")
        locationManager.startUpdatingLocation()

      } else if status == CLAuthorizationStatus.denied {
        loggerDebug.warning("locationServices denied")

        let refreshAlert = UIAlertController(
          title: "Robomart",
          message: "Robomart needs your precise location so our stores can come to you.",
          preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(
          UIAlertAction(
            title: "Ok", style: .default,
            handler: { (_: UIAlertAction!) in
              UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))

        refreshAlert.addAction(
          UIAlertAction(
            title: "Cancel", style: .cancel,
            handler: { (_: UIAlertAction!) in
              loggerDebug.info("Handle Cancel Logic here")

            }))

        present(refreshAlert, animated: true, completion: nil)

      } else if status == CLAuthorizationStatus.restricted {
        loggerDebug.warning("locationServices restricted")
      } else {
        loggerDebug.warning("locationServices disenabled")
        locationManager.startUpdatingLocation()
      }
    } else {
      let refreshAlert = UIAlertController(
        title: "Robomart",
        message: "Robomart needs your precise location so our stores can come to you.",
        preferredStyle: UIAlertController.Style.alert)

      refreshAlert.addAction(
        UIAlertAction(
          title: "Ok", style: .default,
          handler: { (_: UIAlertAction!) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
          }))

      refreshAlert.addAction(
        UIAlertAction(
          title: "Cancel", style: .cancel,
          handler: { (_: UIAlertAction!) in
            loggerDebug.info("Handle Cancel Logic here")
          }))

      present(refreshAlert, animated: true, completion: nil)
    }
  }

  // Location manager didupdatelocation delegate
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else {
      loggerDebug.warning("Location value is nil")
      return
    }

    loggerDebug.trace("Map test: locationManager: checking Camera: didUpdateLocations")
    camera = GMSCameraPosition.camera(
      withLatitude: locValue.latitude, longitude: locValue.longitude, zoom: 16)
    // camera = GMSCameraPosition.camera(withLatitude: (self.setupUserLocation.coordinate.latitude), longitude: (self.setupUserLocation.coordinate.longitude), zoom: 16)

    mapView?.camera = camera
    mapView?.animate(to: camera)
    locationManager.stopUpdatingLocation()

    let location = CLLocation(latitude: locValue.latitude, longitude: locValue.longitude)
    loggerDebug.trace("Map test: locationManager: This location : \(location)")
  }

  // Check if user allow to access location or not
  func locationManager(
    _ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus
  ) {
    switch status {
    case .notDetermined:
      loggerDebug.debug("User still thinking granting location access!")

      manager.startUpdatingLocation()

    case .denied:
      loggerDebug.warning("User denied location access!")

      let refreshAlert = UIAlertController(
        title: "Robomart",
        message: "Robomart needs your precise location so our stores can come to you.",
        preferredStyle: UIAlertController.Style.alert)

      refreshAlert.addAction(
        UIAlertAction(
          title: "Ok", style: .default,
          handler: { (_: UIAlertAction!) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
          }))

      refreshAlert.addAction(
        UIAlertAction(
          title: "Cancel", style: .cancel,
          handler: { (_: UIAlertAction!) in
            loggerDebug.info("Handle Cancel Logic here")
          }))

      present(refreshAlert, animated: true, completion: nil)

      manager.stopUpdatingLocation()

    case .authorizedWhenInUse, .authorizedAlways:

      loggerDebug.debug("User location access! authorizedWhenInUse")
      manager.startUpdatingLocation()

    default:
      break
    }
  }
}

// MARK: - AURUnlockSliderDelegate

extension MainVC: AURUnlockSliderDelegate {
  func unlockSliderDidUnlock(_ slider: AURUnlockSlider) {
    loggerDebug.debug("slide to unlock works!!! \(slider.tag)")

    slider.removeFromSuperview()

    var params: Parameters?

    if slider.tag == 3 {
      // Slide to unlock button initiated
      createUnlockSlider(update: false, isHidden: false) { _ in
        self.unlockSlider.sliderText = "FORCE CLOSE DOOR"
        // self.unlockSlider.sliderText = "SLIDE TO OPEN DOOR"
        // Assign tag to slider button
        // self.unlockSlider.tag = Int(vehicleState!)!
      }

      // let params: Parameters = ["status": "Arrived"]
      params = ["status": "Arrived"]

      // self.processAPICall(endpoint: .setRobomartStatus, param: params) { (completed, error) in
      //     if !completed! {
      //         logger.error("Error: \(error!)")
      //         self.processButton.isEnabled = true
      //         AlertService.shared.alert(in: self, message: "\(error!)")
      //     }
      //
      //     self.processButton.isHidden = true
      //
      //     // Stop/hide loader animation
      //     self.processButton.setTitle(RequestStateButtonString(state: "\(sender.tag)"), for: .normal)
      //     self.buttonActivityIndictor.stopAnimating()
      // }

    } else if slider.tag == 5 {
      createUnlockSlider(update: false, isHidden: false) { _ in

        self.unlockSlider.sliderText = "FORCE CLOSE DOOR"
      }

      loggerDebug.notice("Doors open.")
      params = ["command": "open"]

    } else if slider.tag == 8 {
      createUnlockSlider(update: false, isHidden: false) { _ in

        self.unlockSlider.sliderText = "FORCE CLOSE DOOR"
      }

      slider.removeFromSuperview()

      loggerDebug.notice("Doors close.")
      params = ["command": "close"]
    }

    if slider.tag == 5 || slider.tag == 8 {
      loggerDebug.debug("Set robomart door open / close.")

      processAPICall(endpoint: .openAndCloseDoors, param: params!) { completed, error in
        if !completed! {
          loggerDebug.error("Error: \(error!)")
          AlertService.shared.alert(in: self, message: "\(error!)")
          return
        }

        if slider.tag == 8 {
          if self.activeSlideButtonTimer != nil {
            self.activeSlideButtonTimer?.invalidate()
          }
        }
      }
    } else {
      loggerDebug.debug("Set robomart status.")
      processAPICall(endpoint: .setRobomartStatus, param: params) { completed, error in
        if !completed! {
          loggerDebug.error("Error: \(error!)")
          self.processButton.isEnabled = true
          // AlertService.shared.alert(in: self, message: "\(error!)")
        }

        if slider.tag == 3 {
          self.processButton.isHidden = true
        }
      }
    }
  }

  // MARK: Get all zones
  func getAllZones() {
    if gmsPolygon?.map != nil {
      gmsPolygon?.map = nil
      // logger.info("gmsPolygon: is available!")
    }

    // Get all zones
    APIManager.sharedInstance.get(endpoint: .getAllZonesForDriverApp) { [self] res, error in

      if let err = error {
        loggerDebug.error(
          "GET: \(baseURL)\(EndPoints.getAllZonesForDriverApp.path), Message: \(err.localizedDescription)"
        )
      } else {
        do {
          let json = try JSON(data: res!)
          loggerDebug.trace(
            "GET: \(baseURL)\(EndPoints.getAllZonesForDriverApp.path), Data: \(json)")
          let response = try JSONDecoder().decode(AllZone.self, from: res!)

          if response.success != nil && response.data != nil {
            loggerDebug.debug("Can get zones")
            let zones = response.data!

            //  Coordinates from web portal
            // { lat: 85.0511, lng: -180 },
            //  { lat: 85.0511, lng: 0 },
            //  { lat: 85.0511, lng: 180 },
            //  { lat: -85.0511, lng: 180 },
            //  { lat: -85.0511, lng: 0 },
            //  { lat: -85.0511, lng: -180 },

            fillingPath.addLatitude(85.0511, longitude: -179.5)
            fillingPath.addLatitude(85.0511, longitude: 0)
            fillingPath.addLatitude(85.0511, longitude: 179.5)
            fillingPath.addLatitude(-85.0511, longitude: 179.5)
            fillingPath.addLatitude(-85.0511, longitude: 0)
            fillingPath.addLatitude(-85.0511, longitude: -179.5)

            // Add polygon to the map
            //            gmsPolygonForBG = GMSPolygon(path: fillingPath)
            gmsPolygon = GMSPolygon(path: fillingPath)

            gmsPaths = [GMSPath]()

            for zone in zones {
              let polygon = try! Geometry(wkt: zone.coords!)

              switch polygon {
              case .polygon(let polygon):

                // Get point from polygon
                let points = polygon.exterior.points

                // Collect coords for map
                let coords = points.map {
                  p in
                  CLLocationCoordinate2D(
                    latitude: p.y,
                    longitude: p.x)
                }

                // Draw path on map
                let path = GMSMutablePath()
                for c in coords {
                  path.add(c)
                }

                gmsPaths.append(path)  // Creating zone outline
                gmsPolygon!.holes = gmsPaths  // It will holosr the backgound inside the boarder
                gmsPolygon!.fillColor = UIColor.gray.withAlphaComponent(0.15)

                gmsPolygon!.strokeColor = UIColor.red.withAlphaComponent(0.5)
                gmsPolygon!.strokeWidth = 3

              //                gmsPolygon!.map = self.mapView

              default:
                // Handle other types of Geometry or fail here
                loggerDebug.warning(
                  "GET: \(baseURL)\(EndPoints.getAllZonesForDriverApp.path), Message: Failed to create zone."
                )
              }
            }

            gmsPolygon!.map = self.mapView

          } else {
            loggerDebug.error("Can't get zones")
          }

        } catch let jsonError {
          loggerDebug.error(
            "GET: \(baseURL)\(EndPoints.getAllZonesForDriverApp.path), Message: \(jsonError.localizedDescription)"
          )
        }
      }
    }
  }
}
