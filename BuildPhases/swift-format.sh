#!/bin/bash

echo "Trying to format all modified swift files ..."

if which swift-format >/dev/null; then
  git diff HEAD --diff-filter=d --name-only | grep -e '\(.*\).swift$' | while read line; do
    swift-format -m format -i "${line}";
    echo "$line";
  done
else
  echo "Warning: Formatting failed as swift-format is not installed!"
fi