#!/bin/bash

echo "Trying to lint all swift files in \"${PRODUCT_NAME}/\" folder  ..."

if which swift-format >/dev/null; then
  swift-format -m lint -r "${PROJECT_DIR}/${PRODUCT_NAME}"
  exit 0
else
  echo "Warning: Linting failed as swift-format is not installed!"
fi
