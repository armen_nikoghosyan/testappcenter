#!/bin/bash

# Note: for simplicity, it's assumed that there's already a bundle version
# (which is an integer).

# Get path to the BUILD .plist, NOT the packaged one!
# This fixes the off-by-one bug.
builtInfoPlistPath=${TARGET_BUILD_DIR}/${INFOPLIST_PATH}
echo "Using plist at $builtInfoPlistPath"

modifiedFilesExist=false
# This is the modification date to compare to -- there's a possible bug here,
# if you edit the built plist directly, for some reason. probably you shouldn't
# do that anyways.
compModDate=$(stat -f "%m" "$builtInfoPlistPath")

for filename in *; do
  modDate=$(stat -f "%m" "$filename")
  if [ "$modDate" -gt "$compModDate" ]; then
    modifiedFilesExist=true
    echo "Found newly modified file: $filename"
    break
  fi
done

if $modifiedFilesExist; then
  echo "A file is new, bumping version"

  # Increment the build number and set in Info.plist
  buildNumber=$(
    /usr/libexec/PlistBuddy -c "Print :CFBundleVersion" "$builtInfoPlistPath"
  )
  echo "Retrieved current build number: $buildNumber"

  buildNumber=$(($buildNumber + 1))
  echo "Setting build number to: $buildNumber"

  echo "Setting build number in $builtInfoPlistPath"
  /usr/libexec/PlistBuddy -c \
    "Set :CFBundleVersion $buildNumber" "$builtInfoPlistPath"

  # xcrun agvtool new-version "$buildNumber"
else
  echo "Build number not incremented -- no newly modified files"
fi

# Compose the short version string
versionStringShort=$(
  git describe --tags --dirty --broken --abbrev=8 --first-parent
)
# $(
#   git describe --tags --abbrev=8 --first-parent
# )

echo "Generated version string: $versionStringShort"
echo "Setting version string in $builtInfoPlistPath"
/usr/libexec/PlistBuddy -c \
  "Set :CFBundleShortVersionString $versionStringShort" "$builtInfoPlistPath"